var App = (function() {
        
    var viewsMap = null,
        usingPointer = false,
        view_manager = new ViewManager();
    
    var getCurrentView = function() {
        return view_manager.getCurrentView().id;
    };
    
    var getViewsMap = function() {
        return viewsMap;
    };
    
    var addView = function(viewConfObj) {
        var view = new View(viewConfObj);
        
        App[viewConfObj.id] = view;
        return view_manager.addView(view);
    };
    
    var removeView = function(view) {
        App[view] = null;
        delete App[view];
        
        return view_manager.removeView(view);
    };
    
    var focusItem = function(id) {
        if (document.getElementById(id)) {
            Focus.moveTo(id);
            return true;
        }
        return false;
    };
    
    var deinit = function() {
        view_manager.deinit();
    };
    
    var init = function() {
        var index = -1,
            map = {},
            views = view_manager.getViews(),
            length = views.length;
    
        while (++index < length) {
            var view = views[index];
            
            if (index === 0) {
                view_manager.setCurrentView(view);
            }
            App[view.id] = view;
            map['VIEW_' + view.id.toUpperCase()] = view.id;
            if (typeof view.init === 'function') {
                view.init.call(view);
            }
        }
        
        viewsMap = map;
    };
    
    var handleKeyDown = function(e, keyCode, keyMap) {
        var currentView = view_manager.getCurrentView();
        
        e.preventDefault();
        
        if (usingPointer) {
            usingPointer = false;
            $('body').removeClass('pointer').addClass('rmcu');
        }
        
        if (typeof currentView.onKeyDown === 'function') {
            currentView.onKeyDown(keyCode, keyMap);
        }
        
        switch (keyCode) {
            case keyMap.VK_LEFT : 
                Focus.moveLeft();
                break;
            case keyMap.VK_RIGHT :
                Focus.moveRight();
                break;
            case keyMap.VK_DOWN :
                Focus.moveDown();
                break;
            case keyMap.VK_UP :
                Focus.moveUp();
                break;
            case keyMap.VK_ENTER :
                Focus.click();
                break;
            case keyMap.VK_BACK :
            case keyMap.VK_BACK_SPACE :
                if (typeof currentView.onHistoryBack === 'function') {
                    currentView.onHistoryBack();
                }
                break;
        }
    };
    
    var handleKeyUp = function(keyCode, keyMap) {
        var currentView = view_manager.getCurrentView();
        
        if (typeof currentView.onKeyUp === 'function') {
            currentView.onKeyUp(keyCode, keyMap);
        }
    };
    
    var onMouseMove = function() {
        var currentView = view_manager.getCurrentView();
        
        if (!usingPointer) {
            usingPointer = true;
            $('body').removeClass('rmcu').addClass('pointer');
        }
        
        if (typeof currentView.onMouseMove === 'function') {
            currentView.onMouseMove();
        }
    };
    
    var executeViewFunction = function(options) {
        if (typeof options !== 'object') {
            return;
        }
        var name = options.name,
            params = options.params,
            viewID = options.view,
            view = viewID ? view_manager.getViewById(viewID) : view_manager.getCurrentView();

        if (name && typeof view[name] === 'function') {
            return view[name](params);
        }
    };
    
    var goToView = function(viewID, callback) {
        var currView = view_manager.getCurrentView(),
            nextView = view_manager.getViewById(viewID);

        if (currView !== nextView) {
            
            if (typeof nextView.onLoad === 'function') {
                nextView.onLoad();
            }
            
            TransitionManager.transition(currView, nextView, function() {
                SmartLibrary.Logger.log('App.js', 'public.goToView animation complete. typeof callback = ' + typeof callback);
                if (typeof callback === 'function') {
                    callback();
                }
            }); // hide current view and show next one
            view_manager.setCurrentView(nextView); // set current view
            if (currView) {
                $('#' + currView.containerID).removeClass('active'); // remove active class from previous view
                if (typeof currView.onUnload === 'function') {
                    currView.onUnload();
                }
            }
            $('#' + nextView.containerID).addClass('active'); // add active class to current view

        }

        // Garbage collection
        currView = nextView = null;
    };
    
    return {
        init: init,
        deinit: deinit,
        getCurrentView: getCurrentView,
        getViewsMap: getViewsMap,
        addView: addView,
        removeView: removeView,
        focus: focusItem,
        handleKeyDown: handleKeyDown,
        handleKeyUp: handleKeyUp,
        onMouseMove: onMouseMove,
        executeViewFunction: executeViewFunction,
        goToView: goToView
    };
    
})();