(function() {
    SmartLibrary.Logger.setEnabled(CONF.log);
    TransitionManager.setAnimationSpeed(CONF.animationSpeed);
    SmartLibrary.Navigation.onKeyDown = App.handleKeyDown;
    SmartLibrary.Navigation.onMouseMove = App.onMouseMove;
    SmartLibrary.Navigation.onKeyUp = App.handleKeyUp;
    App.init();
})();