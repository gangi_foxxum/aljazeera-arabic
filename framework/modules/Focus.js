var Focus = Focus || (function(window) {
    
    var _allowedClassesToRegister = ['Carousel'],
        _currFocused = {},
        _watchTimeout = null,
        _animating = false,
        _keepingFocusPosition = false,
        _watchProps = {},
        _map = null,
        _prop = {
            elemId: null,
            animationSpeed: null,
            animationMethod: null,
            border: null,
            onFocus: null,
            onBlur: null
        };
        
    var _canRegister = function(nav) {
        var index = 0,
            allowed = _allowedClassesToRegister,
            className = false;

        while (index <= allowed.length - 1) {
            var classNameString = allowed[index];
            if (nav instanceof window[classNameString]) {
                className = classNameString;
                break;
            }

            index++;
        }
    
        return className;
    };
    
    var _addIdToObject = function(nav) {
        var final = {},
            id = Focus.Helper.generateIdentifier(_map);

        final[id] = nav;

        return final;
    };
    
    var _getMapByElemId = function(elemId) {
        //console.log(_map);
        if (_map) {
            var map = false;
            
            for (var attr in _map) {

                var value = _map[attr];
                /*console.log("outputting property {" + attr + "}, typeof value = " + typeof value);
                console.log(value);
                console.log(value[elemId]);*/

                if ((value && value[elemId] && value[elemId] instanceof Array) || (value && _canRegister(value) && value.owns(elemId))) {
                    //console.log("found!", value);
                    map = value;
                    break;
                } 
            }
            
            return map;
        }

        return null;
    };
    
    var _watch = function() {
        if (_watchTimeout) {
            clearTimeout(_watchTimeout);
        }
        _watchTimeout = setTimeout(function() {
            var nextElem = _currFocused.elem[0],
                nextElemPos = Focus.Helper.cumulativeOffset(nextElem),
                computedStyle = window.getComputedStyle(nextElem),
                width = computedStyle && computedStyle.getPropertyValue('width') !== '' ? parseInt(computedStyle.getPropertyValue('width').split('px')[0]) : null,
                height = computedStyle && computedStyle.getPropertyValue('height') !== '' ? parseInt(computedStyle.getPropertyValue('height').split('px')[0]) : null,
                borderRadiusTopLeft = computedStyle && computedStyle.getPropertyValue('border-top-left-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-top-left-radius').split('px')[0]) : null,
                borderRadiusTopRight = computedStyle && computedStyle.getPropertyValue('border-top-right-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-top-right-radius').split('px')[0]) : null,
                borderRadiusBottomLeft = computedStyle && computedStyle.getPropertyValue('border-bottom-left-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-bottom-left-radius').split('px')[0]) : null,
                borderRadiusBottomRight = computedStyle && computedStyle.getPropertyValue('border-bottom-right-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-bottom-right-radius').split('px')[0]) : null,
                props = _prop.animationMethod === 'css3' ? {
                    '-moz-transition': 'none',
                    '-webkit-transition': 'none',
                    '-o-transition': 'none',
                    'transition': 'none',
                    'top': nextElemPos.top - _prop.border,
                    'left': nextElemPos.left - _prop.border,
                    'width': width + (_prop.border * 2),
                    'height': height + (_prop.border * 2),
                    'border-radius' : borderRadiusTopLeft + 'px ' + borderRadiusTopRight + 'px ' + borderRadiusBottomRight + 'px ' + borderRadiusBottomLeft + 'px'
                } : {
                    'top': nextElemPos.top - _prop.border,
                    'left': nextElemPos.left - _prop.border,
                    'width': width + (_prop.border * 2),
                    'height': height + (_prop.border * 2),
                    'border-radius' : borderRadiusTopLeft + 'px ' + borderRadiusTopRight + 'px ' + borderRadiusBottomRight + 'px ' + borderRadiusBottomLeft + 'px'
                };
            
            //console.log("change detected? _animating = " + _animating + ", keepingFocus = " + _keepingFocusPosition);
            if ((_watchProps.top !== props.top || _watchProps.left !== props.left || _watchProps.width !== props.width || _watchProps.height !== props.height || _watchProps['border-radius'] !== props['border-radius']) && !_keepingFocusPosition) {
                //console.log("change detected! _animating = " + _animating + ", keepingFocus = " + _keepingFocusPosition);
                $('#' + _prop.elemId).css(props);
            }
           
            //if (_animating) {
                _watch();   
            //}
        }, 10);
    };
    
    var _jqueryAnimate = function(elemId, keepPosition) {
        if (typeof $ !== 'function') {
            return false;
        }
        var map = _getMapByElemId(elemId),
            nextElem = map ? window.document.getElementById(elemId) : null,
            nextElemPos = nextElem ? Focus.Helper.cumulativeOffset(nextElem) : null,
            computedStyle = nextElem ? window.getComputedStyle(nextElem) : null,
            width = computedStyle ? parseInt(nextElem.clientWidth) : null,
            height = computedStyle ? parseInt(nextElem.clientHeight) : null,
            //width = computedStyle && computedStyle.getPropertyValue('width') !== '' ? parseInt(computedStyle.getPropertyValue('width').split('px')[0]) : null,
            //height = computedStyle && computedStyle.getPropertyValue('height') !== '' ? parseInt(computedStyle.getPropertyValue('height').split('px')[0]) : null,
            borderRadiusTopLeft = computedStyle && computedStyle.getPropertyValue('border-top-left-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-top-left-radius').split('px')[0]) : null,
            borderRadiusTopRight = computedStyle && computedStyle.getPropertyValue('border-top-right-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-top-right-radius').split('px')[0]) : null,
            borderRadiusBottomLeft = computedStyle && computedStyle.getPropertyValue('border-bottom-left-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-bottom-left-radius').split('px')[0]) : null,
            borderRadiusBottomRight = computedStyle && computedStyle.getPropertyValue('border-bottom-right-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-bottom-right-radius').split('px')[0]) : null;
    
        if (!nextElem) {
            return false;
        }

        if (_currFocused && _currFocused.map) {
            _animating = true;
        }
                
        if (map !== _currFocused) {
            if (_currFocused && _currFocused.id && typeof _prop.onBlur === 'function') {
                _prop.onBlur(_currFocused.id);
            }
            _currFocused = {
                id: elemId,
                elem: $('#' + elemId),
                map: map
            };
            if (typeof _currFocused.map.focus === 'function') {
                _currFocused.map.focus();
            }
            if (typeof _prop.onFocus === 'function') {
                _prop.onFocus(_currFocused.id);
            }
        }
        
        _keepingFocusPosition = keepPosition;

        if (!keepPosition) {
            $('#' + _prop.elemId).stop(true, true).animate({
                'top': nextElemPos.top - _prop.border,
                'left': nextElemPos.left - _prop.border,
                'width': width + (_prop.border * 2),
                'height': height + (_prop.border * 2),
                'border-radius' : borderRadiusTopLeft + 'px ' + borderRadiusTopRight + 'px ' + borderRadiusBottomRight + 'px ' + borderRadiusBottomLeft + 'px'
            }, _prop.animationSpeed);
            
             _watchProps = {
                'top': nextElemPos.top - _prop.border,
                'left': nextElemPos.left - _prop.border,
                'width': width + (_prop.border * 2),
                'height': height + (_prop.border * 2),
                'border-radius' : borderRadiusTopLeft + 'px ' + borderRadiusTopRight + 'px ' + borderRadiusBottomRight + 'px ' + borderRadiusBottomLeft + 'px'
            };
            
            _watch();
        }
    };
    
    var _css3animate = function(elemId, keepPosition) {
        var map = _getMapByElemId(elemId),
            nextElem = map ? window.document.getElementById(elemId) : null,
            nextElemPos = nextElem ? Focus.Helper.cumulativeOffset(nextElem) : null,
            computedStyle = nextElem ? window.getComputedStyle(nextElem) : null,
            width = computedStyle ? parseInt(nextElem.clientWidth) : null,
            height = computedStyle ? parseInt(nextElem.clientHeight) : null,
            //width = computedStyle && computedStyle.getPropertyValue('width') !== '' ? parseInt(computedStyle.getPropertyValue('width').split('px')[0]) : null,
            //height = computedStyle && computedStyle.getPropertyValue('height') !== '' ? parseInt(computedStyle.getPropertyValue('height').split('px')[0]) : null,
            borderRadiusTopLeft = computedStyle && computedStyle.getPropertyValue('border-top-left-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-top-left-radius').split('px')[0]) : null,
            borderRadiusTopRight = computedStyle && computedStyle.getPropertyValue('border-top-right-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-top-right-radius').split('px')[0]) : null,
            borderRadiusBottomLeft = computedStyle && computedStyle.getPropertyValue('border-bottom-left-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-bottom-left-radius').split('px')[0]) : null,
            borderRadiusBottomRight = computedStyle && computedStyle.getPropertyValue('border-bottom-right-radius') !== '' ? parseInt(computedStyle.getPropertyValue('border-bottom-right-radius').split('px')[0]) : null;
    
        /*console.log(map);
        console.log(nextElem);*/
            
        if (!nextElem) {
            return false;
        }
        
        if (_currFocused && _currFocused.map) {
            _animating = true;
        }
                
        if (map !== _currFocused) {
            if (_currFocused && _currFocused.id && typeof _prop.onBlur === 'function') {
                _prop.onBlur(_currFocused.id);
            }
            _currFocused = {
                id: elemId,
                elem: $('#' + elemId),
                map: map
            };
            if (typeof _currFocused.map.focus === 'function') {
                _currFocused.map.focus();
            }
            if (typeof _prop.onFocus === 'function') {
                _prop.onFocus(_currFocused.id);
            }
        }
        
        _keepingFocusPosition = keepPosition;
        //console.log('animate. destination = ' + elemId + ', keepPosition = ' + _keepingFocusPosition);
        
        if (!keepPosition) {
            $('#' + _currFocused.id).on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
                //console.log("transition ended!");
                $(this).off('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd').css({
                    '-moz-transition': 'none',
                    '-webkit-transition': 'none',
                    '-o-transition': 'none',
                    'transition': 'none'
                });
            });

            $('#' + _prop.elemId).css({
                '-moz-transition': 'all ' + (_prop.animationSpeed / 1000) + 's',
                '-webkit-transition': 'all ' + (_prop.animationSpeed / 1000) + 's',
                '-o-transition': 'all ' + (_prop.animationSpeed / 1000) + 's',
                'transition': 'all ' + (_prop.animationSpeed / 1000) + 's',
                'top': nextElemPos.top - _prop.border,
                'left': nextElemPos.left - _prop.border,
                'width': width + (_prop.border * 2),
                'height': height + (_prop.border * 2),
                'border-radius' : borderRadiusTopLeft + 'px ' + borderRadiusTopRight + 'px ' + borderRadiusBottomRight + 'px ' + borderRadiusBottomLeft + 'px'
            });

            $('#' + _currFocused.id).css({
                '-moz-transition': 'all ' + (_prop.animationSpeed / 1000) + 's',
                '-webkit-transition': 'all ' + (_prop.animationSpeed / 1000) + 's',
                '-o-transition': 'all ' + (_prop.animationSpeed / 1000) + 's',
                'transition': 'all ' + (_prop.animationSpeed / 1000) + 's'
            });
            
            _watchProps = {
                'top': nextElemPos.top - _prop.border,
                'left': nextElemPos.left - _prop.border,
                'width': width + (_prop.border * 2),
                'height': height + (_prop.border * 2),
                'border-radius' : borderRadiusTopLeft + 'px ' + borderRadiusTopRight + 'px ' + borderRadiusBottomRight + 'px ' + borderRadiusBottomLeft + 'px'
            };
            
            _watch();
        }
    };

    var public = {};
    
    public.init = function(confObj) {
        var focusId = confObj.elemId,
            elem = window.document.getElementById(focusId),
            computedStyle = elem && typeof window.getComputedStyle === 'function' ? window.getComputedStyle(elem) : null;

        for (var attr in confObj) {
            _prop[attr] = confObj[attr];
        }

        try {
            if (computedStyle) {
                //console.log('computedStyle exists');
                _prop.border = computedStyle['border-left-width'] && computedStyle['border-left-width'].length > 0 ? parseInt(computedStyle['border-left-width'].split('px')[0]) : ($(elem).css("border-left-width").length > 0 ? parseInt($(elem).css("border-left-width")).split('px')[0] : null);
                //console.log('border value is : ' + _prop.border);
            } else {
                _prop.border = parseInt($(elem).css("border-left-width")).split('px')[0];
            }
        } catch (e) {}    

        $('#' + focusId).on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
            //console.log("focus reached destination. _keepingFocusPosition = " + _keepingFocusPosition);
            _animating = false;
            _watch();
        }).click(public.click);
    };
    
    public.register = function(navObject) {
        var registeringClass = _canRegister(navObject) || (navObject instanceof Object ? 'string' : false);
        if (typeof registeringClass === 'string') {
            var final = {},
                allowedClassPos = _allowedClassesToRegister.indexOf(registeringClass),
                allowedClassName = allowedClassPos > -1 ? _allowedClassesToRegister[allowedClassPos] : false,
                currMap = _map,
                nav = _addIdToObject(navObject);

            if (allowedClassName) {
                $('#' + navObject.containerId).on('mouseenter', 'li', function() {
                    //console.log("mouseenter", this, $(this).attr('id'));
                    public.moveTo($(this).attr('id'));
                });
            } else {
                var keys = Object.keys(navObject),
                    length = keys.length,
                    index = -1;
            
                while (++index < length) {
                    $('#' + keys[index]).mouseenter(function() {
                        public.moveTo($(this).attr('id'));
                    });
                }
                //console.log(navObject);
            }

            for (var attrname in currMap) { 
                final[attrname] = currMap[attrname]; 
            }

            for (var attrname in nav) { 
                final[attrname] = nav[attrname]; 
            }

            _map = final;
            return Object.keys(nav)[0];
        }

        return false;
    };
    
    public.unregister = function(elemId) {
        _map[elemId] = null;
    };
    
    public.moveTo = function(elemId, keepPosition) {
        if (typeof elemId === 'function') {
            return elemId();
        } else if (!_prop.animationMethod || !elemId) {
            return false;
        }
        /*console.log("move to : " + elemId);
        console.log(elemId);*/
        $('#' + _currFocused.id).removeClass('focus');
        $('#' + elemId).addClass('focus');
        switch (_prop.animationMethod) {
            case 'css3' :
                _css3animate(elemId, keepPosition);
                break;
            case 'jquery' :
                _jqueryAnimate(elemId, keepPosition);
                break;
        }

        return true;
    };
    
    public.moveRight = function() {
        if (!_currFocused) {
            return false;
        }
        var isMapArray = typeof _canRegister(_currFocused.map) !== 'string',
            move = true;
                
        if (!isMapArray) {
            move = _currFocused.map.moveRight();
            _keepingFocusPosition = !move;
        }
        if (move) {
            if (isMapArray) {
                this.moveTo(_currFocused.map[_currFocused.id][1]);
            } else if (_currFocused.map.getFocused) {
                this.moveTo(_currFocused.map.getFocused());
            }
        } else {
            _currFocused.id = _currFocused.map.getFocused();
            _currFocused.elem = $('#' + _currFocused.map.getFocused());
        }
    };
    
    public.moveLeft = function() {
        if (!_currFocused) {
            return false;
        }
        var isMapArray = typeof _canRegister(_currFocused.map) !== 'string',
            move = true;
        
        if (!isMapArray) {
            move = _currFocused.map.moveLeft();
            _keepingFocusPosition = !move;
        }
        if (move) {
            if (isMapArray) {
                this.moveTo(_currFocused.map[_currFocused.id][0]);
            } else if (_currFocused.map.getFocused) {
                this.moveTo(_currFocused.map.getFocused());
            }
        } else {
            _currFocused.id = _currFocused.map.getFocused();
            _currFocused.elem = $('#' + _currFocused.map.getFocused());
        }
    };
    
    public.moveUp = function() {
        if (!_currFocused) {
            return false;
        }
        var isMapArray = typeof _canRegister(_currFocused.map) !== 'string',
            move = true;
        
        if (!isMapArray) {
            move = _currFocused.map.moveUp();
            _keepingFocusPosition = !move;
        }
        if (move) {
            if (isMapArray) {
                this.moveTo(_currFocused.map[_currFocused.id][2]);
            } else if (_currFocused.map.getFocused) {
                this.moveTo(_currFocused.map.getFocused());
            }
        } else {
            _currFocused.id = _currFocused.map.getFocused();
            _currFocused.elem = $('#' + _currFocused.map.getFocused());
        }
    };
    
    public.moveDown = function() {
        if (!_currFocused) {
            return false;
        }
        var isMapArray = typeof _canRegister(_currFocused.map) !== 'string',
            move = true;
        
        if (!isMapArray) {
            move = _currFocused.map.moveDown();
            _keepingFocusPosition = !move;
        }
        if (move) {
            if (isMapArray) {
                this.moveTo(_currFocused.map[_currFocused.id][3]);
            } else if (_currFocused.map.getFocused) {
                this.moveTo(_currFocused.map.getFocused());
            }
        } else {
            _currFocused.id = _currFocused.map.getFocused();
            _currFocused.elem = $('#' + _currFocused.map.getFocused());
        }
    };
    
    public.click = function() {
        //console.log("you clicked on element : " + _currFocused.id);
        $('#' + _currFocused.id).trigger('click');
    };
    
    public.getCurrNav = function() {
        return _currFocused;
    };
    
    public.show = function() {
        $('#' + _prop.elemId).show();
    };
    
    public.hide = function() {
        $('#' + _prop.elemId).hide();
    };
    
    return public;
    
})(window);

Focus.Helper = (function() {
    
    var _s4 = function() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    };
    
    var public = {};
    
    public.cumulativeOffset = function(element) {
        var offset = $(element).offset(),
            body = $('body');
    
        return {
            top: offset.top - body.offset().top,
            left: offset.left - body.offset().left
        };
    };

    public.str_replace = function(search, replace, str) {
        return str.split(search).join(replace);
    };

    public.generateIdentifier = function() {
        return _s4() + _s4() + '-' + _s4() + '-' + _s4() + '-' + _s4() + '-' + _s4() + _s4() + _s4();
    };
        
    return public;

})();