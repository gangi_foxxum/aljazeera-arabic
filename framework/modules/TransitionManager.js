var TransitionManager = (function(window) {
    
    var _speed = null;
    
    var public = {};
    
    public.onTransitionStart = function() {};
    public.onTransitionComplete = function() {};
    
    /**
    * This function sets the animation speed for transitions<br /><br />
    * 
    * Example:<br />
    * 
    *    setAnimationSpeed(400)
    * 
    * @param {number} speed new animation speed
    * 
    * @returns {number} new transition speed
    */
    public.setAnimationSpeed = function(speed) {
        _speed = speed;
        return _speed;
    };
    
    /**
    * This function performs a transition between two views<br /><br />
    * 
    * Example:<br />
    * 
    *    transition<br />
    *    (<br />
    *    &nbsp;&nbsp;{id: 'view1', containerID: 'my-container-1'},<br />
    *    &nbsp;&nbsp;{id: 'view2', containerID: 'my-container-2'},<br />
    *    &nbsp;&nbsp;function() {<br />
    *    &nbsp;&nbsp;&nbsp;&nbsp;// code to execute when the transition has been completed<br />
    *    &nbsp;&nbsp;}<br />
    *    }
    * 
    * @param {object} currentView active app view
    * @param {object} nextView next active view
    * @param {function} callback function to execute when the transition ends
    * 
    * @returns {boolean} whether the transition could be performed or not
    */
    public.transition = function(currentView, nextView, callback) {
        if (nextView instanceof window.View) {
            if (currentView && typeof currentView.onHideStart === 'function') {
                currentView.onHideStart(nextView.id);
            }
            if (typeof nextView.onShowStart === 'function') {
                nextView.onShowStart();
            }
            if (typeof this.onTransitionStart === 'function') {
                this.onTransitionStart();
            }
            if (currentView instanceof window.View) {
                window.SmartLibrary.Animation.triggerById
                (
                    currentView.containerID,
                    currentView.onHideOptions,
                    _speed,
                    (currentView.onHideComplete) ? currentView.onHideComplete : function() {
                        window.TransitionManager.onTransitionComplete();
                    }
                );
            }
            window.SmartLibrary.Animation.triggerById
            (
                nextView.containerID,
                nextView.onShowOptions,
                _speed,
                (nextView.onShowComplete) ? function() {
                    nextView.onShowComplete();
                    window.TransitionManager.onTransitionComplete();
                    if (typeof callback === 'function') {
                        callback();
                    }
                }: this.onTransitionComplete
            );
            return true;
        }
        return false;
    };
        
    return public;
    
})(window);