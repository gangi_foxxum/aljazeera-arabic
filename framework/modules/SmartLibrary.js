/**
 * 
 * -------------- FOXXUM SMART TV MODULE --------------<br />
 * 
 *              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Platform     : Foxxum <br />
 *              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Version      : 1.0 <br />
 *              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Year         : 2015 <br />
 *              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Developed by : Foxxum GmbH <br />
 *              
 * -----------------------------------------------------
 * 
 **/

var SmartLibrary = (function() {
    
    /**
    * This function adds an event listener to an element<br /><br />
    * 
    * Example:<br />
    * 
    *    SmartLibrary.setListener(document, 'click', myCallbackFunction, false);
    * 
    * @param {object} elem element to add the event listener to
    * @param {string} type event type to attach
    * @param {function} callback function to execute when the event is triggered
    * @param {boolean} useCapture a boolean value that specifies whether the event should be executed in the capturing or in the bubbling phase. 
    * 
    * @returns {undefined}
    */
    var setListener = function(elem, type, callback, useCapture) {
        elem.addEventListener(type, callback, useCapture);
    };
    
    /**
    * This function removes an event listener from an element<br /><br />
    * 
    * Example:<br />
    * 
    *    SmartLibrary.removeListener(document, 'click', myCallbackFunction, false);
    * 
    * @param {object} elem element to remove the event listener from
    * @param {string} type event type to remove
    * @param {function} callback function to execute when the event is triggered
    * @param {boolean} useCapture a boolean value that specifies whether the event should be executed in the capturing or in the bubbling phase. 
    * 
    * @returns {undefined}
    */
    var removeListener = function(elem, type, callback, useCapture) {
        elem.removeEventListener(type, callback, useCapture);
    };
    
    /**
    * This function adds a new script to the page<br /><br />
    * 
    * Example:<br />
    * 
    *    SmartLibrary.includeScript<br />
    *    (<br />
    *      &nbsp;&nbsp;'url-to-my-script', <br />
    *      &nbsp;&nbsp;function() {<br />
    *        &nbsp;&nbsp;&nbsp;&nbsp;// to do if successful<br />
    *      &nbsp;&nbsp;},<br />
    *      &nbsp;&nbsp;function(e) {<br />
    *        &nbsp;&nbsp;&nbsp;&nbsp;// to do if fail<br />
    *      &nbsp;&nbsp;}<br />
    *    );
    * 
    * @param {string} url script url
    * @param {function} onloaded [optional] function to execute if script is added successfully
    * @param {function} onerror [optional] function to execute if script cannot be loaded
    * 
    * @returns {undefined}
    */
    var includeScript = function(url, onloaded, onerror) {
        var script = document.createElement('script'),
            first_page_script = document.getElementsByTagName('script')[0];
    
        script.async = true;
        script.src = url;
        if (typeof onloaded === 'function') {
            script.onload = onloaded;
        }
        if (typeof onerror === 'function') {
            script.onerror = onerror;
        }
        first_page_script.parentNode.insertBefore(script, first_page_script);
        
        // Garbage collection
        script = first_page_script = null;
    };
    
    /**
    * This function returns a GET parameter's value<br /><br />
    * 
    * Example:<br />
    * 
    *    SmartLibrary.$_GET('id');
    * 
    * @param {string} name parameter's name
    * 
    * @returns {string} GET parameter's value 
    */
    var $_GET = function(name) {
        var parts = window.location.search.substr(1).split("&");
        for (var i = 0; i < parts.length; i++) {
            var temp = parts[i].split("=");
            if (decodeURIComponent(temp[0]) === name) {
                return decodeURIComponent(temp[1]);
            }
        }
        return false;
    };
    
    /**
    * This function returns the stylesheet for given classname<br /><br />
    * 
    * Example:<br />
    * 
    *    SmartLibrary.$_GET('id');
    * 
    * @param {string} className class which stylesheet should be returned for
    * @param {string} property [optional] CSS property to return for given class
    * 
    * @returns {object|string|boolean} if <b>className</b> parameter is specified the stylesheet for given element will be returned. If <b>property</b> parameter is 
    * given too its value for given class will be returned. Otherwise, if no parameter is specified or no CSS rules were set for given classname <b>FALSE</b> will be returned
    */
    var getStyle = function(className, property) {
        var classes = document.styleSheets[0].rules || document.styleSheets[0].cssRules;
        for (var x = 0; x < classes.length; x++) {
            if (classes[x].selectorText === className) {
                var cssText = (classes[x].cssText) ? classes[x].cssText : classes[x].style.cssText;
                if (property) {
                    var indexOfProperty = cssText.indexOf(property),
                        index = 0,
                        value = '';
                
                    if (indexOfProperty > -1) {
                        index = indexOfProperty + property.length + 1;
                        while (index < cssText.length) {
                            var char = cssText.substring(index, index + 1);
                            if (char !== ';') {
                                value += char;
                            } else {
                                break;
                            }
                            index++;
                        }
                    }
                    
                    if (value.length > 0) {
                        return value.trim();
                    }
                }
                return cssText; 
            }
        }
        return false;
    };
    
    /**
    * This function shortens given string<br /><br />
    * 
    * Example:<br />
    * 
    *    SmartLibrary.shortenText('so nicht', 2);
    * 
    * @param {string} text string to shorten
    * @param {string} maxLength length of shortened string
    * 
    * @returns {string} the shortened string
    */
    var shortenText = function(text, maxLength) {
        var ret = text;
        if (ret && ret.length > maxLength) {
            ret = ret.substr(0,maxLength-3) + "...";
        }
        return ret;
    };
    
    var addClass = function(classname, el) {
        var index = 0,
            isNodeList = el.length && el.length > 1,
            elements = isNodeList ? el : [el],
            total = elements.length;
    
        while (index < total) {
            var element = elements[index];
            
            if (!hasClass(classname, element)) {
                element.className += " " + classname;
            }
            
            index++;
        }
        
        return isNodeList ? elements : el;
    };
    
    var removeClass = function(classname, el) {
        var index = 0,
            isNodeList = el.length && el.length > 1,
            elements = isNodeList ? el : [el],
            total = elements.length;
            
        while (index < total) {
            var element = elements[index];
                        
            if (hasClass(classname, element)) {
                var reg = new RegExp('(\\s|^)' + classname + '(\\s|$)');
                element.className = element.className.replace(reg, ' ');
            }
                        
            index++;
        }
        
        return isNodeList ? elements : el;
    };
    
    var hasClass = function(classname, element) {
        return !!element.className.match(new RegExp('(\\s|^)' + classname + '(\\s|$)'));
    };
    
    var setStyle = function(style, el) {
        var index = 0,
            isNodeList = el.length && el.length > 1,
            elements = isNodeList ? el : [el],
            total = elements.length,
            props = Object.keys(style),
            propsLength = props.length;
            
        while (index < total) {
            var element = elements[index],
                propsIndex = 0;
                        
            while (propsIndex < propsLength) {
                var prop = props[propsIndex];
                
                element.style[prop] = style[prop];
                
                propsIndex++;
            }
                        
            index++;
        }
        
        return isNodeList ? elements : el;
    };
    
    var loopCollection = function(collection, callback) {
        var index = -1,
            isObject = typeof collection === 'object',
            c = isObject ? Object.keys(collection) : collection,
            length = c.length;
    
        while (++index < length) {
            if (typeof callback === 'function') {
                callback(isObject ? c[index] : index, isObject ? collection[c[index]] : collection[index]);
            }
        }
        
        // Garbage collection
        index = isObject = c = length = null;
    };
            
    return {
        setListener: setListener,
        removeListener: removeListener,
        includeScript: includeScript,
        $_GET: $_GET,
        getStyle: getStyle,
        shortenText: shortenText,
        addClass: addClass,
        removeClass: removeClass,
        hasClass: hasClass,
        setStyle: setStyle,
        loopCollection: loopCollection
    };
    
}());

SmartLibrary.Logger = (function(window) {
   
    var _enabled = null;
    
    /**
    * This function enables/disables debugging<br /><br />
    * 
    * Example:<br />
    * 
    *    SmartLibrary.setEnabled(true);
    * 
    * @param {boolean} enable whether to enable debugging or not
    * 
    * @returns {undefined}
    */
    var setEnabled = function(enable) {
        _enabled = enable;
    };
    
    /**
    * This function outputs a message to the browser's console<br /><br />
    * 
    * Example:<br />
    * 
    *    log('main.js', 'outputting something...');
    * 
    * @param {string} file which JS file is outputting the message
    * @param {object|string|boolean} msg the output
    * 
    * @returns {boolean} whether the log could be outputted
    */
    var log = function(file, msg) {
        if (_isConsoleEnabled()) {
            if (typeof msg === 'string') {
                console.log("{{" + file + "}} :: " + msg);
                if (typeof window.fxm === 'function' && window.fxm.dbg && typeof window.fxm.dbg.log === 'function') {
                    fxm.dbg.log("{{" + file + "}} :: " + msg);
                }
            } else {
                console.log("{{" + file + "}} :: {{output below}} -->");
                console.log(msg);
                if (typeof window.fxm === 'function' && window.fxm.dbg && typeof window.fxm.dbg.log === 'function') {
                    fxm.dbg.log("{{" + file + "}} :: {{output below}} -->");
                    //fxm.dbg.log(JSON.stringify(msg));
                }
            }
            return true;
        }
        return false;
    };
    
    var _isConsoleEnabled = function() {
        // check if debugging is enabled
        if ("console" in window && window.console.log && typeof window.console.log === 'function' && _enabled) {
            return true;
        }
        return false;
    };
    
    return {
        setEnabled: setEnabled,
        log: log
    };
    
})(window);
    
SmartLibrary.Animation = (function() {
    
    var _useCss3 = false;
    
    /**
    * This function triggers a JQUERY animation on a HTML element<br /><br />
    * 
    * Example:<br />
    * 
    *    triggerAnimationById<br />
    *    (<br />
    *      &nbsp;&nbsp;'my-element-id', <br />
    *      &nbsp;&nbsp;{<br />
    *        &nbsp;&nbsp;&nbsp;&nbsp;left: 50,<br />
    *        &nbsp;&nbsp;&nbsp;&nbsp;top: 20<br />
    *      &nbsp;&nbsp;},<br />
    *      &nbsp;&nbsp;400, <br />
    *      &nbsp;&nbsp;function() {<br />
    *        &nbsp;&nbsp;&nbsp;&nbsp;// to do after animation<br />
    *      &nbsp;&nbsp;}<br />
    *    );
    * 
    * @param {string} id HTML element id attribute
    * @param {object} options CSS properties to animate
    * @param {number} speed how fast the animation should be
    * @param {function} callback [optional] callback function to execute once the animation is completed
    * @param {function} onProgress [optional] function to be called after each step of the animation
    * 
    * @returns {boolean} whether the animation could be triggered or not
    */
    var triggerById = function(id, options, speed, callback, onProgress) {
        if (_useCss3 && typeof move !== 'undefined') {
            if (!options && typeof callback === 'function') {
                return callback();
            }
            if (typeof onProgress === 'function') {
                onProgress(1, 1);
            }
            var m = {},
                ended = false;
        
        
            $.extend(m, move('#' + id));
            for (var attr in options) {
                m.set(attr, options[attr]);
            }
            m.duration(speed)
            m.end(function() {
                if (!ended) {
                    ended = true;
                    if (typeof callback === 'function') {
                        callback();
                    }
                    
                    // Garbage collection 
                    m = null;
                }
            });
            
            setTimeout(function() {
                // check if animation ended
                if (!ended && typeof callback === 'function') {
                    ended = true;
                    callback();
                    
                    // Garbage collection 
                    m = null;
                }
            }, speed);
            
            return true;
        } else if (typeof $ !== 'undefined' && speed > 0) {

            $('#' + id).stop(true, false, false).animate(options, {
                duration: speed,
                complete: callback,
                progress: onProgress
            });

            return true;
        } else if (speed === 0) {
            if (options) {
                $('#' + id).css(options);
            }
            setTimeout(function() {
                if (typeof onProgress === 'function') {
                    onProgress(100);
                }
                if (typeof callback === 'function') {
                    callback.call(id);
                }
            }, 20);
            
            return true;
        } else {
            return false;
        }
    };
    
    var setCss3 = function(enable) {
        _useCss3 = enable;
    };
    
    return {
        triggerById: triggerById,
        setCss3: setCss3
    };
    
})();

SmartLibrary.Navigation = (function(document) {
    
    var _blockRemote = false,
        _platform = null;
    
    var onKeyDown = null;
    var onKeysBlocked = null;
    var onMouseMove = null;
    var onKeyUp = null;
            
    var _ignoreKeyPress = function(e) {
        e.preventDefault();
        return false;
    };
        
    var _handleRemoteButtons = function(e) {   
        var $this = SmartLibrary.Navigation;
        if (!$this.isBlocked() && typeof $this.onKeyDown === 'function') {
            $this.onKeyDown(e, e.keyCode, getKeys());
        } else if (typeof $this.onKeysBlocked === 'function') {
            $this.onKeysBlocked(e, e.keyCode, getKeys());
        } else {
            e.preventDefault();
        }
        return false;
    };
    
    var _handleMouseMove = function(e) {
        var $this = SmartLibrary.Navigation;
        if (typeof $this.onMouseMove === 'function') {
            e.preventDefault();
            $this.onMouseMove(e);
        }
    };
    
    var _handleKeyUp = function(e) {
        var $this = SmartLibrary.Navigation;
        if (typeof $this.onKeyUp === 'function' && !$this.isBlocked()) {
            e.preventDefault();
            $this.onKeyUp(e.keyCode, getKeys());
        } else {
            e.preventDefault();
        }
    };
    
    /**
    * This function returns all key codes for given platform<br /><br />
    * 
    * Example:<br />
    * 
    *    getKeys('opera');
    * 
    * @param {string} platform from which platform the key codes should be returned
    * 
    * @returns {object} key codes
    */
    var getKeys = function(platform) {
        switch (platform || _platform) {
            case 'samsung' :
                var keys = new Common.API.TVKeyValue();
                return {
                    // navigation keys
                    VK_DOWN : keys.KEY_DOWN,
                    VK_UP : keys.KEY_UP,
                    VK_LEFT : keys.KEY_LEFT,
                    VK_RIGHT : keys.KEY_RIGHT,
                    // numeric keys
                    VK_0 : keys.KEY_0,
                    VK_1 : keys.KEY_1,
                    VK_2 : keys.KEY_2,
                    VK_3 : keys.KEY_3,
                    VK_4 : keys.KEY_4,
                    VK_5 : keys.KEY_5,
                    VK_6 : keys.KEY_6,
                    VK_7 : keys.KEY_7,
                    VK_8 : keys.KEY_8,
                    VK_9 : keys.KEY_9,
                    // playback keys
                    VK_PLAY : keys.KEY_PLAY,
                    VK_STOP : keys.KEY_STOP,
                    VK_PAUSE : keys.KEY_PAUSE,
                    VK_FAST_FWD : keys.KEY_FF,
                    VK_REWIND : keys.KEY_RW,
                    // other keys
                    VK_ENTER : keys.KEY_ENTER,
                    VK_INFO : keys.KEY_INFOLINK,
                    VK_BACK : keys.KEY_RETURN,
                    VK_BACK_SPACE : keys.KEY_RETURN
                };
            case 'tizen' :
                return {
                    // navigation keys
                    VK_DOWN : 40,
                    VK_UP : 38,
                    VK_LEFT : 37,
                    VK_RIGHT : 39,
                    // numeric keys
                    VK_0 : 48,
                    VK_1 : 49,
                    VK_2 : 50,
                    VK_3 : 51,
                    VK_4 : 52,
                    VK_5 : 53,
                    VK_6 : 54,
                    VK_7 : 55,
                    VK_8 : 56,
                    VK_9 : 57,
                    // playback keys
                    VK_PLAY : 415,
                    VK_STOP : 413,
                    VK_PAUSE : 19,
                    VK_FAST_FWD : 417,
                    VK_REWIND : 412,
                    // other keys
                    VK_ENTER : 13,
                    VK_INFO : 457,
                    VK_BACK : 10009,
                    VK_BACK_SPACE : 10009
                };
            case 'firetv' :
                return {
                    // navigation keys
                    VK_DOWN : (typeof VK_DOWN === 'undefined') ? 40 : VK_DOWN,
                    VK_UP : (typeof VK_UP === 'undefined') ? 38 : VK_UP,
                    VK_LEFT : (typeof VK_LEFT === 'undefined') ? 37 : VK_LEFT,
                    VK_RIGHT : (typeof VK_RIGHT === 'undefined') ? 39 : VK_RIGHT,
                    // playback keys
                    VK_PLAY_PAUSE : (typeof VK_PLAY_PAUSE === 'undefined') ? 179 : VK_PLAY_PAUSE,
                    VK_FAST_FWD : (typeof VK_FAST_FWD === 'undefined') ? 228 : VK_FAST_FWD,
                    VK_REWIND : (typeof VK_REWIND === 'undefined') ? 227 : VK_REWIND,
                    // other keys
                    VK_ENTER : (typeof VK_ENTER === 'undefined') ? 13 : VK_ENTER,
                    VK_MENU : (typeof VK_MENU === 'undefined') ? 18 : VK_MENU,
                    VK_BACK : (typeof VK_BACK === 'undefined') ? 27 : VK_BACK
                };
            case 'ee' :
                return {
                    // navigation keys
                    VK_DOWN : (typeof VK_DOWN === 'undefined') ? 40 : VK_DOWN,
                    VK_UP : (typeof VK_UP === 'undefined') ? 38 : VK_UP,
                    VK_LEFT : (typeof VK_LEFT === 'undefined') ? 37 : VK_LEFT,
                    VK_RIGHT : (typeof VK_RIGHT === 'undefined') ? 39 : VK_RIGHT,
                    // playback keys
                    VK_PLAY_PAUSE : (typeof VK_PLAY_PAUSE === 'undefined') ? 179 : VK_PLAY_PAUSE,
                    VK_FAST_FWD : (typeof VK_FAST_FWD === 'undefined') ? 228 : VK_FAST_FWD,
                    VK_REWIND : (typeof VK_REWIND === 'undefined') ? 227 : VK_REWIND,
                    // other keys
                    VK_ENTER : (typeof VK_ENTER === 'undefined') ? 13 : VK_ENTER,
                    VK_INFO : (typeof VK_INFO === 'undefined') ? 2500 : VK_INFO,
                    VK_BACK : (typeof VK_BACK === 'undefined') ? 27 : VK_BACK,
                    VK_BACK_SPACE : (typeof VK_BACK_SPACE === 'undefined') ? 461 : VK_BACK_SPACE
                };
            case 'lg_webos' :
                return {
                        // navigation keys
                        VK_DOWN : (typeof VK_DOWN === 'undefined') ? 40 : VK_DOWN,
                        VK_UP : (typeof VK_UP === 'undefined') ? 38 : VK_UP,
                        VK_LEFT : (typeof VK_LEFT === 'undefined') ? 37 : VK_LEFT,
                        VK_RIGHT : (typeof VK_RIGHT === 'undefined') ? 39 : VK_RIGHT,
                        // playback keys
                        VK_PLAY : (typeof VK_PLAY === 'undefined') ? 415 : VK_PLAY,
                        VK_STOP : (typeof VK_STOP === 'undefined') ? 413 : VK_STOP,
                        VK_PAUSE : (typeof VK_PAUSE === 'undefined') ? 19 : VK_PAUSE,
                        VK_FAST_FWD : (typeof VK_FAST_FWD === 'undefined') ? 417 : VK_FAST_FWD,
                        VK_REWIND : (typeof VK_REWIND === 'undefined') ? 412 : VK_REWIND,
                        // other keys
                        VK_ENTER : (typeof VK_ENTER === 'undefined') ? 13 : VK_ENTER,
                        VK_INFO : (typeof VK_INFO === 'undefined') ? 2500 : VK_INFO,
                        VK_BACK : (typeof VK_BACK === 'undefined') ? 461 : VK_BACK,
                        VK_BACK_SPACE : (typeof VK_BACK_SPACE === 'undefined') ? 8 : VK_BACK_SPACE
                    };
            case undefined :
            case 'opera'   : 
            case 'unknown' :
            default        :
                return {
                    // navigation keys
                    VK_DOWN : (typeof VK_DOWN === 'undefined') ? 40 : VK_DOWN,
                    VK_UP : (typeof VK_UP === 'undefined') ? 38 : VK_UP,
                    VK_LEFT : (typeof VK_LEFT === 'undefined') ? 37 : VK_LEFT,
                    VK_RIGHT : (typeof VK_RIGHT === 'undefined') ? 39 : VK_RIGHT,
                    // playback keys
                    VK_PLAY : (typeof VK_PLAY === 'undefined') ? 116 : VK_PLAY,
                    VK_STOP : (typeof VK_STOP === 'undefined') ? 117 : VK_STOP,
                    VK_PAUSE : (typeof VK_PAUSE === 'undefined') ? 118 : VK_PAUSE,
                    VK_FAST_FWD : (typeof VK_FAST_FWD === 'undefined') ? 417 : VK_FAST_FWD,
                    VK_REWIND : (typeof VK_REWIND === 'undefined') ? 412 : VK_REWIND,
                    // other keys
                    VK_ENTER : (typeof VK_ENTER === 'undefined') ? 13 : VK_ENTER,
                    VK_INFO : (typeof VK_INFO === 'undefined') ? 2500 : VK_INFO,
                    VK_BACK : (typeof VK_BACK === 'undefined') ? 461 : VK_BACK,
                    VK_BACK_SPACE : (typeof VK_BACK_SPACE === 'undefined') ? 8 : VK_BACK_SPACE,
                    // color keys
                    VK_RED: (typeof VK_RED === 'undefined') ? 82 : VK_RED,
                    VK_GREEN: (typeof VK_GREEN === 'undefined') ? 71 : VK_GREEN,
                    VK_BLUE: (typeof VK_BLUE === 'undefined') ? 66 : VK_BLUE,
                    VK_YELLOW: (typeof VK_YELLOW === 'undefined') ? 89 : VK_YELLOW,
                    // numeric keys
                    VK_0 : (typeof VK_0 === 'undefined') ? 48 : VK_0,
                    VK_1 : (typeof VK_1 === 'undefined') ? 49 : VK_1,
                    VK_2 : (typeof VK_2 === 'undefined') ? 50 : VK_2,
                    VK_3 : (typeof VK_3 === 'undefined') ? 51 : VK_3,
                    VK_4 : (typeof VK_4 === 'undefined') ? 52 : VK_4,
                    VK_5 : (typeof VK_5 === 'undefined') ? 53 : VK_5,
                    VK_6 : (typeof VK_6 === 'undefined') ? 54 : VK_6,
                    VK_7 : (typeof VK_7 === 'undefined') ? 55 : VK_7,
                    VK_8 : (typeof VK_8 === 'undefined') ? 56 : VK_8,
                    VK_9 : (typeof VK_9 === 'undefined') ? 57 : VK_9
                };
        }
        return [];
    };
    
    /**
    * This function blocks/unblocks the navigation<br /><br />
    * 
    * Example:<br />
    * 
    *    doBlock(true);
    * 
    * @param {boolean} block whether to block navigation or not
    * 
    * @returns {undefined}
    */
    var doBlock = function(block) {
        _blockRemote = block;
    };
    
    /**
    * This function returns the current blocking status<br /><br />
    * 
    * Example:<br />
    * 
    *    SmartLibrary.Navigation.isBlocked();
    * 
    * @returns {boolean} whether the navigation is currently blocked or not
    */
    var isBlocked = function() {
        return _blockRemote;
    };
    
    /**
    * This function initializes the <b>Navigation</b> module<br /><br />
    * 
    * Example:<br />
    * 
    *    SmartLibrary.Navigation.init('my-platform');
    *    
    * @param {string} platform name of platform to initialize
    * 
    * @returns {undefined}
    */
    var init = function(platform) {
        _platform = platform;
    };
    
    // handling remote control buttons
    SmartLibrary.setListener(document, 'keydown', _handleRemoteButtons, false);
    // ignoring regular keypress event selective
    SmartLibrary.setListener(document, 'keypress', _ignoreKeyPress, true);
    // handling mouse move
    SmartLibrary.setListener(document.body, 'mousemove', _handleMouseMove, true);
    // handling mouse up
    SmartLibrary.setListener(document, 'keyup', _handleKeyUp, false);

    return {
        onKeyDown: onKeyDown,
        onKeyUp: onKeyUp,
        onMouseMove: onMouseMove,
        onKeysBlocked: onKeysBlocked,
        doBlock: doBlock,
        isBlocked: isBlocked,
        getKeys: getKeys,
        init: init
    };
    
})(document);

SmartLibrary.Ajax = (function() {
    
    var _queue = [];
    
    var _createXMLHttpRequest = function() {
        try { return new XMLHttpRequest();                    } catch(e){};
        try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch(e){};
        try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch(e){};
        try { return new ActiveXObject('Msxml2.XMLHTTP');     } catch(e){};
        try { return new ActiveXObject('Microsoft.XMLHTTP');  } catch(e){};
        return false;
    };
    
    var _setXHR = function(xhr, type, url, params, onRequestFinished, onRequestError, cast) {
        var params_string = _joinParameters(params),
            destination = (type !== 'GET') ? url : (url + ((params && params !== undefined && params.length > 0) ? "?" + params_string : ''));
    
        xhr.open(type, destination, true);
        _setXHRHeaders(xhr, type, params_string);
        xhr.onreadystatechange = _onXHRreadystatechange;
        xhr.onerror = _onXHRerror;
        _sendXHR(xhr, type, params_string, onRequestFinished, onRequestError, cast);
        
        // Garbage collection
        params_string = destination = null;
    };
    
    var _joinParameters = function(params) {
        var stringParams = '';
        for (var attr in params) {
            if (stringParams.length === 0) {
                stringParams += attr + '=' + encodeURIComponent(params[attr]);
            } else {
                stringParams += '&' + attr + '=' + encodeURIComponent(params[attr]);
            }
        }
        return stringParams;
    };
    
    var _setXHRHeaders = function(xhr, type, params) {
        if (type !== 'GET') {
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.setRequestHeader("Content-length", params.length);
            xhr.setRequestHeader("Connection", "close");
        } else if (!this.saveCache) {
            xhr.setRequestHeader('If-Modified-Since', 'Sat, 1 Jan 2000 00:00:00 GMT');
        }
    };
    
    var _onXHRreadystatechange = function() {
        if (this.status === 200 && this.readyState === 4) {
                        
            var request = _getXHRfromQueue(this),
                onRequestFinished = request.onRequestFinished,
                onRequestError = request.onRequestError,
                cast = request.cast;
                    
            if (typeof onRequestFinished === 'function') {
                try {
                    var result = this.responseText;
                    switch (cast) {
                        case 'xml' :
                            result = this.responseXML;
                            break;
                        case 'json' :
                            result = eval('(' + this.responseText + ')');
                            break;
                        default :
                            result = this.responseText;
                            break;
                    }
                    onRequestFinished(result);
                } catch (e) {
                    if (typeof onRequestError === 'function') {
                        onRequestError();
                    }
                }

                // Garbage collection
                request = cast = onRequestFinished = result = null;
            }
            
            _removeXHRfromQueue(this);
            
        }
    };
    
    var _onXHRerror = function() {
        var request = _getXHRfromQueue(this),
                onRequestError = request.onRequestError;
        
        if (typeof onRequestError === 'function') {
            onRequestError();
        }
        
        _removeXHRfromQueue(this);
        
        // Garbage collection
        request = onRequestError = null;
    };
    
    var _sendXHR = function(xhr, type, params, onRequestFinished, onRequestError, cast) {
        _addXHRtoQueue({
            xhr: xhr,
            onRequestFinished: onRequestFinished,
            onRequestError: onRequestError,
            cast: cast
        });
        xhr.send((type !== 'GET') ? ((params && params.length > 0 && typeof params !== undefined) ? params : '') : null);
    };
    
    var _addXHRtoQueue = function(request) {
        _queue.push(request);
    };
    
    var _getXHRfromQueue = function(xhr) {
        var index = 0,
            request = false;
        while (index <= _queue.length - 1) {
            var queue_request = _queue[index];
            if (queue_request.xhr === xhr) {
                request = queue_request;
                break;
            }
            index++;
        }
        return request;
    };
    
    var _removeXHRfromQueue = function(xhr) {
        var index = 0,
            success = false;
        while (index <= _queue.length - 1) {
            var queue_request = _queue[index];
            if (queue_request.xhr === xhr) {
                _queue.splice(index, 1);
                success = true;
                break;
            }
            index++;
        }
        return success;
    };
    
    /**
    * This function performs an AJAX request<br />
    * 
    * Example:<br /><br />
    * 
    *    request<br />
    *    (<br />
    *      &nbsp;&nbsp;'GET',<br /> 
    *      &nbsp;&nbsp;'http://server-url.com/',<br />
    *      &nbsp;&nbsp;{<br />
    *          &nbsp;&nbsp;&nbsp;&nbsp;a: 1,<br />
    *          &nbsp;&nbsp;&nbsp;&nbsp;b: 2<br />
    *      &nbsp;&nbsp;},<br />
    *      &nbsp;&nbsp;function(response) {<br />
    *        &nbsp;&nbsp;&nbsp;&nbsp;// to do when request completed<br />
    *      &nbsp;&nbsp;},<br />
    *      &nbsp;&nbsp;'json',<br />
    *      &nbsp;&nbsp;function() {<br />
    *        &nbsp;&nbsp;&nbsp;&nbsp;// to do when request starts<br />
    *      &nbsp;&nbsp;},<br />
    *      &nbsp;&nbsp;function() {<br />
    *        &nbsp;&nbsp;&nbsp;&nbsp;// to do in case request fails<br />
    *      &nbsp;&nbsp;},<br />
    *    );
    * 
    * @param {string} type HTTP request type (GET, POST, PUT, DELETE)
    * @param {string} url where to make the request
    * @param {object} params [optional] parameters to be sent to the server
    * @param {function} onRequestFinished [optional] callback function to execute once the request is completed
    * @param {string} cast [optional] cast response from the server (xml, json)
    * @param {function} onRequestStart [optional] callback function to execute inmediately after the request is sent to the server
    * @param {function} onRequestError [optional] callback function to execute if the request fails
    * 
    * @returns {undefined}
    */
    var request = function(type, url, params, onRequestFinished, cast, onRequestStart, onRequestError) {
        
        if (typeof onRequestStart === 'function') {
            onRequestStart();
        }
        
        var xhr = _createXMLHttpRequest();
        _setXHR(xhr, type, url, params, onRequestFinished, onRequestError, cast);
    };
        
    return {
        request: request
    };
    
})();

SmartLibrary.Connection = (function(window) {
    
    var _connected = true,
        _enabled = false,
        _path = null,
        _delay = null,
        _timeout = null,
        _requestTimeout = null,
        _timeoutExceeded = null,
        _onNetworkStatusChanged = null,
        _autoUpdate = true;
        
    var _performCheckRequest = function() {
        if (_requestTimeout) {
            window.clearTimeout(_requestTimeout);
        }
        window.SmartLibrary.Ajax.request(
            'GET', 
            _path, 
            '', 
            function() {
                if (!_timeoutExceeded) {
                    _setStatusConnection(true);
                } else {
                    _resetRequestTimeout();
                }
            },
            false,
            function() {},
            function() {
                if (!_timeoutExceeded) {
                    _setStatusConnection(false);
                } else {
                    _resetRequestTimeout();
                }
            }
        );
        _requestTimeout = window.setTimeout(function() {
            _setStatusConnection(false);
            _timeoutExceeded = true;
        }, _delay);
    };
    
    var _resetRequestTimeout = function() {
        _timeoutExceeded = false;
        if (_delay) {
            _requestTimeout = window.setTimeout(_performCheckRequest, _delay);
        } else {
            _performCheckRequest();
        }
    };
    
    var _setStatusConnection = function(success) {
        if (typeof _onNetworkStatusChanged === 'function' && success !== _connected)  {
            _onNetworkStatusChanged(success);
        }
        _connected = success;
        if (!_timeoutExceeded) {
            window.clearTimeout(_requestTimeout);
            if (_autoUpdate) {
                if (_delay) {
                    _requestTimeout = window.setTimeout(_performCheckRequest, _delay);
                } else {
                    _performCheckRequest();
                }
            }
        }
    };
    
    /**
    * This function starts the network connection checking<br /><br />
    * 
    * Example:<br />
    * 
    *    check<br />
    *    (<br />
    *    &nbsp;&nbsp;'ajax/ping.php',<br />
    *    &nbsp;&nbsp;5000<br />
    *    &nbsp;&nbsp;2000<br />
    *    }
    * 
    * @param {string} path path to any file to reach (the contents will not be handled anyhow, as this is just to check that this URL is reachable)
    * @param {function} callback function which will handle changes on network status
    * @param {number} timeout request timeout (in milliseconds)
    * @param {number} requestDelay delay after which a new request will be performed (in milliseconds)
    * 
    * @returns {boolean} whether the network checking could be initiated or not
    */
    var check = function(path, onNetworkStatusChange, timeout, requestDelay) {
        if (!_enabled) {
            _enabled = true;
            _timeoutExceeded = false;
            _path = path;
            _onNetworkStatusChanged = onNetworkStatusChange;
            _delay = (typeof requestDelay === 'number') ? requestDelay : null;
            _timeout = timeout;
            _performCheckRequest();
            return true;
        }
        return false;
    };
    
    /**
    * This function returns whether Internet connection is on or not<br /><br />
    * 
    * Example:<br />
    * 
    *    isConnected()
    * 
    * @returns {boolean} whether Internet connection is on or not
    */
    var isConnected = function() {
        return _connected;
    };
    
    /**
    * This function enables/disables this module<br /><br />
    * 
    * Example:<br />
    * 
    *    setEnabled(false)
    * 
    * @param {boolean} enabled whether to enable module or not
    * 
    * @returns {undefined}
    */
    var setEnabled = function(enabled) {
        _enabled = enabled;
    };
    
    var setAutoUpdate = function(enabled) {
        _autoUpdate = enabled;
    };
    
    var pause = function() {
        if (_enabled && _requestTimeout) {
            clearTimeout(_requestTimeout);
        }
    };
    
    var resume = function() {
        if (_enabled) {
            _performCheckRequest();
        }
    };
            
    return {
        check: check,
        setEnabled: setEnabled,
        isConnected: isConnected,
        pause: pause, 
        resume: resume,
        setAutoUpdate: setAutoUpdate
    };
    
})(window);