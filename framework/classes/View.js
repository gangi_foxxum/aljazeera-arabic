function View(confObj) {
    
    var _self = this;
    
    this.id = null; // view identifier
    this.containerID = null; // container identifier of this view (references HTML element)
    
    this.onLoad = null; // load function
    this.onUnload = null; // unload function
    
    this.init = null; // initialization function
    this.deinit = null; // deinitialization function
    
    this.onHideStart = null; // callback function to execute when view starts to be hidden
    this.onHideOptions = {}; // object containing CSS properties to animate on calling onHide() function
    this.onHideComplete = null; // callback function to execute when view has been hidden
    
    this.onShowStart = null; // callback function to execute when view starts to be visible
    this.onShowOptions = {}; // object containing CSS properties to animate on calling onShow() function
    this.onShowComplete = null; // callback function to execute when view has been shown
    
    var _construct = function(confObj) {
        for (var attr in confObj) {
            _self[attr] = confObj[attr];
        }
    };

    _construct(confObj);
    
}