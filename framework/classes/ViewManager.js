function ViewManager() {
    
    var _views = [], // views list
        _currentView = null; // current view

    /**
    * This function adds a view to the collection<br /><br />
    * 
    * Example:<br />
    * 
    *    viewManager.addView(new View({}));
    * 
    * @param {View} view view to add
    * 
    * @returns {View|boolean} included view if success. Otherwise false.
    */
    this.addView = function(view) {
        if (view instanceof View) {
            _views.push(view);
            return view;
        }
        return false;
    };
    
    /**
    * This function removes a view from the collection<br /><br />
    * 
    * Example:<br />
    * 
    *    viewManager.removeView(view);
    * 
    * @param {View} view view to remove
    * 
    * @returns {boolean} whether the view could be removed or not
    */
    this.removeView = function(view) {
        var index = _views.indexOf(view);
        if (index > -1) {
            _views.splice(index, 1);
            return true;
        }
        return false;
    };
    
    /**
    * This function removes all views from the collection<br /><br />
    * 
    * Example:<br />
    * 
    *    viewManager.deinit();
    *  
    * @returns {undefined}
    */
    this.deinit = function() {
        _views = [];
        _currentView = null;
    };
    
    /**
    * This function returns the current view<br /><br />
    * 
    * Example:<br />
    * 
    *    var currView = viewManager.getCurrentView();
    *  
    * @returns {View} current view
    */
    this.getCurrentView = function() {
        return _currentView;
    };
    
    /**
    * This function returns a view by its ID attribute<br /><br />
    * 
    * Example:<br />
    * 
    *    var currView = viewManager.getViewById('Main');
    *    
    * @param {string} id desired view ID
    *  
    * @returns {View|boolean} desired view if exists. Otherwise false.
    */
    this.getViewById = function(id) {
        var index = 0;
        
        while (index <= _views.length - 1) {
            if (_views[index].id === id) {
                return _views[index];
            }
            index++;
        }
        
        return false;
    };
    
    /**
    * This function sets the current view<br /><br />
    * 
    * Example:<br />
    * 
    *    viewManager.setCurrentView(view);
    *  
    * @param {View} view next current view
    * 
    * @returns {boolean} whether it could be set or not
    */
    this.setCurrentView = function(view) {
        if (view instanceof View) {
            _currentView = view;
            return true;
        }
        return false;
    };
    
    /**
    * This function returns the view list<br /><br />
    * 
    * Example:<br />
    * 
    *    var views = viewManager.getViews();
    *  
    * @returns {Array} the view list
    */
    this.getViews = function() {
        return _views;
    };
        
}