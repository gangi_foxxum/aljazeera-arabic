function Carousel(confObj) {
    
    this.nav = null;
    this.onMove = null;
    this.noMoveTimeout = null;
    this.noMoveTimeoutDuration = 50;
    this.onFocus = null;
    this.onBlur = null;
    this.containerId = null;
    this.itemIdPrefix = null;
    this.onAddingItem = null;
    this.onRemovingItem = null;
    this.onItemSelected = null;
    this.focusAfterAnimation = null;
    this.currDivNumber = 0;
    this.itemsPerPage = 0;
    this.maxVisibleItems = 0;
    this.isMatrix = false;
    this.speed = 400;
    this.itemsArray = [];
    this.carouselInnerClass = 'carouselInner';
    this.carouselTitleClass = 'carouselTitle';
    this.carouselWrapperClass = 'carouselWrapper';
    this.carouselClass = 'carousel';
    this.itemHtmlTemplate = '<li id="{{itemIdPrefix}}{{itemId}}"></li>';
    this.noLoadedClass = 'not-loaded';
    this.loadItemContent = true;
    this.active = true;
    this.horizontal = true;
    
    var self = this,
        _carouselPx = 0,
        _totalItems = 0,
        _focusOnLimit = false,
        _visibleItems = [],
        _moveTimeout = null,
        _moving = false,
        _keepMoving = false,
        _speed = 0,
        _itemCopy = null,
        _container = null,
        _holdingKeyTimeout = false,
        _holdingKeyTimeoutDuration = 100;

    var _init = function(confObj) {
        var carouselHTML = _createCarouselHTML(confObj);
        if (confObj.after) {
            $('#' + confObj.after).after(carouselHTML);
        } else if (confObj.clear) {
            $('#' + confObj.parent).html(carouselHTML);
        } else {
            $('#' + confObj.parent).append(carouselHTML);
        }
        _speed = self.speed;
        if (self.horizontal) {
            move('#' + self.containerId)
                .x(0)
                .duration(0)
                .end();
        } else {
            move('#' + self.containerId)
                .y(0)
                .duration(0)
                .end();
        }
        _setVisibleItems(0);
        setTimeout(_onInitialized, 300);
        
        // Garbage collection
        carouselHTML = null;
    };
    
    var _onInitialized = function() {
        var item = $('#' + confObj.itemIdPrefix + 1),
            fontSize = parseFloat($("body").css("font-size"));
        
        _itemCopy = item.clone(true);
        _container = $('#' + self.containerId);

        if (self.isMatrix) {
            _carouselPx = {
                width: item.outerWidth(true),
                height: item.outerHeight(true)
            };
            _container.css({
                'width':  (self.itemsPerPage * _carouselPx.width) + 'px',
                'height': ((Math.round(self.itemsArray.length / self.itemsPerPage) + 1) * _carouselPx.height) + 'px'
            });
        } else {
            //_carouselPx = self.horizontal ? item.outerWidth(true) : item.outerHeight(true);
            _carouselPx = self.horizontal ? item.outerWidth(true) / fontSize : item.outerHeight(true) / fontSize;
            //console.log(_carouselPx + "em");
            _container.css((self.horizontal ? 'width' : 'height'), (confObj.itemsArray.length * _carouselPx) + 'em');
        }
        
        _addNavItems(confObj.itemsArray, true);
        
        _container.on('click', 'li', function() {
            if (typeof self.onItemSelected === 'function') {
                self.onItemSelected.call(this, self.itemsArray[self.currDivNumber]);
            }
        });
        _container.on('mouseenter', 'li', _onItemHovered);

        if (typeof confObj.onInitialized === 'function') {
            confObj.onInitialized.call(self);
        }

        confObj.onInitialized = item = null;
    };
    
    var _onItemHovered = function(e) {
        var item = $(this),
            pos = parseInt(item.attr('data-pos'));

        e.stopPropagation();
        
        if (self.isMatrix) {
            var posInVisibleItems = _visibleItems.indexOf(pos);
            
            _container.children().removeClass('focus');
            
            if (posInVisibleItems === -1 && pos > _visibleItems[_visibleItems.length - 1]) { // moving down
                _focusOnLimit = true;
                if (_focusOnLimit && pos < self.itemsArray.length) {
                    _setVisibleItems(_visibleItems[0] + self.itemsPerPage);
                }

                _moveCarousel('down', pos);
            } else if (posInVisibleItems === -1 && pos < _visibleItems[0]) { // moving up
                _focusOnLimit = true;
                if (_focusOnLimit && _visibleItems.length > 0) {
                    _setVisibleItems(_visibleItems[0] - self.itemsPerPage);
                }

                _moveCarousel('up', pos);
            }
            
            // Garbage collection
            posInVisibleItems = null;
        } else {
            if (pos > self.currDivNumber && self.itemsArray.length > self.itemsPerPage) { // moving right
                _focusOnLimit = pos > _visibleItems[_visibleItems.length - 2] && _visibleItems[_visibleItems.length - 1] < self.itemsArray.length;
                if (_focusOnLimit && _visibleItems.length > 0 && pos < self.itemsArray.length) {
                    _setVisibleItems(_visibleItems[0] + 1);
                }

                _moveCarousel((self.horizontal ? 'right' : 'down'), pos);
            } else if (pos < self.currDivNumber && self.itemsArray.length > self.itemsPerPage) { // moving left
                _focusOnLimit = pos < _visibleItems[1] || pos - 1 < _visibleItems[0];
                if (_focusOnLimit && _visibleItems.length > 0) {
                    _setVisibleItems(_visibleItems[0] - 1);
                }

                _moveCarousel((self.horizontal ? 'left' : 'up'), pos);
            }        
        }
        
        // Garbage collection
        item = pos = null;
    };
    
    var _setVisibleItems = function(startPos) {
        if (startPos < 0 || startPos > self.itemsArray.length) {
            return;
        }
        var index = 0,
            limit = self.itemsPerPage,
            itemsArrayLength = self.itemsArray.length;
        
        if (limit > 0) {
            _visibleItems = [];
            while (index < limit && startPos < itemsArrayLength) {
                _visibleItems.push(startPos);
                startPos++;
                index++;
            }
        }
        
        // Garbage collection
        index = limit = itemsArrayLength = null;
    };
    
    var _createCarouselHTML = function(confObj) { 
        var itemsArray = confObj.itemsArray,
            carouselPx = (confObj.itemsArray.length * _carouselPx) + 'px',
            html = [];

            html.push('<div id="' + confObj.carouselWrapperId + '" class="' + self.carouselWrapperClass + '">' + 
                    
                        '<div class="' + self.carouselTitleWrapperClass + '">' +
                        
                            '<p class="' + self.carouselTitleClass + '">' + (confObj.carouselTitle || '') + '</p>' + 
                        
                        '</div>' +

                        '<div class="' + self.carouselInnerClass + '">' + 
                            
                            '<ul id="' + confObj.containerId + '" class="' + self.carouselClass + '" style="position: absolute; ' + (self.horizontal ? 'width' : 'height') + ': ' + carouselPx + '">');

                                html.push(_getItemsHtml(itemsArray));

            html.push('</ul>' + 

                        '</div>' +

                    '</div>');

        return html.join('');
    };
    
    var _getItemsHtml = function(itemsArray, pos) {
        var html = [],
            index = 0,
            startAt = typeof pos !== 'undefined' ? pos : _totalItems,
            prop = Object.keys(self);
        
        while (index < itemsArray.length) {

            var itemHtmlTemplate = self.itemHtmlTemplate,
                prop_index = 0,
                endPosLiTag = itemHtmlTemplate.indexOf('>'),
                dataPosString = ' data-pos="' + startAt + '"';
            
            itemHtmlTemplate = [itemHtmlTemplate.slice(0, endPosLiTag), dataPosString, itemHtmlTemplate.slice(endPosLiTag)].join('');
            
            while (prop_index < prop.length) {
                var needle = '{{' + prop[prop_index] + '}}',
                    position = itemHtmlTemplate.indexOf(needle),
                    replace = self[prop[prop_index]];
            
                if (position > -1) {
                    itemHtmlTemplate = itemHtmlTemplate.replace(new RegExp(needle, 'g'), replace);
                }
                prop_index++;
            }
            
            itemHtmlTemplate = itemHtmlTemplate.replace(new RegExp('{{itemId}}', 'g'), startAt);

            html.push(itemHtmlTemplate);
            index++;
            startAt++;
        }
                        
        return html.join('');
    };
    
    var _onMove = function(dir) {
        if (_holdingKeyTimeout) {
            clearTimeout(_holdingKeyTimeout);
        }
        
        switch (dir) {
            case 'up' :
                _moveCarouselUp();
                break;
            case 'down' :
                _moveCarouselDown();
                break;
            case 'left' :
                _moveCarouselLeft();
                break;
            case 'right' :
                _moveCarouselRight();
                break;
        }
        
        _holdingKeyTimeout = setTimeout(function() {
            _keepMoving = false;
            self.speed = _speed;
        }, _holdingKeyTimeoutDuration);
    };
    
    var _moveCarouselLeft = function(forceMove) {
        if ((!_moving || !_keepMoving) && self.currDivNumber > 0 && (self.horizontal || (typeof forceMove === 'boolean' && forceMove))) {
            if (self.isMatrix && self.currDivNumber % self.itemsPerPage === 0 && self.currDivNumber < self.itemsArray.length) {
                if (typeof self.onMove === 'function') {
                    self.onMove.call(self, (self.horizontal ? 'right' : 'down'));
                }
                return;
            }
            var pos = self.currDivNumber;

            ////console.log("_moveCarouselLeft. pos = " + pos);
            _focusOnLimit = pos < _visibleItems[1] || pos - 1 < _visibleItems[0];
            if (_focusOnLimit && _visibleItems.length > 0) {
                _setVisibleItems(_visibleItems[0] - 1);
            }
            _keepMoving = true;

            if (pos > 0) {
                _moveCarousel((self.horizontal ? 'left' : 'up'), pos - 1);
            } else if (typeof self.onMove === 'function') {
                self.onMove.call(self, (self.horizontal ? 'left' : 'up'));
            }
        } else {
            // key was not released yet
            ////console.log("key was not released yet");
            _keepMoving = true;
            self.speed = _speed * 1.2;
            
            if (typeof self.onMove === 'function') {
                self.onMove.call(self, (typeof forceMove === 'boolean' && forceMove) ? 'up' : 'left');
            }
        }
    };
    
    var _moveCarouselRight = function(forceMove) {
        ////console.log("_moveCarouselRight. _moving = " + _moving + ", _keepMoving = " + _keepMoving);
        if ((!_moving || !_keepMoving) && self.currDivNumber < self.itemsArray.length - 1 && (self.horizontal || (typeof forceMove === 'boolean' && forceMove))) {
            ////console.log(self.currDivNumber + 1 % self.itemsPerPage);
            if (self.isMatrix && (self.currDivNumber + 1) % self.itemsPerPage === 0 && self.currDivNumber > 0) {
                if (typeof self.onMove === 'function') {
                    self.onMove.call(self, (self.horizontal ? 'right' : 'down'));
                }
                return;
            }
            var pos = self.currDivNumber,
                nextPos = pos + 1;

            _focusOnLimit = pos > _visibleItems[_visibleItems.length - 2] && _visibleItems[_visibleItems.length - 1] < self.itemsArray.length;
            if (_focusOnLimit && _visibleItems.length > 0 && nextPos < self.itemsArray.length) {
                ////console.log("update setVisibleElems. startAt = " + (_visibleItems[0] + 1));
                _setVisibleItems(_visibleItems[0] + 1);
            }
            _keepMoving = true;
            ////console.log("_moveCarouselRight (1) pos = " + pos + ", _focusOnLimit = " + _focusOnLimit);

            if (nextPos < self.itemsArray.length) {
                ////console.log("movingRight (2) pos = " + pos + ", _focusOnLimit = " + _focusOnLimit + ", last visible item: " + _visibleItems[_visibleItems.length - 1] + ", self.itemsArray.length = " + self.itemsArray.length);
                _moveCarousel((self.horizontal ? 'right' : 'down'), nextPos);
            } else if (typeof self.onMove === 'function') {
                self.onMove.call(self, (self.horizontal ? 'right' : 'down'));
            }
        } else {
            // key was not released yet
            _keepMoving = true;
            self.speed = _speed * 1.2;
            ////console.log("key was not released yet! increase self.speed to : " + self.speed);
            
            if (typeof self.onMove === 'function') {
                self.onMove.call(self, (typeof forceMove === 'boolean' && forceMove) ? 'down' : 'right');
            }
        }
    };
    
    var _moveCarouselUp = function() {
        ////console.log("moveCarouselUp");
        if (self.isMatrix) {
            if ((!_moving || !_keepMoving) && self.currDivNumber - self.itemsPerPage >= 0) {
                var pos = self.currDivNumber,
                    nextPos = pos - self.itemsPerPage;

                _focusOnLimit = true;
                if (_focusOnLimit && _visibleItems.length > 0) {
                    _setVisibleItems(_visibleItems[0] - self.itemsPerPage);
                }
                _keepMoving = true;

                if (pos > 0) {
                    _moveCarousel('up', nextPos);
                } else if (typeof self.onMove === 'function') {
                    self.onMove.call(self, 'up');
                }
            } else {
                _keepMoving = true;
                self.speed = _speed * 1.2;

                if (typeof self.onMove === 'function') {
                    self.onMove.call(self, 'up');
                }
            }
        } else {
            if (self.horizontal) {
                //_moving = true;
                if (typeof self.onMove === 'function') {
                    self.onMove.call(self, 'up');
                } 
            } else {
                _moveCarouselLeft(true);
            }
        }
    };
    
    var _moveCarouselDown = function() {
        ////console.log("moveCarouselDown");
        if (self.isMatrix) {
            if ((!_moving || !_keepMoving) && self.currDivNumber + self.itemsPerPage < self.itemsArray.length) {
                var pos = self.currDivNumber,
                    nextPos = pos + self.itemsPerPage;

                _focusOnLimit = true;
                if (_focusOnLimit && nextPos < self.itemsArray.length) {
                    _setVisibleItems(_visibleItems[0] + self.itemsPerPage);
                }
                _keepMoving = true;

                if (nextPos < self.itemsArray.length) {
                    _moveCarousel('down', nextPos);
                } else if (typeof self.onMove === 'function') {
                    self.onMove.call(self, (self.horizontal ? 'right' : 'down'));
                }
            } else {
                // key was not released yet
                _keepMoving = true;
                self.speed = _speed * 1.2;

                if (typeof self.onMove === 'function') {
                    self.onMove.call(self, 'down');
                }
            }
        } else {
            if (self.horizontal) {
                //_moving = true;
                if (typeof self.onMove === 'function') {
                    self.onMove.call(self, 'down');
                } 
            } else {
                _moveCarouselRight(true);
            }
        }
    };
    
    var _moveCarousel = function(dir, pos, callback) {
        if (typeof self.onMove === 'function') {
            self.onMove.call(self, dir);
        }
        self.currDivNumber = pos;
        $('#' + self.containerId).children().removeClass('focus');
        if (typeof self.onBlur === 'function') {
            self.onBlur();
        }
        ////console.log("focusOnLimit = " + _focusOnLimit + ", self.speed = " + self.speed);
        if (_focusOnLimit) {
            _moving = true;
            if (!self.focusAfterAnimation) {
                $('#' + self.itemIdPrefix + pos).addClass('focus');
            }
            _addTemporaryItem(dir);
            _animateCarousel(dir, callback);
        } else if (typeof callback === 'function') {
            callback();
        } else {
            _moving = false;
            $('#' + self.itemIdPrefix + pos).addClass('focus');
            if (typeof self.onFocus === 'function') {
                self.onFocus.call(self, self.itemsArray[self.currDivNumber]);
            }
        }
    };
    
    var _animateCarousel = function(dir, callback) {
        var animationEnded = false;
        ////console.log("_animateCarousel. dir = " + dir + ", left = " + (- _visibleItems[0] * _carouselWidth));
        if (self.noMoveTimeout) {
            clearTimeout(self.noMoveTimeout);
        }
        self.noMoveTimeout = setTimeout(function() {
            ////console.log("self.noMoveTimeout end! keepMoving = " + _keepMoving + ", animationEnded = " + animationEnded);
            if (typeof self.onFocus === 'function') {
                self.onFocus.call(self, self.itemsArray[self.currDivNumber]);
            }
            if (_keepMoving) {
                _moving = false;
                if ((dir === 'left' || dir === 'up') && self.currDivNumber > 0) {
                    if (self.isMatrix) {
                        _moveCarouselUp();
                    } else {
                        _moveCarouselLeft(true);
                    }
                } else if ((dir === 'right' || dir === 'down') && self.currDivNumber < self.itemsArray.length - 1) {
                    if (self.isMatrix) {
                        _moveCarouselDown();
                    } else {
                        _moveCarouselRight(true);
                    }
                } else {
                    _endAnimation(dir, callback);
                }
            } else if (animationEnded) {
                _endAnimation(dir, callback);
            }
        }, (self.speed > 0 ? self.speed : 0));
        if (self.speed > 0) {
            var fontSize = parseFloat($('body').css('font-size')),
                firstVisibleItem = !self.isMatrix ? _visibleItems[0] : parseInt(_visibleItems[0] / self.itemsPerPage),
                distance = (!self.isMatrix) ? -_visibleItems[0] * _carouselPx * fontSize : ((dir === 'right' || dir === 'left') ? -firstVisibleItem * _carouselPx.width * fontSize : -firstVisibleItem * _carouselPx.height * fontSize),
                speed = self.speed;

            ////console.log("distance = " + distance);

            if ((self.horizontal && !self.isMatrix) || (self.isMatrix && (dir === 'right' || dir === 'left'))) {
                move('#' + self.containerId)
                    .x(distance)
                    .duration(speed)
                    .end(function() {
                        animationEnded = true;
                        if (!_keepMoving || (self.currDivNumber === 0 || self.currDivNumber === self.itemsArray.length - 1)) {
                            _endAnimation(dir, callback);
                        }
                    });
            } else if ((!self.horizontal && !self.isMatrix) || (self.isMatrix && (dir === 'up' || dir === 'down'))) {
                move('#' + self.containerId)
                    .y(distance)
                    .duration(speed)
                    .end(function() {
                        animationEnded = true;
                        if (!_keepMoving || (self.currDivNumber === 0 || self.currDivNumber === self.itemsArray.length - 1)) {
                            _endAnimation(dir, callback);
                        } 
                    });
            }
            
            // Garbage collection
            firstVisibleItem = distance = speed = null;
        } else {
            $('#' + self.containerId).css((self.horizontal ? 'left' : 'top'), - (_visibleItems[0] * _carouselPx) + 'px');
            if (!_keepMoving || self.currDivNumber === 0 || self.currDivNumber === self.itemsArray.length - 1) {
                _endAnimation(dir, callback);
            } 
        }
    };
    
    var _endAnimation = function(dir, callback) {
        ////console.log("_endAnimation");
        var d = dir;
        if (self.speed > 0) {
            if (_moveTimeout) {
                clearTimeout(_moveTimeout);
            } 
            _moveTimeout = setTimeout(function() {
                _endTimeout(d, callback);
            }, 100);    
        } else {
            _endTimeout(d, callback);
        }
    };
    
    var _endTimeout = function(d, callback) {
        ////console.log("transition end! _updateVisibleElems = " + _updateVisibleElems + ", self.loadItemContent = " + self.loadItemContent);
        if (typeof callback === 'function') {
            callback();
        } 
        _showNotLoadedItems();
        _moving = false;
        _moveTimeout = null;
    };
    
    var _showNotLoadedItems = function() {
        var index = 0,
            itemsCollection = _container.children('.' + self.noLoadedClass),
            itemsCollectionLength = itemsCollection.length;
    
        while (index < itemsCollectionLength) {
            _addItemToNav(parseInt(itemsCollection.eq(index).attr('data-pos')));
            
            index++;
        }
        
        itemsCollection.removeClass(self.noLoadedClass);
        
        // Garbage collection
        index = itemsCollection = itemsCollectionLength = null;
    };
    
    var _addTemporaryItem = function(dir) {
        var firstVisibleItem = _visibleItems[0],
            lastVisibleItem = _visibleItems[_visibleItems.length - 1],
            itemsCollection = _container.children(),
            itemsCollectionLength = itemsCollection.length,
            lastItem = itemsCollection.last(),
            lastItemPos = parseInt(lastItem.attr('data-pos')),
            firstItem = itemsCollection.first(),
            firstItemPos = parseInt(firstItem.attr('data-pos')),
            itemCopy = _itemCopy.clone().addClass(self.noLoadedClass),
            nextPos = null;
 
        /*//console.log(self);
        //console.log(_visibleItems);
        //console.log(_container);
        //console.log("itemsCollectionLength = " + itemsCollectionLength + ", maxVisibleItems: " + self.maxVisibleItems);
        //console.log("firstItemPos = " + firstItemPos + ", firstVisibleItem = " + firstVisibleItem);*/
        
        if ((dir === 'right' || dir === 'down') && !self.isMatrix) {
            nextPos = lastItemPos + 1;//(firstVisibleItem - 1 + self.maxVisibleItems);
            if (firstItemPos + 2 < firstVisibleItem) {
                firstItem.remove();
            }
            if (nextPos < self.itemsArray.length) {
                itemCopy.css((self.horizontal ? 'left' : 'top'), (nextPos * _carouselPx) + 'em').attr('data-pos', nextPos);
                itemCopy.attr('id', self.itemIdPrefix + nextPos);
                _container.append(itemCopy);
                _addItemToNav(nextPos, false);
            }
        } else if ((dir === 'left' || dir === 'up') && !self.isMatrix) {
            nextPos = firstItemPos - 1;
            if (lastItemPos > lastVisibleItem + 2) {
                lastItem.remove();
            }
            if (nextPos >= 0) {
                itemCopy.css((self.horizontal ? 'left' : 'top'), (nextPos * _carouselPx) + 'em').attr('data-pos', nextPos);
                itemCopy.attr('id', self.itemIdPrefix + nextPos);
                _container.prepend(itemCopy);
                _addItemToNav(nextPos, false);
            }
        } 
        
        // Garbage collection
        firstVisibleItem = lastVisibleItem = itemsCollection = itemsCollectionLength = lastItem = lastItemPos = firstItem = firstItemPos = itemCopy = nextPos = null;
    };
    
    var _addNavItems = function(itemsArray, setPosition) {
        /*self.nav = new fxm.nav({});
        self.nav.add(self.containerId, function() { _onMove('left'); }, function() { _onMove('right'); }, function() { _onMove('up'); }, function() { _onMove('down'); });*/
        
        var index = 0;

        while (index < itemsArray.length) {
            _addItemToNav(_totalItems, setPosition);
            
            index++;
            _totalItems++;
        }
        
        // Garbage collection
        index = null;
    };
    
    var _addItemToNav = function(pos, setPosition) {
        if (pos > self.itemsArray.length - 1) {
            return;
        }
        if (setPosition) {
            if (self.isMatrix) {
                var colPos = pos % self.itemsPerPage,
                    rowPos = parseInt(pos / self.itemsPerPage),
                    elem = document.getElementById(self.itemIdPrefix + pos);
            
                ////console.log("_addItemToNav => pos: " + pos);
                elem.style.left = (colPos * _carouselPx.width) + 'px';
                elem.style.top = (rowPos * _carouselPx.height) + 'px';
                elem.setAttribute('data-row', rowPos);
                elem.setAttribute('data-col', colPos);
                
                // Garbage collection
                colPos = rowPos = elem = null;
            } else {
                document.getElementById(self.itemIdPrefix + pos).style[(self.horizontal ? 'left' : 'top')] = (pos * _carouselPx) + 'em';
            }
        }
        if (typeof self.onAddingItem === 'function') {
            self.onAddingItem(pos, self.itemsArray[pos]);
        }
    };
    
    var __construct = function(confObj) {
        var keys = Object.keys(confObj),
            index = 0,
            length = keys.length;
    
        confObj.itemHtmlTemplate = confObj.itemHtmlTemplate || self.itemHtmlTemplate;
        while (index < length) {
            var attr = keys[index];
            
            self[attr] = confObj[attr];
            
            index++;
        }
        if (confObj.itemsArray) {
            confObj.itemsArray = confObj.itemsArray.length >= self.maxVisibleItems ? confObj.itemsArray.slice(0, self.maxVisibleItems) : confObj.itemsArray;
            _init(confObj);
        }
        
        // Garbage collection
        keys = index = length = null;
    };
    
    this.getItemsArray = function() {
        return self.itemsArray;
    };
    
    this.getFocused = function() {
        return this.itemIdPrefix + this.currDivNumber;
    };
    
    this.getCurrItem = function() {
        return self.itemsArray[self.currDivNumber];
    };
    
    this.getItemAt = function(pos) {
        return self.itemsArray[pos];
    };
    
    this.getTotal = function() {
        return this.itemsArray.length;
    };
    
    this.getVisibleItems = function() {
        return _visibleItems;
    };

    this.unload = function() {
        var index = 0,
            itemsCollection = _container.children(),
            itemsCollectionLength = itemsCollection.length,
            removeFunction = typeof self.onRemovingItem === 'function';
    
        while (index < itemsCollectionLength) {
            var pos = parseInt(itemsCollection.eq(index).attr('data-pos'));
            
            if (removeFunction) {
                self.onRemovingItem(pos, self.itemsArray[pos]);
            }
            
            index++;
        }
        
        itemsCollection.addClass(self.noLoadedClass);
        
        // Garbage collection
        index = itemsCollection = itemsCollectionLength = removeFunction = null;
    };
    
    this.load = function() {
        _showNotLoadedItems();
    };
    
    this.focus = function(focusDivNumber) {
        var currDivNumber = (typeof focusDivNumber === 'number') ? (_visibleItems[focusDivNumber] ? _visibleItems[focusDivNumber] : focusDivNumber) : self.currDivNumber;
        
        self.active = true;
        self.currDivNumber = currDivNumber;
        if (typeof self.onFocus === 'function') {
            self.onFocus.call(self, self.itemsArray[self.currDivNumber]);
        }
        _moving = false;
        
        // Garbage collection
        currDivNumber = null;
    };
    
    this.defocus = function() {
        self.active = false;
        if (typeof self.onBlur === 'function') {
            self.onBlur.call(self, self.currDivNumber);
        }
        _container.children().removeClass('focus');
    };
    
    this.getRight = function() {
        return self.itemIdPrefix + (self.currDivNumber + 1);
    };
    
    this.getLeft = function() {
        return self.itemIdPrefix + (self.currDivNumber - 1);
    };
    
    this.getUp = function() {
        return self.itemIdPrefix + (self.currDivNumber - 1);
    };
    
    this.getDown = function() {
        return self.itemIdPrefix + (self.currDivNumber + 1);
    };
    
    this.moveToPos = function(pos) {
        ////console.log(_visibleItems.indexOf(pos));
        if (pos >= 0 && pos < this.itemsArray.length) {
            ////console.log(_visibleItems, _visibleItems.indexOf(pos));
            if (_visibleItems.indexOf(pos) === -1) {
              //  //console.log("indexOf(pos) === -1");
                var speedBackup = this.speed;
                
                _focusOnLimit = true;
                if (pos < _visibleItems[0]) { // left/up
          //          //console.log("left/up");
                    _setVisibleItems(pos);
                    _moveCarousel((self.horizontal ? 'left' : 'up'), pos);
                } else if (pos > _visibleItems[_visibleItems.length - 1]) { // right/down
            //        //console.log("right/down");
                    _setVisibleItems(pos);
                    _moveCarousel((self.horizontal ? 'right' : 'down'), pos);
                }
                
                _focusOnLimit = false;
                this.speed = speedBackup;
            } else {
                //console.log("this.currDivNumber = " + pos);
                this.currDivNumber = pos;
                if (typeof self.onFocus === 'function') {
                    self.onFocus.call(self, self.itemsArray[self.currDivNumber]);
                }
            }
        }
    };
    
    this.moveUp = function() {
        _onMove('up');
        return !_moving;
    };
    
    this.moveDown = function() {
        _onMove('down');
        return !_moving;
    };
    
    this.moveLeft = function() {
        _onMove('left');
        return !_moving;
    };
    
    this.moveRight = function() {
        _onMove('right');
        return !_moving;
    };
    
    this.owns = function(id) {
        var index = -1,
            length = this.itemsArray.length,
            owns = false;
    
        while (++index < length) {
            if (id === this.itemIdPrefix + index) {
                owns = true;
                break;
            }
        }
        
        return owns;
    };
    
    this.addItems = function(items) {
        this.itemsArray = this.itemsArray.concat(items);
    };

    __construct(confObj);

}
