<?php

class AppController {
    
    private $conf_file_path = 'conf.json';
    private $temp_file_path = 'app_temp.php';
    
    private $framework_files = array(
        'folder_path' => 'framework',
        'files' => array(
            'classes' => array(
                'ViewManager',
                'View',
                'Carousel'
            ),
            'modules' => array(
                'Focus',
                'App',
                'SmartLibrary',
                'TransitionManager'
            )
        ) 
    );
    
    private $conf;
    private $temp;
    
    public function __construct() {
        $contents = file_get_contents($this->conf_file_path);
        $this->conf = json_decode($contents, true);
        
        $this->temp = file_get_contents($this->temp_file_path);
    }
    
    public function render() {
        $files = array(
            'css' => array(),
            'javascript' => array(),
            'views' => array()
        );
        $this->setTitle();
        
        $framework = $this->getFrameworkFiles();
        $viewFiles = $this->getViewFiles();
        $customFiles = $this->getCustomFiles();
        $files['css'] = array_merge($framework['css'], $viewFiles['css'], $customFiles['css']);
        $files['javascript'] = array_merge($customFiles['javascript'], $framework['javascript'], $viewFiles['javascript']);
        $files['views'] = array_merge($framework['views'], $viewFiles['views'], $customFiles['views']);
        
        $this->replaceContentsInTemplate($files);
        echo $this->temp;
    }
    
    private function setTitle() {
        $this->temp = str_replace('{{TITLE}}', $this->conf['title'], $this->temp);
    }

    private function replaceContentsInTemplate($files) {
        $init_file_path = $this->framework_files['folder_path'] . '/modules/Init.js'; 
        $merge_files = isset($this->conf['merge']) && $this->conf['merge'];
        $keys = array_keys($files);
        
        foreach ($keys as $fileType) {
            $html = $merge_files ? (($fileType == 'css') ? '<style type="text/css">' : ($fileType == 'javascript' ? '<script type="text/javascript">' : '')) : '';
            $html .= $merge_files ? (($fileType == 'javascript') ? 'var CONF = ' . json_encode($this->conf) . ';' : '<script type="text/javascript">var CONF = ' . json_encode($this->conf) . ';</script>') : '<script type="text/javascript">var CONF = ' . json_encode($this->conf) . ';</script>'; // set CONF
            $replace = '';
            foreach ($files[$fileType] as $file) {
                $replace = '{{' . strtoupper($fileType) . '}}';
                switch ($fileType) {
                    case 'css' :
                        $html .= (($merge_files) ? file_get_contents($file) : '<link rel="stylesheet" type="text/css" href="' . $file . '" />');
                        break;
                    case 'javascript' :
                        $html .= (($merge_files) ? file_get_contents($file) : '<script type="text/javascript" src="' . $file . '"></script>');
                        break;
                    case 'views' :
                        $html .= file_get_contents($file);
                        break;
                }
            }
            $html .= $merge_files ? (($fileType == 'javascript') ? file_get_contents($init_file_path) : '') : ($fileType == 'javascript' ? '<script type="text/javascript" src="' . $init_file_path . '"></script>' : '');
            $html .= $merge_files ? (($fileType == 'css') ? '</style>' : '</script>') : '';

            $this->temp = str_replace($replace, $html, $this->temp);
        }
    }
    
    private function getFrameworkFiles() {
        $final = array(
            'css' => array(),
            'javascript' => array(),
            'views' => array()
        );
        //$html = '';
        foreach ($this->framework_files['files'] as $folder => $files) {
            $base_url = $this->framework_files['folder_path'] . '/';
            foreach ($files as $file) {
                $file_path = $base_url . $folder . '/' . $file . '.js';
                array_push($final['javascript'], $file_path);
                //$html .= '<script type="text/javascript" src="' . $file_path . '"></script>';
            }
        }
        return $final;
    }
    
    private function getViewFiles() {
        $files = array(
            'css' => array(),
            'javascript' => array(),
            'views' => array()
        );
        /*$css_html = '';
        $js_html = '';
        $views_html = '';*/
        foreach ($this->conf['views'] as $view) {
            $css_file_path = 'css/' . $view . '.css';
            $js_file_path = 'javascript/' . $view . '.js';
            $view_file_path = 'views/' . $view . '.php';
            
            array_push($files['css'], $css_file_path);
            array_push($files['javascript'], $js_file_path);
            array_push($files['views'], $view_file_path);
            //$css_html .= '<link rel="stylesheet" type="text/css" href="' . $css_file_path . '" />';
            //$js_html .= '<script type="text/javascript" src="' . $js_file_path . '"></script>';
            //$views_html .= file_get_contents($view_file_path);
        }
        /*$this->temp = str_replace('{{CSS}}', $css_html, $this->temp);
        $this->temp = str_replace('{{JAVASCRIPT}}', $js_html, $this->temp);
        $this->temp = str_replace('{{VIEWS}}', $views_html, $this->temp);*/
        return $files;
    }
    
    private function getCustomFiles() {
        $conf_contents = $this->conf;
        $files = array(
            'css' => isset($conf_contents['css']) ? $conf_contents['css'] : array(),
            'javascript' => isset($conf_contents['javascript']) ? $conf_contents['javascript'] : array(),
            'views' => array()
        );
        /*$conf_contents = $this->conf;
        if (isset($conf_contents['css'])) {
            $css_html = '';
            foreach ($conf_contents['css'] as $css) {
                array_push($files['css'], $css);
                $css_html .= '<link rel="stylesheet" type="text/css" href="' . $css . '" />';
            }
            $this->temp = str_replace('{{EXTRACSS}}', $css_html, $this->temp);
        } else {
            $this->temp = str_replace('{{EXTRACSS}}', '', $this->temp);
        }

        if (isset($conf_contents['javascript'])) {
            $js_html = '';
            foreach ($conf_contents['javascript'] as $js) {
                $js_html .= '<script type="text/javascript" src="' . $js . '"></script>';
            }
            $this->temp = str_replace('{{EXTRAJS}}', $js_html, $this->temp);
        } else {
            $this->temp = str_replace('{{EXTRAJS}}', '', $this->temp);
        }*/
        return $files;
    }
    
}

$controller = new AppController();
$controller->render();
