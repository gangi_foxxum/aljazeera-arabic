<?php

class Helper {

    public static $logs = true;
   
    /**
     * This function returns an array containing the input data.
     *
     * @return array contains the input data
     *
     */
    public static function getInputData() {
        $request_method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
        switch ($request_method) {
            case 'GET' :
                return filter_input_array(INPUT_GET);
            case 'POST' :
                return filter_input_array(INPUT_POST);
        }
    }
   
    /**
     * This function prints an array
     *
     * @param array $array array to print
     * @param boolean $format nice output
     *
     */
    public static function printArray($array, $format = true) {
        if (self::$logs) {
            if ($format) {
                echo "<pre>";
                print_r($array);
                echo "</pre>";
            } else {
                var_dump($array);
            }
        }
    }
   
    /** 
     * This function outputs a log
     *
     * @param string $string string to output
     * @param boolean $print whether to output the result or not
     *
     */
    public static function log($string, $print = true) {
        if (self::$logs && $print) {
            echo $string . '<br />';
        }
    }
   
    /** 
     * This function generates a unique identifier
     *
     * @return integer the unique identifier
     */
    public static function generateUniqueID() {
        return md5(uniqid(rand(), true));//uniqid();
    }
   
       
    /**
     * This function sends an output to the client
     *
     * @param array $result the final output for the client
     *
     */
    public static function returnResult($header, $result, $download = false) {
        if ($download) {
            header("Content-Disposition: attachment; filename=\"$download\"");
        }
        header("Content-Type: " . $header);
        echo $result;
    }
       
    public static function makeRequest($method, $url, $data = false, $return_response = true, $header = false) {
        $curl = curl_init();
        if (isset($data['userAgent'])) {
            curl_setopt($curl, CURLOPT_USERAGENT, $data['userAgent']);
        }

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                if ($header) {
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if ($return_response) {
            return curl_exec($curl);
        } else {
            curl_exec($curl);
        }
    }
   
    // Function to get the client IP address
    public static function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }
        return $ipaddress;
    }
   
    public static function get_user_agent() {
        $server_vars = filter_input_array(INPUT_SERVER);
        return isset($server_vars['HTTP_USER_AGENT']) ? $server_vars['HTTP_USER_AGENT'] : false;
    }
   
    public static function loopArray($array, $callable_function, &$params = array(), $depth = 0) {
        foreach($array as $key => $value){
            //If $value is an array.
            if(is_array($value)) {
                if (is_callable($callable_function)) {
                    $callable_function($key, $value, $params, $depth);
                }
                //We need to loop through it.
                self::loopArray($value, $callable_function, $params, $depth + 1);
            } else if (is_callable($callable_function)) {
                $callable_function($key, $value, $params, $depth);
            }
        }
    }

}
