<?php
$countryCodeFromIp = strtolower(geoip_country_code_by_name(getenv("REMOTE_ADDR")));
$arabicCountries = ["dz","bh", "td", "km", "dj", "eg", "er", "iq", "il", "jo", "kw", "lb", "ly","mt", "mr", "ma", "om", "ps", "qa", "sa", "so", "sd", "sy", "tz", "tn", "ae", "ye"];
$lookForCountry = array_search($countryCodeFromIp, $arabicCountries);

if (gettype($lookForCountry) !== 'boolean'){
    echo json_encode("arabic");
}else{
    echo json_encode("english");
}

?>
