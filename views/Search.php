<div id="searchView" class="view">
    
    <div id="searchWrapper">
    
        <div id="searchInputWrapper">
            <div id="searchMessage">بحث</div>
            <div id="searchInput">                
<!--                <div id="searchCursor"></div>-->
                <div id="searchInner"></div>
            </div>
        </div>

        <div id="searchInputKeys">
            <div class="searchInputKeysRow">
                <div id="key_ض" class="searchInputKey" data-key="ض">ض</div>
                <div id="key_ص" class="searchInputKey" data-key="ص">ص</div>
                <div id="key_صث" class="searchInputKey" data-key="صث">صث</div>
                <div id="key_ق" class="searchInputKey" data-key="ق">ق</div>
                <div id="key_ف" class="searchInputKey" data-key="ف">ف</div>
                <div id="key_غ" class="searchInputKey" data-key="غ">غ</div>
                <div id="key_ع" class="searchInputKey" data-key="ع">ع</div>
                <div id="key_ه" class="searchInputKey" data-key="ه">ه</div>
                <div id="key_خ" class="searchInputKey" data-key="خ">خ</div>
                <div id="key_ح" class="searchInputKey" data-key="ح">ح</div>
                <div id="key_ج" class="searchInputKey" data-key="ج">ج</div>
                <div id="key_د" class="searchInputKey" data-key="د">د</div>
            </div>
            <div class="searchInputKeysRow">
                <div id="key_ش" class="searchInputKey" data-key="ش">ش</div>
                <div id="key_س" class="searchInputKey" data-key="س">س</div>
                <div id="key_ي" class="searchInputKey" data-key="ي">ي</div>
                <div id="key_ب" class="searchInputKey" data-key="ب">ب</div>
                <div id="key_ل" class="searchInputKey" data-key="ل">ل</div>
                <div id="key_ا" class="searchInputKey" data-key="ا">ا</div>
                <div id="key_ت" class="searchInputKey" data-key="ت">ت</div>
                <div id="key_ن" class="searchInputKey" data-key="ن">ن</div>
                <div id="key_م" class="searchInputKey" data-key="م">م</div>
                <div id="key_ك" class="searchInputKey" data-key="ك">ك</div>
                <div id="key_ط" class="searchInputKey" data-key="ط">ط</div>
                <div id="key_BACK" class="searchInputKey" data-key="BACK">&#xf104;</div>
            </div>
            <div class="searchInputKeysRow">
                <div id="key_ئ" class="searchInputKey" data-key="ئ">ئ</div>
                <div id="key_ء" class="searchInputKey" data-key="ء">ء</div>
                <div id="key_ؤ" class="searchInputKey" data-key="ؤ">ؤ</div>
                <div id="key_ر" class="searchInputKey" data-key="ر">ر</div>
                <div id="key_لا" class="searchInputKey" data-key="لا">لا</div>
                <div id="key_ى" class="searchInputKey" data-key="ى">ى</div>
                <div id="key_ة" class="searchInputKey" data-key="ة">ة</div>
                <div id="key_و" class="searchInputKey" data-key="و">و</div>
                <div id="key_ز" class="searchInputKey" data-key="ز">ز</div>               
                <div id="key_ظ" class="searchInputKey" data-key="ظ">ظ</div>  
                <div id="key_SPACE" class="searchInputKey" data-key="SPACE">_</div>
                <div id="key_ENTER" class="searchInputKey" data-key="ENTER">&#xf002;</div>
                
            </div>
        </div>
        
    </div>
    
</div>