<div id="mainView" class="appView">
    <div id="logoWrapper"></div>
    <div id="buttonsInfo">
        <div id="redInfo" class="button">بحث</div>
        <div id="redButton" class="button"></div>
        <div id="blueInfo" class="button">قائمة</div>
        <div id="blueButton" class="button"></div>
        <div id="changeLanguageIcon" class="icon"></div>
    </div>
    <div id="episodesInfo">
        <div id="title"></div>
        <div id="name"></div>
        <div id="summary"></div>
    </div>
    <div id="sidebarLauncher"><img src="images/Triangle.png"></div>
    <div id="liveWrapper">
        <div id="liveMessage">شاهد بالبث المباشر</div>
        <div id="liveVideoWrapper">
            <div id="playerView" class="appView">
            <video id="videoPlayer"></video>
            <div id="controls">
                <div id="progressbar"></div>
                <div id="timeInfo">
                    <div id="currentTime">00:00</div>
                    <div id="duration">| 00:00</div>
                </div>
                <div id="playerControls">
                    <div class="clickControl" id="backward">&#xf04a;</div>
                    <div class="clickControl" id="playPause">&#xf04c;</div>
                    <div class="clickControl" id="forward">&#xf04e;</div>
                    <div class="clickControl" id="videoInfo">&#xf129;</div>
                    <div class="clickControl" id="showCarousel"></div>
                    <div class="clickControl" id="close">&#xf00d;</div>
                </div>

            </div>
           <div id="mainVideoObject"></div>
           </div>
        </div>
    </div>
    <div id="liveInfo">
            <div id="nowPlaying">
            <div id="messageNow" class="message">اللعب الآن</div>
            <div id="nameOfNow" class="nameOf"></div>
            <div id="startedAtNow" class="started"></div>
        </div>
        <div id="next">
            <div id="messageNext" class="message">التالى</div>
            <div id="nameOfNext" class="nameOf"></div>
            <div id="startedAtNext" class="started"></div>
        </div>
    </div>
    <div id="carouselWrapper"></div>
    <div id="collectionWrapper"></div>
    <div id="moreTab">
        <div id="moreTitle">آخر</div>
        <div id="privacyElem" class="moreElement" onclick="App.Main.openPrivacy('policy')">
            <div id="privacyIcon" class="moreIcon"></div>
            <div class="moreName">سياسة الخصوصية</div>
        </div>
        <div id="cookieElem" class="moreElement" onclick="App.Main.openPrivacy('cookie')">
            <div id="cookieIcon" class="moreIcon"></div>
            <div class="moreName">سياسة ملفات تعريف الارتباط</div>
        </div>
    </div>
    <div id="privacyView" class="agreement">
        <div class="privacyViewTitle"></div>
        <div class="privacyViewText textAgreement"></div>
        <div class="privacyViewUrl urlAgreement"></div>
        <div id="privacyViewButton" onclick="App.Main.backToMoreTab()"><p>موافق</p></div>
    </div>
</div>
