<div id="languageChooserView" class="appView">
    <div id="languageChooserWrapper">
        <div id="icon"></div>
        <div id="languagesWrapper">
            <div id="english">ENGLISH</div>
            <div id="arabic">العربية</div>
        </div>
    </div>
</div>