App.addView((function() {
    // Private variables and functions
     
    
// Public variables and functions
    var view = {};

    view.id = 'Splash';
    view.containerID = 'splashView';

    view.onHideOptions = {};
    view.onShowOptions = {};

    view.onLoad = function() {};
    
    view.onUnload = function() {
    };

    view.init = function(view) {
    };
    
    view.deinit = function() {};

    view.onHideStart = function() {};
    view.onHideComplete = function() {};

    view.onShowStart = function() {};
    view.onShowComplete = function() {    };

    view.onHistoryBack = function() {};
    
    return view;
})());