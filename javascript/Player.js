App.addView((function() {
	// Private variables and functions
	var _navId = null,
            _url = null,
            _diffTime = 0,
            _currentTime = 0,
            _hideTimer = "",
			_liveVideo = true,
            _duration = 0;

		  var _checkBackward = function(){
    		if (SmartLibrary.$_GET('controls') !== "false"){
    			Focus.moveTo("backward");
    	   	}else{
    			switch (Focus.getCurrNav().id) {
    				case "playPause":
    					Focus.moveTo("close");
    					break;
    				case "close":
    					Focus.moveTo("playPause");
    					break;
    			}
    		}
    	}

    	var _checkForward = function(){
    		if (SmartLibrary.$_GET('controls') !== "false"){
    			Focus.moveTo("forward");
    	   	}else{
    			switch (Focus.getCurrNav().id) {
    				case "playPause":
    					Focus.moveTo("videoInfo");
    					break;
    				case "videoInfo":
    					Focus.moveTo("playPause");
    					break;
    			}
    		}
    	}

			var _backToVideos = function(){
				// console.log(App.Player.liveVideo)
				clearTimeout(_hideTimer);
					videoPlayer.pause();
					$("#controls").hide();

					if(App.Main.getCurrentLoc() === "main"){
						if (SmartLibrary.$_GET('hls') !== "false"){
							Focus.moveTo("liveVideoWrapper");
						}
						_resetCssValues();
					}
					if(App.Main.getCurrentLoc() === "episodes" || App.Main.getCurrentLoc() === "search"){
						console.log("episodes")
						clearTimeout(_hideTimer);
						$("#sidebarWrapper").hide();
				        $("#mainView").css("opacity", "1");
				        $("#liveWrapper").hide();
				        $("#liveInfo").hide();
				        $("#carouselWrapper").show();
						$("#carouselWrapper").css("top","-150px")
				        $("#collectionWrapper").hide();
						$("#currentTime").html("00:00");
						$("#duration").html("| 00:00");
						$(".contentTitle").show();
						$("#carouselWrapper").css("z-index", "");
						$("#carouselWrapper").css("position", "");
						$("#liveVideoWrapper").css('width', "");
						$("#liveVideoWrapper").css('height',"");
						$("#liveVideoWrapper").css('top',"");
						$("#liveVideoWrapper").css('left',"");
						$("#liveVideoWrapper").css('z-index',"");
						$("#name").css("width", "");
						$("#summary").css("width", "");
						console.log("last focused "+App.Main.getLastEpisodeFocused())
						Focus.moveTo(App.Main.getLastEpisodeFocused());
						if(App.Main.getCurrentLoc() === "search"){
							$("#episodesInfo").hide()
							$("#episodesInfo").css("background-color", "");
							$("#episodesInfo").css("margin-left", "");
							$("#episodesInfo").css("opacity", "");
							$("#episodesInfo").css("top", "");
							$("#episodesInfo").css("width", "");
							$("#episodesInfo").css("z-index", "");
							$("#episodesInfo").css("height", "");
							$("#episodesInfo").css("padding-left", "");
						}else{
							$("#episodesInfo").css("background-color", "");
							$("#episodesInfo").css("margin-left", "");
							$("#episodesInfo").css("opacity", "");
							$("#episodesInfo").css("top", "");
							$("#episodesInfo").css("width", "");
							$("#episodesInfo").css("z-index", "");
							$("#episodesInfo").css("height", "");
							$("#episodesInfo").css("padding-left", "");
						}
					}
			};

			var _resetCssValues = function(){
	        //    $("#splashIcon").show();
				clearTimeout(_hideTimer);
				App.Main.setSidebarFlag(true)
	            $("#searchView").hide();
				$("#mainVideoObject").css('width', "");
				$("#mainVideoObject").css('height',"");
				$("#liveVideoWrapper.focus").css("border-width", "");
				$("#playerView").css("left", "");
				$("#playerView").css("position", "");
				$("#playerView").css("top", "");
				$("#playerView").css("z-index", "");
				$("#progressbar").css("left", "");
				$("#progressbar").css("width", "");
				$("#controls").css("width",  "");
				$("#controls").css("left",  "");
				$("#videoPlayer").css("position", "");
				$("#videoPlayer").css("left", "");
				$("#videoPlayer").css("height", "");
				$("#videoPlayer").css('width',"");
				$("#liveVideoWrapper").css('width', "");
				$("#liveVideoWrapper").css('height',"");
				$("#liveVideoWrapper").css('top',"");
				$("#liveVideoWrapper").css('left',"");
				$("#liveVideoWrapper").css('z-index',"");
				$("#mainVideoObject").css('width',"");
				$("#mainVideoObject").css('height',"");
	            $("#progressbar").hide();
	            $("#controls").hide();
	            $("#currentTime").html("00:00");
	            $("#duration").html("| 00:00");
	            $(".contentTitle").show();
	            $("#carouselWrapper").css("z-index", "");
	            $("#carouselWrapper").css("top", "");
	            $("#carouselWrapper").css("position", "");
	            $("#episodesInfo").css("background-color", "");
	            $("#episodesInfo").css("margin-left", "");
	            $("#episodesInfo").css("opacity", "");
	            $("#episodesInfo").css("top", "");
	            $("#episodesInfo").css("width", "");
	            $("#episodesInfo").css("z-index", "");
	            $("#episodesInfo").css("height", "");
	            $("#episodesInfo").css("padding-left", "");
	            $("#name").css("width", "");
	            $("#summary").css("width", "");
	            $("#episodesInfo").hide();
	            $("#carouselWrapper").show();
	            //$("#carouselWrapper").animate({ "top": "" }, "slow");
				$("#carouselWrapper").css("top", "");
				$("#collectionWrapper").show();
	            //$("#collectionWrapper").animate({ "top": "" }, "slow");
				$("#collectionWrapper").css("top", "");
				videoPlayer.src = App.Main.getLivestreamUrl();
				clearTimeout(_hideTimer);
				if (SmartLibrary.$_GET('hls') !== "false"){
					$("#liveWrapper").show();
					$("#liveInfo").show();
					videoPlayer.play()
				}
				else{
					App.Main.hlsStatus()
				}

	        };

			var _progressBarSet = function() {
				_duration = videoPlayer.duration;
				_currentTime=videoPlayer.currentTime;
				_diffTime = (_currentTime*100)/_duration;
					$( "#progressbar").progressbar({
						value: _diffTime
					});
		        };

        var _toCarousel = function(){
            _showControls();
            if ($("#carouselWrapper").css("z-index") === "3000") {
                App.Main.findCarousel("carousel_images_");
            }
        };

        var _secondsToHms = function(d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);
        return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : (m < 10 ? "0" : "")) + m + ":" + (s < 10 ? "0" : "") + s);
        };

        var _setTimers = function (){
           $("#currentTime").html(_secondsToHms(_currentTime));
        };

        var activateHide = function (){
            console.log("ACTIVATE HIDE");
            _hideTimer = setTimeout(function() {hideControls();}, 10000);
        };


        var _showControls = function (){
            console.log("SHOW CONTROLS");
			$('#controls').css("top","640px");
            $('#carouselWrapper').show();
            $('#episodesInfo').show();
            clearTimeout(_hideTimer);
            activateHide();
        };

		var hideControls = function (){
			//$('#controls').animate({"top": "717px"}, "slow");

			if ($("#liveVideoWrapper").css("width") === "1280px"){
				$('#controls').css("top","717px");
				$('#carouselWrapper').hide();
				$('#episodesInfo').hide();
				Focus.moveTo("playPause");
			}
			else{
				console.log("Good try")
			}

		}


		var _setListeners = function() {
			var video = document.getElementById('videoPlayer');
			        video.addEventListener("play", function(){
			            console.log("state - play - media has been started or is no longer paused");
			        });
			        video.addEventListener("pause", function(){
			            console.log("state - pause - the media is paused either by the user or programmatically");
			        });
			        video.addEventListener("playing", function(){
			            console.log("state - playing - the media is playing after having been paused or stopped for buffering");
			        });
			        video.addEventListener("progress", function(){
			            console.log("state - progress - the browser is in the process of getting the media data (downloading the media)");
			        });
			        video.addEventListener("stalled", function(){
			            console.log("state - stalled - the browser is trying to get media data, but data is not available");
			        });
			        video.addEventListener("waiting", function(){
			            console.log("state - waiting - the media has paused but is expected to resume (like when the media pauses to buffer more data)");
			        });
			        video.addEventListener("error", function(e){
			            console.log("state - error - an error occurred during the loading of a media file");
			            switch (e.target.error.code) {
			                case e.target.error.MEDIA_ERR_ABORTED:
			                    console.log('***MEDIA_ERR_ABORTED - You aborted the video playback.');
			                    break;
			                case e.target.error.MEDIA_ERR_NETWORK:
			                    console.log('***MEDIA_ERR_NETWORK - A network error caused the video download to fail part-way.');
			                    break;
			                case e.target.error.MEDIA_ERR_DECODE:
			                    console.log('***MEDIA_ERR_DECODE - The video playback was aborted due to a corruption problem or because the video used features your browser did not support.');
			                    break;
			                case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
			                    console.log('***MEDIA_ERR_SRC_NOT_SUPPORTED - The video could not be loaded, either because the server or network failed or because the format is not supported.');
			                    break;
			                default:
			                    console.log('***An unknown error occurred.');
			                    break;
			            }

			        });

	            $('#backward').click(function() {
	                _showControls();
					var seekToTime = videoPlayer.currentTime - 30;
					if (seekToTime < 0){
						seekToTime = 0;
					}
					videoPlayer.currentTime = seekToTime;
	            });
	            $('#playPause').click(function(){
	                _showControls();
	                if (videoPlayer.currentTime > 0){
	                    if (!videoPlayer.paused){
	                        $("#playPause").html("&#xf04b;");
	                        videoPlayer.pause();
	                    }
	                    else {
	                        videoPlayer.play();
	                        $("#playPause").html("&#xf04c;");
	                    }
	                }
	            });

           $("#showCarousel").click(function () {
               _showControls();
               if ($("#carouselWrapper").css("z-index") !== "3000") {
                    $(".contentTitle").hide();
					$("#carouselWrapper").css("position", "absolute");
					$("#carouselWrapper").css("z-index", "3000");
                    $("#carouselWrapper").css("top", "-10%");
                    _episodesInfoBack();
                    App.Main.findCarousel("carousel_images_")
                }
                else {
                    _carouselBack();
                }
           });

           $("#videoInfo").click(function () {
               _showControls();
                if ($("#episodesInfo").css("z-index") !== "3000") {
                    $("#episodesInfo").css("background-color", "#000000");
                    $("#episodesInfo").css("margin-left", "64px");
                    $("#episodesInfo").css("opacity", "0.8");
                    $("#episodesInfo").css("top", "478px");
                    $("#episodesInfo").css("height", "163px");
                    $("#name").css("width", "1137px");
                    $("#summary").css("width", "1041px");
                    $("#episodesInfo").css("width", "1152px");
                    $("#episodesInfo").css("z-index", "3000");
                    _carouselBack();
                }
                else {
                   _episodesInfoBack();
                }

           });

		   $('#forward').click(function() {
			   _showControls();
			   var seekToTime = videoPlayer.currentTime + 30;
			   if (seekToTime < videoPlayer.duration){
				   videoPlayer.currentTime = seekToTime;
			   }else{
				   videoPlayer.currentTime = videoPlayer.duration;
			   }
		   });

		   $('#close').click(function(){
			   ajkeen.AjVideoSession('stop');
			   if (!SmartLibrary.$_GET('guid')){
				  setTimeout(_backToVideos, 200); // Wait to sent the keen.io data
			  }else{
				  ajkeen.AjVideoSession('stop');
				  setTimeout(function(){window.history.back();}, 200); // Wait to sent the keen.io data
			  }

		   });
	};

        var _episodesInfoBack = function(){
            $("#episodesInfo").css("background-color", "");
            $("#episodesInfo").css("margin-left", "");
            $("#episodesInfo").css("opacity", "");
            $("#episodesInfo").css("top", "");
            $("#episodesInfo").css("width", "");
            $("#episodesInfo").css("z-index", "");
        }

        var _carouselBack = function() {
            $(".contentTitle").show();
            $("#carouselWrapper").css("z-index", "");
            $("#carouselWrapper").css("top", "");
            $("#carouselWrapper").css("position", "");
        }

	// Public variables and functions
	var view = {};


	view.id = 'Player';
	view.containerID = 'playerView';
	view.onHideOptions = {opacity: 0};
	view.onShowOptions = {opacity: 1};
    view.showControls = _showControls;
	view.liveVideo = _liveVideo;
	view.onLoad = function() {
	};
	view.onUnload = function() {
	};

	view.init = function(view) {
            _setListeners();
            $("#progressbar").hide();
            $("#controls").hide();
			_navId = Focus.register({
                backward: ["close", "playPause", _toCarousel, hideControls],
                playPause: [_checkBackward, _checkForward, _toCarousel, hideControls],
                forward: ["playPause", "videoInfo", _toCarousel, hideControls],
                videoInfo: [_checkForward, "showCarousel", _toCarousel, hideControls],
                showCarousel: ["videoInfo", "close", _toCarousel, hideControls],
                close: ["showCarousel",_checkBackward , _toCarousel, hideControls]
        });
	};

	view.deinit = function() {};

	view.onHideStart = function() {
		console.log("Player onHideStart");
	};
	view.onHideComplete = function() {
		console.log("Player onHideComplete");
	};

	view.onShowStart = function() {
		console.log("Player onShowStart");
	};

	view.onShowComplete = function() {
		console.log("Player onShowComplete");
	};

	view.setUrl = function(url) {
		videoPlayer.src = url;
		videoPlayer.play()
		$("#splashIcon").slideUp();
	};

    view.getVideoInfo = function(name, summary, length) {
        console.log("getVideoInfo");
        $("#name").html(name);
        $("#summary").html(summary);
        var toSeconds = length/1000;
        $("#duration").html("| " + _secondsToHms(toSeconds));
        setInterval(_progressBarSet, 400);
        setInterval(_setTimers,1000);
        activateHide();
        $("#videoPlayer").css("width", "1280px");
        $("#videoPlayer").css("height","720px");
    };


	view.onHistoryBack = function(){
        if (!SmartLibrary.$_GET('guid')){
            if (($("#liveVideoWrapper").css("width") === "1280px") || ($("#searchWrapper").is(":visible")) || ($("#episodesInfo").is(":visible")))  {
                if ($("#controls").is(":visible")){
                    ajkeen.AjVideoSession('stop');
                }
                setTimeout(_backToVideos, 600); // Wait to sent the keen.io data
            }
            if (App.Main.getCurrentLoc() === "main" && $("#liveVideoWrapper").css("width") !== "1280px"){
                window.history.back();
            }
            if ((App.Main.getCurrentLoc() === "episodes" || App.Main.getCurrentLoc() === "search" || App.Main.getCurrentLoc() === "collection" ) && $("#liveVideoWrapper").css("width") !== "1280px"){
                document.location.reload(true);
            }
        }else{
            window.history.back();
        }

        };

        view.onKeyDown = function(keyCode, keyMap) {
            switch (keyCode){
                        case keyMap.VK_RED:
                            if ($("#liveVideoWrapper").css("width")!== "1280px"){
                                if ($("#sidebarWrapper").is(":visible")) {
                                    $("#mainView").css("opacity", "1");
                                    $("#sidebarWrapper").hide();
                                }
                                console.log("RED");
                                $("#searchView").show();
                                $("#liveWrapper").hide();
                                $("#liveInfo").hide();
                                $("#carouselWrapper").hide();
                                $("#collectionWrapper").hide();
                                $("#episodesInfo").hide();
                                Focus.moveTo('key_ض');
								App.Main.setCurrentLoc("search");

                            }
                            break;
                            case keyMap.VK_ENTER:
							if (Focus.getCurrNav().id.substr(0,3) ==="key"){
								App.Search.inputKey()
							}
                            if (App.Main.sidebarFlag() === false){
                                if (($("#liveVideoWrapper").css("width") === "1280px") && ($("#controls").is(":visible")=== false)){
                                    console.log("close enter");
                                    App.Player.onHistoryBack();
                                    Focus.moveTo("liveVideoWrapper");
									App.Main.setSidebarFlag(true)
                                    videoPlayer.play();
                                }
                                if (($("#liveVideoWrapper").css("width") !== "1280px") && ($("#liveVideoWrapper").is(":visible") === true) && ($("#controls").is(":visible")=== false)) {
                                    console.log("OPEN");
                                    $("#video").remove();
									App.Main.setSidebarFlag(false)
                                    App.Main.playLiveStream();
                                    document.getElementById("video").style.width = "1280px";
                                    document.getElementById("video").style.height = "720px";
                                    $("#liveVideoWrapper").css('width', "1280px");
                                    $("#liveVideoWrapper").css('height',"720px");
                                    $("#playerView").css('width', "1280px");
                                    $("#playerView").css('height',"720px");
                                    $("#mainVideoObject").css('width', "1280px");
                                    $("#mainVideoObject").css('height',"720px");
                                    $("#liveVideoWrapper.focus").css("border-width", "0px");
                                    $("#liveVideoWrapper").css('top',"-147px");
                                    $("#liveVideoWrapper").css('left',"-99px");
                                    $("#liveVideoWrapper").css('z-index',"1500");
                                }
                            }
                        break;
                        case keyMap.VK_BLUE:
                            if ($("#liveVideoWrapper").css("width")!== "1280px"){
                                if ($("#sidebarWrapper").is(":visible")) {
                                    $("#mainView").css("opacity", "1");
                                    $("#sidebarWrapper").hide();
                                    if ($("#liveVideoWrapper").is(":visible")) {
                                        Focus.moveTo("liveVideoWrapper");
                                        $("#carouselWrapper").show();
                                        $("#carouselWrapper").animate({"top": ""}, "slow");
                                        $("#collectionWrapper").animate({"top": ""}, "slow");
                                        $("#liveWrapper").show();
                                        $("#liveInfo").show();
                                    }

                                    if (($("#carouselWrapper").is(":visible")) && ($("#liveVideoWrapper").is(":visible")) === false) {
                                        App.Main.findCarousel("carousel_images_")
                                    }
                                    if (($("#carouselWrapper").is(":visible") === false) && ($("#liveVideoWrapper").is(":visible")) === false && ($("#collectionWrapper").is(":visible"))) {
                                        App.Main.findCarousel("collection_images_");
                                    }
                                    if ($("#searchWrapper").is(":visible")) {
                                        Focus.moveTo("key_ض");
                                    }
                                }
                                    else {
                                        if ($("#controls").is(":visible")=== false) {
                                            $("#mainView").css("opacity", "0.4");
                                            $("#sidebarWrapper").show();
                                            Focus.moveTo("watchLive");
                                        }
                                    }
                            }
                    break;
            }
        };
    return view;
})());
