var _language = {
    started: "بدأت",
    in: "في",
    minsAgo: "دقائق مضت",
    mins: "دقيقة",
    featured: "متميز",
    collections: "مختارات",
    showing: "عرض",
    results: "النتائج لل",
    latestNews: "مقاطع جديدة",
    latestProgrammes: "احدث حلقات",                      
    discussions: "برامج حوارية",
    newsMagazine: "مجلة تلفزيونية",
    documentaries: "برامج وثائقية",
    latestEpisodes: "أحدث حلقة",
    unavaliable: "المعلومات غير متوفرة"
};

