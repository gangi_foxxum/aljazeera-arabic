(function() {
    HTML5player = {
        videoSrc: null, sourceObject: null, mime: 'video/mp4', poster: 'images/preview.png', fullScreen: false, windowWidth: 590, windowHeight: 340, fullScreenWidth: 1280, fullScreenHeight: 720, fullScreenClass: 'fullscreen', videoObjectWrapper: 'mainVideoObject', progressBarId: 'progress-bar', videoObjectId: 'video', videoObjectNavigation: null, zindex: 0, startPosition: 0, seekTime: 1, STOPPED: 0, PLAYING: 1, PAUSED: 2, BUFFERING: 4, ENDED: 5, ERROR: 6, PROGRESS: 7, LOADED: 8, LOADEDMETADATA: 9, SEEKING: 10, SEEKED: 11, STARTPLAYING: 12, currentState: null, loadCatchCounter: 0, raiseOnStop: true,
        init: function() {
            HTML5player.videoDiv = document.getElementById(HTML5player.videoObjectWrapper);
            if (!HTML5player.videoDiv) {
                HTML5player.videoDiv = document.createElement("div");
                HTML5player.videoDiv.setAttribute("id", HTML5player.videoObjectWrapper);
                HTML5player.videoDiv.setAttribute("z-index", HTML5player.zindex);
                document.getElementsByTagName('body')[0].appendChild(HTML5player.videoDiv);
            }
            if (HTML5player.videoObject) {
                raiseOnStop = false;
                //HTML5player.videoObject.playPosition = 0;
                //HTML5player.videoObject.playTime = 0;
                if (HTML5player.videoObject.parentNode) {
                    HTML5player.videoObject.parentNode.removeChild(HTML5player.videoObject);
                }
            }
            HTML5player.videoDiv.innerHTML = "";
            HTML5player.videoObject = document.createElement("video");
            HTML5player.videoObject.setAttribute("id", HTML5player.videoObjectId);
            HTML5player.videoObject.setAttribute("z-index", HTML5player.zindex);
            //HTML5player.videoObject.setAttribute("poster", HTML5player.poster);
            if (HTML5player.fullScreen) {
                //HTML5player.videoObject.setAttribute("width", HTML5player.fullScreenWidth);
                //HTML5player.videoObject.setAttribute("height", HTML5player.fullScreenHeight);
            } else {
                //HTML5player.videoObject.setAttribute("width", HTML5player.windowWidth);
                //HTML5player.videoObject.setAttribute("height", HTML5player.windowHeight);
            }
            HTML5player.videoDiv.appendChild(HTML5player.videoObject);
            HTML5player.videoObject.innerHTML = "";
            //HTML5player.sourceObject = document.createElement("source");
            HTML5player.videoObject.setAttribute("src", this.videoSrc);
            if (this.mime) {
                HTML5player.videoObject.setAttribute("type", this.mime);
            }
            //HTML5player.videoObject.appendChild(HTML5player.sourceObject);
            HTML5player.currentState = HTML5player.STOPPED;
            HTML5player.initEvents();
        }, load: function(videoSrc) {
            HTML5player.videoSrc = videoSrc;
            HTML5player.init();
        }, initEvents: function() {
            HTML5player.videoObject.addEventListener("ended", function() {
		HTML5player.playStateChange(HTML5player.ENDED, 'ended');
    	    });
	    HTML5player.videoObject.addEventListener("playing", function() {
                HTML5player.playStateChange(HTML5player.STARTPLAYING, 'start playing');
	    });
	    HTML5player.videoObject.addEventListener("progress", function() {
                HTML5player.playStateChange(HTML5player.PROGRESS, 'progress');
	    });
	    HTML5player.videoObject.addEventListener("loadstart", function() {
                HTML5player.playStateChange(HTML5player.PROGRESS, 'load started');
	    });
	    HTML5player.videoObject.addEventListener("loadeddata", function() {
		HTML5player.playStateChange(HTML5player.LOADED, 'data loaded');
	    });
	    HTML5player.videoObject.addEventListener("loadedmetadata", function() {
		HTML5player.playStateChange(HTML5player.LOADEDMETADATA, 'metadata loaded');
	    });
            HTML5player.videoObject.addEventListener("timeupdate", function() { 
        	HTML5player.playStateChange(HTML5player.PLAYING, 'time update');
	    });
	    HTML5player.videoObject.addEventListener("pause", function() {
        	HTML5player.playStateChange(HTML5player.PAUSED, 'paused');
    	    });
    	    HTML5player.videoObject.addEventListener("seeking", function() {
        	HTML5player.playStateChange(HTML5player.SEEKING, 'seeking');
    	    });
	    HTML5player.videoObject.addEventListener("seeked", function() {
                HTML5player.playStateChange(HTML5player.SEEKED, 'seeked');
	    });
            HTML5player.videoObject.addEventListener("stalled", function() {
                HTML5player.playStateChange(HTML5player.ERROR, 'stalled');
	    });
            HTML5player.videoObject.addEventListener("emptied", function() {
                HTML5player.playStateChange(HTML5player.ERROR, 'emptied');
	    });
            HTML5player.videoObject.addEventListener("abort", function() {
                HTML5player.playStateChange(HTML5player.ERROR, 'abort');
	    });
	    HTML5player.videoObject.addEventListener("waiting", function() {
		HTML5player.playStateChange(HTML5player.BUFFERING, 'waiting');
	    });
            HTML5player.videoObject.addEventListener("suspend", function() {
		HTML5player.playStateChange(HTML5player.ERROR, 'suspend');
	    });
    	    HTML5player.videoObject.addEventListener("error", function(e) {
        	HTML5player.playStateChange(HTML5player.ERROR, e);
    	    }); 
        }, play: function(position) {
            try {
                HTML5player.currentState = HTML5player.PLAYING;
                if (position) {
                    HTML5player.startPosition = position;
                    setTimeout("HTML5player.seek(" + HTML5player.startPosition + ");", 500);
                } else {
                    HTML5player.startPosition = 0;
                }
                HTML5player.videoObject.play();
            } catch (e) {
                HTML5player.loadCatchCounter++;
                if (this.loadCatchCounter < 3) {
                    setTimeout(function() {
                        HTML5player.play(HTML5player.startPosition);
                    }, 250);
                    return;
                } else {
                    HTML5player.stopPlaycheckTimer();
                    HTML5player.onerror();
                }
            }
        }, pause: function() {
            if (HTML5player.videoObject && HTML5player.currentState === HTML5player.PLAYING) {
                HTML5player.currentState = HTML5player.PAUSED;
                HTML5player.videoObject.pause();
            }
        }, stop: function() {
            if (HTML5player.videoObject && HTML5player.currentState !== HTML5player.STOPPED) {
                HTML5player.currentState = HTML5player.STOPPED;
                HTML5player.videoObject.pause();
                //HTML5player.videoObject.currentTime = 0;
            }
        }, backward: function() {
            if (!HTML5player.videoObject)
                return;
            var seekToTime = HTML5player.videoObject.currentTime - (HTML5player.seekTime * 30);
            if (seekToTime < 0)
                seekToTime = 0;
            HTML5player.seek(seekToTime);
        }, forward: function() {
            if (!HTML5player.videoObject)
                return;
            var seekToTime = HTML5player.videoObject.currentTime + (HTML5player.seekTime * 30);
            if (seekToTime > HTML5player.duration())
                return;
            HTML5player.seek(seekToTime);
        }, seek: function(seekToTime) {
            if (!HTML5player.videoObject)
                return;
            HTML5player.videoObject.currentTime = seekToTime;
            //HTML5player.videoObject.onseeked();
        }, duration: function() {
            return HTML5player.videoObject.duration;
        }, currentTime: function() {
            return HTML5player.videoObject.currentTime;
        }, playcheckTimeout: 10, playcheckTimer: null, startPlaycheckTimer: function() {
            HTML5player.stopPlaycheckTimer();
            HTML5player.playcheckTimer = setTimeout("HTML5player.onerror();", HTML5player.playcheckTimeout * 1000);
        }, stopPlaycheckTimer: function() {
            if (HTML5player.playcheckTimer)
                clearTimeout(HTML5player.playcheckTimer);
        }, playStateChange : function(status, msg) {
            var funct = null;
            HTML5player.currentState = status;
            switch (status) {
                case HTML5player.PLAYING :
                    funct = HTML5player.onPlayHook;
                    break;
                case HTML5player.BUFFERING :
                    funct = HTML5player.onBufferHook;
                    break;
                case HTML5player.ENDED :
                    funct = HTML5player.onEndHook;
                    break;
                case HTML5player.PAUSED :
                    funct = HTML5player.onPauseHook;
                    break;
                case HTML5player.ERROR :
                    funct = HTML5player.onErrorHook;
                    break;
                case HTML5player.SEEKED :
                    funct = HTML5player.onSeekedHook;
                    break;
                case HTML5player.PROGRESS :
                    funct = HTML5player.onProgressHook;
                    break;
            }
            if (typeof funct === 'function') {
                funct(msg);
            }
            funct = null;
        }, onPlayHook : function() {}, 
        onBufferHook : function() {}, 
        onErrorHook : function() {}, 
        onPauseHook : function() {}, 
        onEndHook : function() {}, 
        onSeekedHook : function() {},
        onProgressHook : function() {}
    };
})();