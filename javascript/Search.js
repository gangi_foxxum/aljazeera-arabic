App.addView((function() {

    // Private variables and functions
    var _navId = null,
        _keyWord = '',
        _cursorHistory = ["0"],
        _cursorPos = 0;

    var _setNav = function(params) {
        _navId = Focus.register({
            // first row
            key_ض: [null, 'key_ص', 'blueInfo', 'key_ش'],
            key_ص: ['key_ض', 'key_صث', 'blueInfo', 'key_س'],
            key_صث: ['key_ص', 'key_ق', 'blueInfo', 'key_ي'],
            key_ق: ['key_صث', 'key_ف', 'blueInfo', 'key_ب'],
            key_ف: ['key_ق', 'key_غ', 'blueInfo', 'key_ل'],
            key_غ: ['key_ف', 'key_ع', 'blueInfo', 'key_ا'],
            key_ع: ['key_غ', 'key_ه', 'blueInfo', 'key_ت'],
            key_ه: ['key_ع', 'key_خ', 'blueInfo', 'key_ن'],
            key_خ: ['key_ه', 'key_ح', 'blueInfo', 'key_م'],
            key_ح: ['key_خ', 'key_ج', 'blueInfo', 'key_ك'],
            key_ج: ['key_ح', 'key_د', 'blueInfo', 'key_ط'],
            key_د: ['key_ج', _showSidebar, 'blueInfo', 'key_BACK'],

            // second row
            key_ش: [, 'key_س', 'key_ض', 'key_ئ'],
            key_س: ['key_ش', 'key_ي', 'key_ص', 'key_ء'],
            key_ي: ['key_س', 'key_ب', 'key_صث', 'key_ؤ'],
            key_ب: ['key_ي', 'key_ل', 'key_ق', 'key_ر'],
            key_ل: ['key_ب', 'key_ا', 'key_ف', 'key_لا'],
            key_ا: ['key_ل', 'key_ت', 'key_غ', 'key_ى'],
            key_ت: ['key_ا', 'key_ن', 'key_ع', 'key_ة'],
            key_ن: ['key_ت', 'key_م', 'key_ه', 'key_و'],
            key_م: ['key_ن', 'key_ك', 'key_خ', 'key_ز'],
            key_ك: ['key_م', 'key_ط', 'key_ح', 'key_ظ'],
            key_ط: ['key_ك', 'key_BACK', 'key_ج', 'key_SPACE'],
            key_BACK: ['key_ط', _showSidebar, 'key_د', 'key_ENTER'],

            // third row
            key_ئ: [, 'key_ء', 'key_ش', _goDown],
            key_ء: ['key_ئ', 'key_ؤ', 'key_س', _goDown],
            key_ؤ: ['key_ء', 'key_ر', 'key_ي', _goDown],
            key_ر: ['key_ؤ', 'key_لا', 'key_ب', _goDown],
            key_لا: ['key_ر', 'key_ى', 'key_ل', _goDown],
            key_ى: ['key_لا', 'key_ة', 'key_ا', _goDown],
            key_ة: ['key_ى', 'key_و', 'key_ت', _goDown],
            key_و: ['key_ة', 'key_ز', 'key_ن', _goDown],
            key_ز: ['key_و', 'key_ظ', 'key_م', _goDown],
            key_ظ: ['key_ز', 'key_SPACE', 'key_ك', _goDown],
            key_SPACE: ['key_ظ', 'key_ENTER', 'key_ط', _goDown],
            key_ENTER: ['key_SPACE', _showSidebar, 'key_BACK', _goDown]
        });
    };

    var _goDown = function() {
        if ($("#carouselWrapper").is(":visible")){
            App.Main.findCarousel("carousel_images_");
        }
    };

    var _showSidebar = function(){
        $("#mainView").css("opacity", "0.4");
        $("#sidebarWrapper").show();
        Focus.moveTo("watchLive");
    };

    var _inputKey = function() {
        var key = $("#"+Focus.getCurrNav().id),
            value = key.attr('data-key');

        switch (value) {
            case 'SPACE' :
                _addChar(' ');
                break;

            case 'BACK' :
                _removeCharacter();
                break;

            case 'ENTER' :
                _doSearch();
            break;

            default :
                _addChar(value);
                break;
        }

        if (_keyWord.length === 0) {
            $('#searchCursor').hide();
        } else {
            $('#searchCursor').show();
        }

        // Garbage collection
        key = value = null;
    };

    var _removeCharacter = function () {
    if (_cursorPos === 0) {
        return;
    }
    _cursorPos--;
    _cursorHistory.pop();
    //$('#searchInnerS').find('span').eq(_cursorPos).remove();
    _keyWord = _keyWord.substring(0, _cursorPos) + _keyWord.substring(_cursorPos + 1, _keyWord.length);
    $('#searchInner').text(_keyWord);
    _updateCursor(false);
    };

        var _addChar = function (char) {
        charK = char === "&nbsp;" ? " " : char;
        _keyWord = [_keyWord.slice(0, _cursorPos), charK, _keyWord.slice(_cursorPos)].join('');
        // SmartLibrary.Logger.log("Search.js", '_keyWord = ' + _keyWord +'charK = ' + charK + '_cursorPos = ' + _cursorPos);
        var searchInput = $('#searchInner'),
            lastChar = searchInput.last(),
            newChar = char;
        searchInput.text(_keyWord);
        _cursorPos++;
        _updateCursor(true, char);
        // Garbage collection
        searchInput = lastChar = newChar = null;
    };

    $.fn.textWidth = function (text, font) {
        if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
        $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
        return $.fn.textWidth.fakeEl.width();
    };

    var _updateCursor = function (adding, char) {
        var cursor = $('#searchCursor'),
            left = 0,
            widthInputCenter = cursor.outerWidth(true),
            characters = $('#searchInner').text().length + 1;
        if (typeof adding === 'undefined') {
            adding = true;
        }
        if (char === " ") {
            left += $("#searchInner").textWidth() + 30;
        }
        else {
            left += $("#searchInner").textWidth() + 10;
        }
        cursor.css('right', left + 'px');
        _cursorHistory.push(left);
    }

    // var _addChar = function(char) {
    //     _keyWord = [_keyWord.slice(0, _cursorPos), char, _keyWord.slice(_cursorPos)].join('');
    //     var searchInput = $('#searchInner'),
    //         lastChar = searchInput.find('span').eq(_cursorPos - 1),
    //         newChar = '<span class="char">' + char + '</span>';
    //     if (lastChar && lastChar.length > 0) {
    //         lastChar.after(newChar);
    //     } else {
    //         searchInput.append(newChar);
    //     }
    //     _cursorPos++;
    //     _updateCursor();
    //
    //     // Garbage collection
    //     searchInput = lastChar = newChar = null;
    // };
    //
    // var _updateCursor = function(adding) {
    //     var cursor = $('#searchCursor'),
    //         left = 0,
    //         widthInputCenter = cursor.outerWidth(true),
    //         characters = $('#searchInner').find('.char');
    //
    //     if (typeof adding === 'undefined') {
    //         adding = true;
    //     }
    //
    //     characters.each(function(index) {
    //         var outerWidth = $(this).outerWidth(true);
    //         if (index < _cursorPos) {
    //             left += outerWidth;
    //         }
    //     });
    //
    //     left += 10;
    //     cursor.css('left', left + 'px');
    //
    //
    //     // Garbage collection
    //     left = cursor = characters = widthInputCenter = null;
    // };

    var _moveCursorToPos = function(pos, adding) {
        if (pos >= 0 && pos <= $('#searchInner').find('.char').length) {
            _cursorPos = pos;
            _updateCursor(adding);
        }
    };

    var _doSearch = function() {
        if (_keyWord && _keyWord.length > 2) {
            $("#splashIcon").show();
            App.Main.doSearch(_keyWord);
        }
    };

    // Public variables and functions
    var view = {};

    view.id = 'Search';
    view.containerID = 'searchView';

    view.onHideOptions = {top: '-100%'};
    view.onShowOptions = {top: 0};

    view.init = function(view) {
        SmartLibrary.Logger.log('Search.js', view);
        _setNav();
    };

    view.deinit = function() {
        SmartLibrary.Logger.log('Search.js', 'deinit');
    };

    view.onHideStart = function() {
        SmartLibrary.Logger.log('Search.js', 'onHideStart');
    };

    view.onHideComplete = function() {
        SmartLibrary.Logger.log('Search.js', 'onHideComplete');
        Focus.unregister(_navId);
        _navId = null;
    };

    view.onShowStart = function() {
    };

    view.onShowComplete = function() {
        SmartLibrary.Logger.log('Search.js', 'onShowComplete');
    };

    view.onHistoryBack = function() {
        App.goToView(App.getViewsMap().VIEW_MAIN);
    };
    view.inputKey = _inputKey;
    return view;

})());
