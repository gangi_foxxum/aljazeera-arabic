App.addView((function() {
    // Private variables and functions
    var _episodesData = "",
        _epgNow = [],
        _epgNext = [],
        _hls = "",
        _lastEpisodeFocused,
        _navIdCounter = 0,
        _navIdCollectionCounter = 0,
        _requestEpg = "",
        _epgData = "",
        _requestUrl = "",
        _requestEpisodes = "",
        _requestEpgData =  "",
        _requestData = new XMLHttpRequest(),
        _requestEpisodesData = new XMLHttpRequest(),
        _requestEpgData =new XMLHttpRequest(),
        _myData = null,
        _categories = null,
        _launcherId =null,
        _contentItem = [],
        _contentEpisodes = [],
        _allCollectionsEpisodes = [],
        _contentCollection = [],
        _contentCount = null,
        _allCategories = [],
        minutesToNow = null,
        minutesToNext = null,
        _collection = null,
        _navigation = null,
        _collectionFiltered = [],
        _itemFiltered = [],
        _getVideo = null,
        filtered = [],
        _liveStreamUrl = null,
         _currentLoc = "main",
        _sidebarFlag = true;

    // Focus

    var _initFocus = function() {
        Focus.init({
          elemId : 'fxm_focus',
          animationSpeed: 0,
          animationMethod: 'css3', // either css or jquery
          onFocus: function(elemId) {
              //console.log(elemId + " gains the focus");
          },
          onBlur: function(elemId) {
              //console.log(elemId + " loses the focus");
          }
        });
    };

    var _setLaunchers = function () {
        _launcherId = Focus.register({
            liveVideoWrapper: [_goToBlueInfo, _showSidebar, _goToBlueInfo, _goToCarousel],
            blueInfo: ["redInfo", _showSidebar, _infoToCarousel, _infoToCarousel],
            redInfo: ["changeLanguageIcon", "blueInfo", _infoToCarousel, _infoToCarousel],
            changeLanguageIcon: [null, "redInfo", _infoToCarousel, _infoToCarousel]
        });
    };

    var _goToBlueInfo = function(){
        if ($("#liveVideoWrapper").css("width") !== "1280px"){
            Focus.moveTo("blueInfo");
        }
    };

    var _infoToCarousel = function(){
        if (App.Main.getCurrentLoc() !== "privacy"){
            if ($("#liveVideoWrapper").is(":visible")=== false) {
                if ($("#carouselWrapper").is(":visible")){
                    App.Main.findCarousel("carousel_images_");
                }
                else if ($("#collectionWrapper").is(":visible")) {
                    App.Main.findCarousel("collection_images_");
                } else if ($("#searchWrapper").is(":visible")){
                    Focus.moveTo("key_Q");
                }
            }
            else {
                Focus.moveTo("liveVideoWrapper");
            }
        }else{
            Focus.moveTo("cookieElem");
        }

    };


    var  _goToCarousel = function(){
        //console.log(_currentLoc);
        if (_currentLoc === "main"){
            $("#liveWrapper").hide();
            $("#liveInfo").hide();
            $("#carouselWrapper").show();
            $("#carouselWrapper").css("top","-350px")
            $("#collectionWrapper").css("top","-350px");
            Focus.moveTo(_navigation.itemIdPrefix + _navigation.currDivNumber);
            videoPlayer.pause();
        }

    };

    var _reverseCarousel = function(time) {
        var translate = 0;
            setTimeout(function(){
                if (_navigation) {
                        _navigation.moveToPos(_navigation.getTotal()-1);
                        translate = (_navigation.getTotal()*256);
                        $(_navigation.itemIdPrefix).css("transform", "translateX(-"+translate+"px)");
                        translate=0;
                        if (time === 500) {
                            Focus.moveTo(_navigation.itemIdPrefix + _navigation.currDivNumber);
                        //    Focus.moveTo("liveVideoWrapper");
                        }
                }
                if (_collection) {
                     _collection.moveToPos(_collection.getTotal()-1);
                        translate = (_collection.getTotal()*256);
                        $(_collection.itemIdPrefix).css("transform", "translateX(-"+translate+"px)");
                        if (time === 500) {
                            Focus.moveTo(_collection.itemIdPrefix + _collection.currDivNumber);
                            // if ($("#carouselWrapper").is(":visible")){
                            //     Focus.moveTo(_navigation.itemIdPrefix + _navigation.currDivNumber);
                            // }
                            if ($("#liveVideoWrapper").is(":visible") && _hls !== "false"){
                                Focus.moveTo("liveVideoWrapper");
                            }else{
                                if ($("#carouselWrapper").is(":visible")){
                                    Focus.moveTo(_navigation.itemIdPrefix + _navigation.currDivNumber);
                                }else {
                                    Focus.moveTo(_collection.itemIdPrefix + _collection.currDivNumber);
                                }

                            }
                        }
                 }
                $("#splashIcon").hide()
            }, time);
    };


    var _goToLiveVideo = function(){
            if (_hls === "false"){
                Focus.moveTo("blueInfo");
            }
            else{
                    $("#liveWrapper").show();
                    $("#liveInfo").show();
                    $("#carouselWrapper").show();
                    //$("#carouselWrapper").animate({ "top": "" }, "slow");
                    $("#carouselWrapper").css("top","");
                    $("#collectionWrapper").css("top","");
                    //$("#collectionWrapper").animate({ "top": "" }, "slow");
                    Focus.moveTo("liveVideoWrapper");
                    videoPlayer.play();
                }

        };


    var _showSidebar = function() {
        if (_sidebarFlag === true){
            $("#mainView").css("opacity", "0.4");
            $("#sidebarWrapper").show();
		  App.Sidebar.checkWatchLive();
        }
    };

    var _goToCollection = function(){
        $("#carouselWrapper").hide();
        $("#collectionWrapper").css("top","-700px");
        Focus.moveTo(_collection.itemIdPrefix + _collection.currDivNumber);
    };

    var _onClickListeners = function() {
        $("#changeLanguageIcon").click(function() {
           $("#mainView").fadeTo("fast" , 0.4);
           Focus.moveTo("english");
           $( "#languageChooserWrapper").show();
        });


        $("#blueInfo").click(function() {
            _showSidebar();
         });

        $("#redInfo").click(function() {
            $("#search").click();
        });

        $("#liveVideoWrapper").click(function() {
            if (($("#liveVideoWrapper").css("width") === "1280px") && ($("#controls").is(":visible")=== false)){
                //console.log("close");
                App.Player.onHistoryBack();
                Focus.moveTo("liveVideoWrapper");
                 videoPlayer.play();
            }
            if (($("#liveVideoWrapper").css("width") !== "1280px") && ($("#controls").is(":visible")=== false)) {
                //console.log("OPEN");
                _sidebarFlag = false;
                //$("#video").remove();
                $("#liveVideoWrapper.focus").css("border-width", "0px");
                App.Main.playLiveStream();
                document.getElementById("videoPlayer").style.width = "1280px";
                document.getElementById("videoPlayer").style.height = "720px";
                $("#liveVideoWrapper").css('width', "1280px");
                $("#liveVideoWrapper").css('height',"720px");
                $("#playerView").css('width', "1280px");
                $("#playerView").css('height',"720px");
                $("#mainVideoObject").css('width', "1280px");
                $("#mainVideoObject").css('height',"720px");
                $("#liveVideoWrapper").css('top',"-147px");
                $("#liveVideoWrapper").css('left',"-595px");
                $("#liveVideoWrapper").css('z-index',"1500");
                $("#video").css('width', "1280px");
                $("#video").css('height',"720px");
            }

        });
    };

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    // Getting Video Data
    var _getData = function(){
            if (getCookie("privacyAccepted") === ""){
                setCookie("language","arabic");
                // Check first if the user has been used the app before, so we check if the user already played a video
                if (typeof window.localStorage.ajVdoSessionID === "undefined") {
                    Focus.register({privacyButtonAgree: ["","","",""]});
                    $("#privacyAgreement").show();
                    Focus.moveTo("privacyButtonAgree");
                }else{
                    Focus.register({updateButtonAgree: ["","","",""]});
                    $("#updateAgreement").show();
                    Focus.moveTo("updateButtonAgree");
                }
            }else {
                $.getJSON('ajax/countryLoc.php', function(languageResponse) {
                    if (getCookie("language") === "") {
                        setCookie("language", languageResponse, 365);
                        if(languageResponse === "english" && !SmartLibrary.$_GET('guid')) {
                            document.location.href = "../aljazeera/" + location.search
                        }
                    }else{
                        if (getCookie("language") === "english" && !SmartLibrary.$_GET('guid')){
                            document.location.href = "../aljazeera/"+location.search
                        }
                    }
                $.getJSON('../getJson-arabic/json/episodes.json', function (data) {
                    _contentItem = data;
                    _liveStreamUrl = "http://aljazeera-ara-apple-live.adaptive.level3.net/apple/aljazeera/arabic/800.m3u8";  // Temp Url
                    $.getJSON('../getJson-arabic/json/collections.json', function (data1) {
                        _contentCollection = data1;
                        _contentItem.reverse();
                        _contentCollection.reverse();
                        $.getJSON('../getJson-arabic/json/allcollectionsepisodes.json', function (data2) {
                            _allCollectionsEpisodes = data2;
                            _playVideoFromGet();
                        });
                    });
                });
            });
        }
    };

    //Getting episodes for each Collection
    var _getCollectionEpisodes = function(guid, title) {
        //    _requestEpisodes = "../getJson-arabic/json/collections/" +encodeURIComponent(guid)+".json";
           $.getJSON('../getJson-arabic/json/collections/' +encodeURIComponent(guid)+'.json', function(data) {
               _contentEpisodes = [];
               _contentEpisodes = data;
               _setCarousel(_contentEpisodes, title);
               _reverseCarousel(500);
               $("#collectionWrapper").hide();
               $("#carouselWrapper").show();
               $("#carouselWrapper").css("top", "-214px");
               $("#episodesInfo").show();
               $(".contentTitle").css("color", "#F57C28");
               Focus.moveTo(_navigation.itemIdPrefix + _navigation.currDivNumber);
           });
           };

   // Function to get EPG today info
   var _getEpgInfo = function() {
        var  currentDate = moment().format("DD-MM-YYYY"),
             currentTime = new Date(),
             epgDate = [],
             epgPrograms = null;
        _requestEpg = "../getJson-arabic/json/epg.json";
        _requestEpgData.open('GET', _requestEpg);
        _requestEpgData.responseType = 'json';
        _requestEpgData.send();
        _requestEpgData.onload = function () {
            if (typeof _requestEpgData.response === "string") {
                var responseJson = JSON.parse(_requestEpgData.response);
            } else {
                var responseJson = _requestEpgData.response;
            }
            _epgData = responseJson["data"]["station"]["days"];
            SmartLibrary.loopCollection(_epgData, function (i, item) {
                epgDate = moment(_epgData[i]["date"]*1000).format("DD-MM-YYYY"); //Converting to 13-digits timestamp to be able to compare.
                if (epgDate === currentDate) {
                    epgPrograms = _epgData[i]["programmes"];
                    currentTime.setMinutes(currentTime.getMinutes() + currentTime.getTimezoneOffset()); // Current Time to UTC 0
                    // Looking for the "now playing" program
                     SmartLibrary.loopCollection(epgPrograms, function (j, item) {
                        var k = Number(j)+1;
                        if (_toTimestamp(epgPrograms[j]["startTime"]) < (currentTime.getTime()/1000) && (currentTime.getTime()/1000) < _toTimestamp(epgPrograms[k]["startTime"])){
                            _epgNow.push({title: epgPrograms[j]["seriesTitle"]["#cdata-section"],
                                          startTime: _toTimestamp(epgPrograms[j]["startTime"])
                                        });
                            _epgNext.push({title: epgPrograms[k]["seriesTitle"]["#cdata-section"],
                                          startTime: _toTimestamp(epgPrograms[k]["startTime"])
                                       });
                        minutesToNow = (((currentTime.getTime()/1000) - _epgNow[0]["startTime"])*0.0166667).toFixed();
                        minutesToNext = (((_epgNext[0]["startTime"] - currentTime.getTime()/1000))*0.0166667).toFixed();
                        }
                    });
                }
            });
            if (_epgNow[0] !== undefined) {
                $("#startedAtNow").append(_language.started.toUpperCase() +" "+ String(minutesToNow).toEasternDigits() +" " + _language.minsAgo.toUpperCase());
                $("#startedAtNext").append(_language.in.toUpperCase() +" "+ String(minutesToNext).toEasternDigits() +" "+ _language.mins.toUpperCase());
                $("#nameOfNow").append(_epgNow[0]["title"]);
                $("#nameOfNext").append(_epgNext[0]["title"]);
            }
            else {
                $("#startedAtNow").append("");
                $("#startedAtNext").append("");
                $("#nameOfNow").append(_language.unavaliable);
                $("#nameOfNext").append(_language.unavaliable);
            }
        };
   };

   // Function to convert Western arabic numbers to Eastern arabic numbers

    String.prototype.toEasternDigits = function () {
        var id = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        return this.replace(/[0-9]/g, function (w) {
            return id[+w];
        });
    };

   // Function to convert HH:MM:SS to timestamp

   var _toTimestamp = function(timeString){
       var operator = timeString.split(":"),
           time = new Date();
        time.setHours(operator[0],operator[1],operator[2]); // HH:MM:SS
        return (time.getTime()/1000);
    };

  // Function to convert milliseconds to minutes and round the number
   var _msToMinutes = function(msvar){
       var minutes = Math.round(msvar/60000);
       return minutes;
   };

   // Function to set the player size and play a video

    var _initPlayer = function(videourl, item) {
        //$("#video").remove();
        App.Player.getVideoInfo(item.name, item.summary, item.length);
        Focus.moveTo('playPause');
        $("#liveWrapper").show();
        $("#liveVideoWrapper").css("width", "1280px");
        $("#liveVideoWrapper").css('height',"720px");
        $("#liveVideoWrapper").css('top',"-147px");
        $("#liveVideoWrapper").css('left',"-595px");
        $("#liveVideoWrapper").css('z-index',"1500");
        $("#progressbar").show();
        $("#timeInfo").show();
        $("#episodesInfo").css("left", "");
        $("#episodesInfo").css("top", "");
        $("#episodesInfo").css("width", "");
        $("#episodesInfo").show();
        $("#controls").css("width", "1280px");
        $("#controls").css("top", "640px");
        $("#controls").css("height", "80px");
        $("#controls").css("padding-left", "60px");
        $("#controls").show();
        App.Player.setUrl(videourl);
        $("#videoPlayer").css('width', "1280px");
        $("#videoPlayer").css('height',"720px");
        setTimeout(function(){Focus.moveTo('playPause')},200);
        $("#playPause").html("&#xf04c;");
        $("#playerView").css('opacity', "1");
        $("#playerView").css('width', "1280px");
        $("#playerView").css('height',"720px");
    };

    // Function to get a parameter from get url and then play video.
    var _playVideoFromGet = function(){
            _getVideo=SmartLibrary.$_GET('guid');
            //console.log(SmartLibrary.$_GET('guid'));
            if (_getVideo !== false){
                var AllEpisodes = _contentItem.concat(_allCollectionsEpisodes)
                for (var i=0; i < AllEpisodes.length; i++){
                    if (typeof AllEpisodes[i] !== "undefined" ){
                        if (AllEpisodes[i].guid === _getVideo) {
                                // App.Player.getVideoInfo(AllEpisodes[i].name, AllEpisodes[i].summary, AllEpisodes[i].length);
                                //#$("#videoPlayer").remove();
                            if (SmartLibrary.$_GET('controls') === "false") {
                                $("#forward").hide();
                                $("#backward").hide();
                            }
                            _initPlayer(AllEpisodes[i].videoUrl, AllEpisodes[i]);
                            ajkeen.AjEvent(AllEpisodes[i].categorieName, AllEpisodes[i].idCategorie, AllEpisodes[i]);
                            Focus.moveTo("playPause");
                            break;
                        }
                    }
                };
            }else{
                _setCarousel(_contentItem, _language.featured);
                _setCollection(_contentCollection, _language.collections);
                App.goToView(App.getViewsMap().VIEW_PLAYER);
            }
        };

        // Function to remove HLS

        var _hlsStatus = function(){
            _hls=SmartLibrary.$_GET('hls');
            if (_hls === "false"){
                _goToCarousel()
		  	$("#watchLive").hide();
                ////console.log("NO HLS VERSION")

            }
            else{
                ////console.log("NORMAL VERSION")
                if (!SmartLibrary.$_GET('guid')){
                    App.Main.playLiveStream();
                }

            }
	     if (SmartLibrary.$_GET('controls') === "false"){
				$("#forward").hide()
				$("#backward").hide()
		}


        };


   // Function to set Carousel

   var _setCarousel = function(object, title) {
       if (object.length > 30){
            object.length = 30;
       }
            var options = {
                parent: 'carouselWrapper',
                carouselWrapperId: 'contentWrapper_' + _navIdCounter,
                carouselWrapperClass: 'contentWrapper',
                carouselTitleWrapperClass: 'contentTitleWrapper',
                carouselTitleClass: 'contentTitle',
                carouselTitle: (title === _language.featured || title === _language.results) ? title : _language.latestEpisodes,
                carouselInnerClass: 'contentInner',
                carouselClass: 'contentCarousel acceleratedElem',
                containerId: 'carousel_images_' + _navIdCounter,
                itemIdPrefix: 'carousel_images_' + _navIdCounter + '_',
                horizontal: true,
                isMatrix: false,
                clear: true,
                itemHtmlTemplate:'<li id="{{itemIdPrefix}}{{itemId}}" class="content clickable">' +
                                                '<div class="contentImageWrapper">' +
                                                '<img class="contentImage acceleratedElem" />' +
                                                '</div>' +
                                                '<div class="contentNameWrapper">' +
                                                    '<p class="contentName"></p>' +
                                                '</div>' +
                                                 '<div class="contentDurationWrapper">' +
                                                    '<p class="contentDuration"></p> ' +
                                                '</div>' +
                                            '</li>',
                itemsArray: object,
                itemsPerPage: 2,
                maxVisibleItems: object.length,
                speed: 400,
                onInitialized: function() {
                    //console.log("carousel Initialized");

                     if (SmartLibrary.$_GET('guid') === false){
                        if ($("#liveWrapper").is(":visible")) {
                        Focus.moveTo("liveVideoWrapper");
                        // _hlsStatus()

                        }
                        if (!$("#liveWrapper").is(":visible") && App.Main.getCurrentLoc() !== "search" ){
                            Focus.moveTo(this.itemIdPrefix + this.currDivNumber);
                        }else{
                            if (App.Main.getCurrentLoc()=== "search") {
                                //console.log("here")
                                if (object.length === 0){
                                    //console.log("now here")
                                    $("#splashIcon").hide();
                                    Focus.moveTo("key_ENTER");
                                }
                                else {
                                    Focus.moveTo(this.itemIdPrefix + this.currDivNumber);
                                }
                        }

                        }

                    }
                },
                onAddingItem: function(index, item) {
                    var elem = $(document.getElementById(this.itemIdPrefix + index)),
                    length = String(_msToMinutes(item.length)).toEasternDigits();
                    elem.find('.contentImage').attr('src', item.imageUrl);
                    elem.find('.contentName').html(item.name);
                    elem.find('.contentDuration').html(length + " " + _language.mins.toUpperCase());
                 // Garbage collection
                    elem = null;
                },

                onRemovingItem: function(index, item) {
                    var elem = $(document.getElementById(this.itemIdPrefix + index));
                    elem.find('.contentImage').attr('src', '');

                    // Garbage collection
                    elem = null;
                },
                onMove: function(dir) {
                    switch (dir) {
                        case 'up':
                            if ($("#carouselWrapper").css("z-index") !== "3000") {
                                if ($("#collectionWrapper").is(":visible")) {
                                  _goToLiveVideo();
                                }
                                if ($("#searchWrapper").is(":visible")){
                                    Focus.moveTo('key_ض');
                                }
                                 if ($("#episodesInfo").is(":visible")){
                                    Focus.moveTo("blueInfo");
                                }
                            }

                             break;
                        case 'down':
                            if ($("#carouselWrapper").css("z-index") === "3000") {
                                Focus.moveTo("showCarousel");
                                $("#carouselWrapper").css("z-index", "");
                            }
                            else {
                                if ($("#collectionWrapper").is(":visible")) {
                                    _goToCollection();
                                }
                            }
                            break;
                        case 'left':
                            if (this.currDivNumber === (object.length-1)) {
                                   $(".contentInner").css("margin-left","458px");
                            };
                            if ($("#carouselWrapper").css("z-index") === "3000"){
                                if ($("liveVideoWrapper").is(":visible")){
                                    App.Player.showControls();
                               }
                            }
                            break;
                        case 'right':
                            if ($("#carouselWrapper").css("z-index") !== "3000") {
                               if (this.currDivNumber === (object.length-1)) {
                                   _showSidebar();
                               }
                            }else{
                                if ($("liveVideoWrapper").is(":visible")){
                                    App.Player.showControls();
                               }
                            }
                            break;
                    }
                },
                onFocus: function(item) {
                    _lastEpisodeFocused = Focus.getCurrNav().id;
                    if (title !== _language.featured){
                        $("#title").html(title);
                        $("#name").html(item.name);
                        $("#summary").html(item.summary);
                    }

                },
                onBlur: function() {},
                onItemSelected: function(item) {
                    App.Player.liveVideo = false;
                    //
                    if  ($("#carouselWrapper").css("z-index") === "3000") {
                         ajkeen.AjVideoSession('playlist');
                         $("#carouselWrapper").css("z-index", "");
                     }
                    // document.location.reload(true)
                    $("#splashIcon").show();
                    setTimeout(function(){_initPlayer(item.videoUrl, item)},200);
                    ajkeen.AjEvent(item.categorieName, item.idCategorie, item);
                }
            };
            _navIdCounter++;
            _navigation = new Carousel(options);
            Focus.register(_navigation);
        };

       // Setting collection's carousel

     var _setCollection = function(object, title) {
            var options = {
                parent: 'collectionWrapper',
                carouselWrapperId: 'contentColWrapper_' + _navIdCollectionCounter,
                carouselWrapperClass: 'contentColWrapper',
                carouselTitleWrapperClass: 'contentColTitleWrapper',
                carouselTitleClass: 'contentColTitle',
                carouselTitle: title,
                carouselInnerClass: 'contentColInner',
                carouselClass: 'contentCollection acceleratedElem',
                containerId: 'collection_images_'+_navIdCollectionCounter,
                itemIdPrefix: 'collection_images_'+_navIdCollectionCounter+ '_',
                horizontal: true,
                isMatrix: false,
                clear: true,
                itemHtmlTemplate:'<li id="{{itemIdPrefix}}{{itemId}}" class="content collectionClickable">' +
                                                '<div class="contentColImageWrapper">' +
                                                '<img class="contentColImage acceleratedElem" />' +
                                                '</div>' +
                                                '<div class="contentColNameWrapper">' +
                                                    '<p class="contentColName"></p>' +
                                            '</li>',
                itemsArray: object,
                itemsPerPage: 2,
                maxVisibleItems: object.length,
                speed: 400,
                onInitialized: function() {
                    // carousel initialized! do whatever you want now...
                    //console.log("Collection's carousel Initialized");
                    _hlsStatus()
                   //Focus.moveTo(this.itemIdPrefix + this.currDivNumber); // I want you to gain the focus :)
                },
                onAddingItem: function(index, item) {
                    var elem = $(document.getElementById(this.itemIdPrefix + index));
                    elem.find('.contentColImage').attr('src', item.background);
                 // Garbage collection
                    elem = null;
                },

                onRemovingItem: function(index, item) {
                    var elem = $(document.getElementById(this.itemIdPrefix + index));
                    elem.find('.contentColImage').attr('src', '');

                    // Garbage collection
                    elem = null;
                },
                onMove: function(dir) {
                    switch (dir) {
                        case 'up':
                          if (App.Main.getCurrentLoc !== "collections") {
                                _goToCarousel();
                            }
                             if ($("#carouselWrapper").is(":visible")=== false){
                                Focus.moveTo("blueInfo");
                            }
                             break;
                        case 'down':
                             break;
                        case 'right':
                               if (this.currDivNumber === (object.length-1)) {
                                   _showSidebar();
                               };
                        break;
                        case 'left':
                        if (this.currDivNumber === (object.length - 1)) {
                            $(".contentColInner").css("margin-left", "458px");
                        };
                        break;
                    }
                },
                onFocus: function(item) {
                        $("#name").html(item.name);
                        $("#summary").html(item.summary);
                },
                onBlur: function() {},
                onItemSelected: function(item) {
                        //console.log("selected")
                     App.Player.liveVideo = false;
                    // //console.log("item was clicked: ", this);
                    App.Main.setCurrentLoc("episodes");
                    $("#splashIcon").show();
                    // $("#episodesInfo").show();
                     window.localStorage.setItem('prevCa    tegorie', window.localStorage.getItem('currentCategorie'));
                     window.localStorage.setItem('currentCategorie', item.name);
                     ajkeen.AJNavigate(window.localStorage.getItem('prevCategorie'), window.localStorage.getItem('currentCategorie'));
                    _getCollectionEpisodes(item.guid, item.name);
                }
            };
            _navIdCollectionCounter++;
            _collection = new Carousel(options);
            Focus.register(_collection);
            $("#collectionWrapper").show();
            //console.log("end")
            if (title !== _language.collections) {
                _reverseCarousel(500);
                Focus.moveTo(_collection.itemIdPrefix + _collection.currDivNumber);
            }
            else {
                _reverseCarousel(500);
            }
        };



    // Public variables and functions
    var view = {};

    view.id = 'Main';
    view.containerID = 'mainView';

    view.onHideOptions = {left: '-100%'};
    view.onShowOptions = {left: 0};

    view.onLoad = function() {};


    view.onUnload = function() {};

    view.init = function(view) {
        window.localStorage.setItem('currentCategorie', "Home");
        _initFocus();
        _setLaunchers();
        _getData();
        _getEpgInfo();
        _onClickListeners();
        $("#searchView").hide();
        $("#sidebarWrapper").hide();
        $("#languageChooserWrapper").hide();
        $("#episodesInfo").hide();
        $("#timeInfo").hide();
     };

    view.deinit = function() {};

    view.onHideStart = function() {};
    view.onHideComplete = function() {};

    view.onShowStart = function() {};
    view.onShowComplete = function() {};

    view.onHistoryBack = function() {
        //console.log("back");
    };

    view.collectionFilter = function(categorie){
        window.localStorage.setItem('prevCategorie', window.localStorage.getItem('currentCategorie'));
        window.localStorage.setItem('currentCategorie', categorie);
        ajkeen.AJNavigate(window.localStorage.getItem('prevCategorie'), window.localStorage.getItem('currentCategorie'));
        _collectionFiltered = [];
        SmartLibrary.loopCollection(_contentCollection, function (index, item) {
            if (_contentCollection[index]["categorieName"] === categorie){
                _collectionFiltered.push({    id: item["id"],
                                              idCategorie: item["idCategorie"],
                                              categorieName: item["categorieName"],
                                              guid: item["guid"],
                                              name: item["name"],
                                              summary: item["summary"],
                                              background: item["background"]

                                        });

            }
            });
            $("#liveWrapper").hide();
            $("#liveInfo").hide();
            $("#carouselWrapper").hide();
            $("#collectionWrapper").css("top","-636px");
            //$(".contentColTitleWrapper").hide();
            _setCollection(_collectionFiltered, categorie.toUpperCase());
    };

    view.restoreCarousel = function() {
        _getData();
    };

    view.itemFilter = function (categorie) {
        _itemFiltered = [];
        SmartLibrary.loopCollection(_contentItem, function (index, item) {
            if (item["categorieName"] === categorie) {
                _itemFiltered.push({id: item["id"],
                    idCategorie: item["idCategorie"],
                    categorieName: item["categorieName"],
                    guid: item["guid"],
                    name: item["name"],
                    summary: item["summary"],
                    imageUrl: item["imageUrl"],
                    videoUrl: item["videoUrl"],
                    length: item["length"],
                    date: item["date"]
                });
            }
        });

      _setCarousel(_itemFiltered, categorie.toUpperCase());
      _reverseCarousel(500);
    };

    view.setSidebarFlag= function(condition){
        _sidebarFlag = condition;
    }

    view.sidebarFlag = function() {
        return _sidebarFlag;
    }

    view.doSearch = function(keyword) {
        filtered = [];
        for (var i=0; i < keyword.split(" ").length; i++){
            SmartLibrary.loopCollection(_allCollectionsEpisodes, function (index, item) {
                       if(item["name"].toUpperCase().indexOf(keyword.split(" ")[i]) !== -1 || item["summary"].toUpperCase().indexOf(keyword.split(" ")[i]) !== -1){
                            filtered.push({
                                                      guid: item["guid"],
                                                      name: item["name"],
                                                      summary: item["summary"],
                                                      imageUrl: item["imageUrl"],
                                                      videoUrl: item["videoUrl"],
                                                      length: item["length"],
                                                      date: item["date"]
                          });
                        }
                });
            SmartLibrary.loopCollection(_contentItem, function (index, item) {
                       if(item["name"].toUpperCase().indexOf(keyword.split(" ")) !== -1 || item["summary"].toUpperCase().indexOf(keyword.split(" ")) !== -1){
                            filtered.push({
                                                      guid: item["guid"],
                                                      name: item["name"],
                                                      summary: item["summary"],
                                                      imageUrl: item["imageUrl"],
                                                      videoUrl: item["videoUrl"],
                                                      length: item["length"],
                                                      date: item["date"]
                          });
                        }
                });
        }

        // Look for duplicate results.

           for (i=0; i < filtered.length; i++){
                var flag = 0;
                for(var j=0; j < filtered.length; j++) {
                    if (filtered[i]["guid"] === filtered[j]["guid"]){
                        flag++;
                        if (flag > 1){
                            filtered.splice([j], 1);
                        }
                    }
                }
            };

        // Sort by date
            filtered.sort(function(a, b) {
                    if (a.date > b.date) {
                            return - 1;
                        }
                    if (a.date < b.date) {
                        return 1;
                    }
                });

        if (filtered.length > 1){
            _setCarousel(filtered.reverse(), _language.results);
            _reverseCarousel(500);
            $("#carouselWrapper").show();
            $("#carouselWrapper").css("top", "-150px");
        }else{
            $("#splashIcon").hide();

        }

        //$("#episodesInfo").show();
        //$("#episodesInfo").css("left", "600px");
        //$("#episodesInfo").css("top", "182px");
        //$("#episodesInfo").css("width", "530");
    };

    view.findCarousel = function (target) {
        if (target === "collection_images_"){
            Focus.moveTo(_collection.itemIdPrefix + _collection.currDivNumber);
        }
        if (target === "carousel_images_"){
            Focus.moveTo(_navigation.itemIdPrefix + _navigation.currDivNumber);
        }
    };

    view.playLiveStream = function(){
        App.Player.setUrl(_liveStreamUrl);
    };

    view.getLivestreamUrl = function(){
        return _liveStreamUrl;
    }

    view.getCurrentLoc = function (){
        return _currentLoc;
    }
    view.setCurrentLoc = function (loc){
        _currentLoc = loc;
    }
    view.getLastEpisodeFocused = function(){
        return _lastEpisodeFocused;
    };
    view.AcceptAgreement = function (){
        setCookie("privacyAccepted", "true", 365);
        setTimeout(function () {
            location.reload(true);
        },600);
    };
    view.openPrivacy = function (type){
        var title = $(".privacyViewTitle");
        var text = $(".privacyViewText");
        var url = $(".privacyViewUrl");
        if (type === "cookie"){
            title.text("سياسة ملفات تعريف الارتباط");
            text.text(" لقراءة سياسة ملفات الارتباط لدى الجزيرة، اذهبوا إلى: ");
            url.text("network.aljazeera.com/ar/cookies");
        }else{
            title.text("سياسة الخصوصية");
            text.text("لقراءة سياسة الخصوصية الخاصة بالجزيرة ، انتقل إلى:");
            url.text("network.aljazeera.com/ar/privacy");
        }
        $("#privacyView").show();
        Focus.register({privacyViewButton: ["","","",""]});
        Focus.moveTo("privacyViewButton");
    };

    view.backToMoreTab = function(){
        $("#privacyView").hide();
        if ($(".privacyViewUrl").text() === "network.aljazeera.com/ar/cookies"){
            Focus.moveTo("cookieElem");
        }else{
            Focus.moveTo("privacyElem");
        }
    };
    view.hlsStatus = _hlsStatus;
    view.setCookie = setCookie;
    view.getCookie = getCookie;

    return view;
})());
