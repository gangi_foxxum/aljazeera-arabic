App.addView((function() {
    // Private variables and functions
    var _languageId =null;


    var _setLanguage = function () {
        _languageId = Focus.register({
                english: [_hideLanguageChooser, _hideLanguageChooser, null, "arabic"],
                arabic:[_hideLanguageChooser, _hideLanguageChooser, "english", null]
        });
    };

    var _hideLanguageChooser = function() {
        $("#languageChooserWrapper").hide();
        $("#mainView").css({ opacity: 1 });
        Focus.moveTo("liveVideoWrapper");
    };

    var _setListeners = function(){
    $("#english").click(function(){
        App.Main.setCookie("language", "english", 365);
	   document.location.href = "../aljazeera/"+location.search
    });

    $("#arabic").click(function(){
        App.Main.setCookie("language", "arabic", 365);
        document.location.href = "../aljazeera-arabic/"+location.search
    });
   };

      // Public variables and functions
    var view = {};

    view.id = 'languageChosser';
    view.containerID = 'languageChosserView';

    view.onHideOptions = {left: '-100%'};
    view.onShowOptions = {left: 0};

    view.onLoad = function() {};
    view.onUnload = function() {};

    view.init = function(view) {
       _setLanguage();
       _setListeners();
    };
    view.deinit = function() {};

    view.onHideStart = function() {};
    view.onHideComplete = function() {};

    view.onShowStart = function() {};
    view.onShowComplete = function() {};

    view.onHistoryBack = function() {};

    return view;
})());
