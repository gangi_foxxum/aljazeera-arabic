/* Utilities
 *
 * App utility methods
 *
 */

(function (exports) {
  "use strict";
  function AJKeen() {
    // make it a singleton
    var selectedVideo = '';
    var selectedPlaylist = '';
    if (exports.ajkeen) {
      return ajkeen;
    }

    //Keen.io details come here
    var client = new Keen({
      projectId: "588f37508db53dfda8a84fbd",
      writeKey: "039136F1D4DF19F42AB37E1F12E81EF730919FACB5DA41DF23F2D6D14DCEBBCA9B598C0FC7CA2F9145EBA32944C895B7ADD5751295AE46B11FF80553DCA51E5763F3FB8C2C38C682765141B2CA38656E365DD3BD17051B6D80CA92B90EB212F8"
    });

    window.localStorage.setItem('Lang', "AR");

    var getDeviceID = function () {
      try {
        var thisC = window.localStorage.getItem('ajID');
        if (thisC.length > 0) {

        }
        else {
          window.localStorage.setItem('ajID',
            Math.random().toString(36).substr(2, 9),
            {
              expires: 1078
            });
        }
        return window.localStorage.getItem('ajID');
      }
      catch (r) {
        window.localStorage.setItem('ajID',
          Math.random().toString(36).substr(2, 9),
          {
            expires: 1078
          });

        return window.localStorage.getItem('ajID');
      }
    };

    var getOSVersion = function () {
      if (typeof (clientInfo) !== "undefined" && typeof (clientInfo.getOSVersion()) !== "undefined"){
        return clientInfo.getOSVersion().toString();
      }
      else {
        return "n/a";
      }
    };

    var getDevice = function () {
     if (typeof (clientInfo) !== "undefined" && typeof (clientInfo.getDevice()) !== "undefined"){
        return clientInfo.getDevice().toString();
      }
      else {
        return "n/a";

      }
    };

    var getDeviceType = function () {
     if (typeof (clientInfo) !== "undefined" && typeof (clientInfo.getDeviceType()) !== "undefined"){
        return clientInfo.getDeviceType().toString();
      }
      else {
        return "n/a";

      }
    };

    var setSessionID = function () {
      window.localStorage.setItem('ajSessionID',
        Math.random().toString(36).substr(2, 9),
        {
          expires: 30
        });

    };

    var setVdoSessionID = function () {
      window.localStorage.setItem('ajVdoSessionID',
        Math.random().toString(36).substr(2, 9),
        {
          expires: 30
        });
    };

    var setVdoData = function (vdoData) {
        var vdoDataMod = {
        id: vdoData.id,
        idCategorie: vdoData.idCategorie,
        categorieName: vdoData.categorieName,
        guid: vdoData.guid,
        name: vdoData.name,
        summary: vdoData.summary,
        imageUrl: vdoData.imageUrl,
        videoUrl: vdoData.videoUrl,
        length: (vdoData.length/1000),
        date: vdoData.date
        };
      window.localStorage.setItem('currVdo', JSON.stringify(vdoDataMod));
    };

    // This is common data that we need to track on Keen.io
    var keenCommonData = {
      timestamp: new Date().toISOString(),
      "addons": [
        {
          "name": "keen:ip_to_geo",
          "input": {
            "ip": "ip_address"
          },
          "output": "ip_geo_info"
        },
        {
          "name": "keen:ua_parser",
          "input": {
            "ua_string": "user_agent"
          },
          "output": "parsed_user_agent"
        }
      ]
    };

    var deviceInfo = {
        id: getDeviceID().toString(),
        model: getDevice(),
        version: getOSVersion(),
        family: getDeviceType(),
        sessionid: window.localStorage.getItem('ajSessionID')
    };

    //This is the definition of launch event
    var AJLaunched = {
      content: { type: "launch", method: "fromAppIcon" },
      device: deviceInfo,
      "ip_address": "${keen.ip}",
      "user_agent": "${keen.user_agent}",
      "keen": keenCommonData
    };

    //This is the definition of exit event
    var AJExit = {
      content: { type: "exit", method: "fromAppIcon" },
       device: deviceInfo,
      "ip_address": "${keen.ip}",
      "user_agent": "${keen.user_agent}",
      "keen": keenCommonData
    };

    //This is to save launch event on keen.io
    $(document).ready(function () {
      setSessionID();
      //console.log(AJLaunched);
      client.addEvent("session", AJLaunched);
    });

    //This is to  save video from playlist
    this.AjEvent = function (_from, _fromID,vdoData) {
      var AJNew = [];
      setVdoSessionID();
      setVdoData(vdoData);
      AJNew = {
        content: {
          type: "playbackAction",
          fromTitle: _from,
          fromId: _fromID,
          videoURL: vdoData.video,
          id: vdoData.guid,
          title: vdoData.name,
          refData: vdoData,
          videoDuration: vdoData.length,
          vdoSessionID: window.localStorage.getItem('ajVdoSessionID'),
          channel: window.localStorage.getItem('Lang')
        },
        device: deviceInfo,
        "ip_address": "${keen.ip}",
        "user_agent": "${keen.user_agent}",
        "keen": keenCommonData
      };
      console.log(AJNew);
      client.addEvent("videoPlayback", AJNew);
      return '';
    };

    //This is to  save next video automatically played
    this.AjVideoSession = function (eventType) {
      var pdata = JSON.parse(window.localStorage.getItem('currVdo'));
      var AJNextP = [];
      AJNextP = {
        content: {
          endReason: eventType,
          from: "video",
          id: pdata.guid,
          title: pdata.name,
          videoURL: pdata.videoUrl,
          videoDuration: pdata.length,
          playbackDuration: videoPlayer.currentTime,
          vdoSessionID: window.localStorage.getItem('ajVdoSessionID'),
          channel: window.localStorage.getItem('Lang')
        },
        device: deviceInfo,
        "ip_address": "${keen.ip}",
        "user_agent": "${keen.user_agent}",
        "keen": keenCommonData
      };
      console.log(AJNextP);
      client.addEvent("videoSession", AJNextP , function(err, res) {
        if (err) {
            console.log(err);
        } else {
            console.log("Hooray, it worked!");
        }
        });
      return '';
    };

    //This is to track change in the playlist
    this.AJNavigate = function (_prev, _cur) {
      var AJClick = {
        content: {
          title: _cur,
          id: '',
          fromTitle: _prev,
          fromID : '',
          type: "playlist"
        },
        device: deviceInfo,
        channel: window.localStorage.getItem('Lang'),
        "ip_address": "${keen.ip}",
        "user_agent": "${keen.user_agent}",
        "keen": keenCommonData
      };
      if (_cur !== _prev) {
        //console.log(AJClick);
        client.addEvent("navigate", AJClick);
        window.localStorage.setItem('prevMenu', _cur);
      }
      return '';
    };

    //This method tracks AppCycle Exit Event.
    this.ExitAppEvent = function () {
      //console.log(AJExit);
      client.addEvent("session", AJExit);
    }.bind(this);


  };

  exports.AJKeen = AJKeen;
} (window));
