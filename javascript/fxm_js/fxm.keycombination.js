(function() {
    fxm.keycombination = {
        timer: null,
        timerDelay: 1000,
        keyCombination: null,
        startTimer: function() {
            if (this.timer) {
                clearTimeout(this.timer);
            }
            this.timer = setTimeout('fxm.keycombination.clear()', this.timerDelay);
        },
        clear: function() {
            this.keyCombination = null;
        },
        check: function(key) {
            if (!this.keyCombination) {
                this.keyCombination = String.fromCharCode(key);
            } else {
                this.keyCombination += String.fromCharCode(key);
            }
            if (this.callback && typeof(this.callback) == 'function') this.callback(String(this.keyCombination));
            else this.clear();
            this.startTimer();
        },
        callback: null
    };
    fxm.keycombination.constructor.prototype = new fxm.object;
})();