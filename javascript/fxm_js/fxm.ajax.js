
(function() {
    fxm.ajax = {};
    fxm.ajax.block = false;
    fxm.ajax.showLoading = false;
    fxm.ajax.post = function(url, params, callbackFunction, blocking) {
        var clbck = callbackFunction;
        var http = new XMLHttpRequest();
        http.open("POST", url);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.setRequestHeader("Content-length", params.length);
        http.setRequestHeader("Connection", "close");
        http.onreadystatechange = function() {
            if (http.readyState == 4) {
                if (fxm.ajax.showLoading) {
                    fxm.hideLoading();
                } else if (fxm.ajax.block) {
                    fxm.unblockKeys();
                }
                if (http.status == 200) {
                    clbck(http.responseText);
                } else {
                    clbck(null);
                }
                clbck = null;
                http = null;
            }
        };
        if (fxm.ajax.showLoading) {
            fxm.showLoading();
        } else if (fxm.ajax.block) {
            fxm.blockKeys();
        }
        http.send(params);
        url = null;
        params = null;
        callbackFunction = null;
        blocking = null;
    };
    fxm.ajax.get = function(url, params, callbackFunction, blocking) {
        var clbck = callbackFunction;
        var http = new XMLHttpRequest();
        if (params.length > 0)
            url = url + '?' + params;
        http.open("GET", url);
        http.onreadystatechange = function() {
            if (http.readyState == 4) {
                if (fxm.ajax.showLoading) {
                    fxm.hideLoading();
                } else if (fxm.ajax.block) {
                    fxm.unblockKeys();
                }
                if (http.status == 200) {
                    clbck(http.responseText);
                } else {
                    clbck(null);
                }
                clbck = null;
                http = null;
            }
        };
        if (fxm.ajax.showLoading) {
            fxm.showLoading();
        } else if (fxm.ajax.block) {
            fxm.blockKeys();
        }
        http.send();
        url = null;
        params = null;
        callbackFunction = null;
        blocking = null;
    };
    fxm.ajax.getXML = function(url, params, callbackFunction, blocking) {
        var clbck = callbackFunction;
        var http = new XMLHttpRequest();
        if (params.length > 0)
            url = url + '?' + params;
        http.open("GET", url);
        http.onreadystatechange = function() {
            if (http.readyState == 4) {
                if (fxm.ajax.showLoading) {
                    fxm.hideLoading();
                } else if (fxm.ajax.block) {
                    fxm.unblockKeys();
                }
                if (http.status == 200) {
                    clbck(http.responseXML);
                } else {
                    clbck(null);
                }
                clbck = null;
                http = null;
            }
        };
        if (fxm.ajax.showLoading) {
            fxm.showLoading();
        } else if (fxm.ajax.block) {
            fxm.blockKeys();
        }
        http.send();
        url = null;
        params = null;
        callbackFunction = null;
        blocking = null;
    };
})();