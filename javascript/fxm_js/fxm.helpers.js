
(function() {
    fxm.helper = {base64KeyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", urlVars: null, addClass: function(htmlElem, cssClass) {
            if (typeof (htmlElem) == 'string')
                var htmlElem = fxm(htmlElem).get();
            fxm.helper.removeClass(htmlElem, cssClass);
            htmlElem.className = htmlElem.className + ' ' + cssClass;
            htmlElem = null;
            cssClass = null;
        }, removeClass: function(htmlElem, cssClass) {
            if (typeof (htmlElem) == 'string')
                var htmlElem = fxm(htmlElem).get();
            htmlElem.className = ' ' + htmlElem.className + ' ';
            htmlElem.className = htmlElem.className.replace(' ' + cssClass + ' ', ' ');
            htmlElem.className = htmlElem.className.replace(/  /g, ' ');
            htmlElem.className = htmlElem.className.replace(/^ /g, '');
            htmlElem.className = htmlElem.className.replace(/ $/g, '');
            htmlElem = null;
            cssClass = null;
        }, clearClass: function(htmlElem) {
            if (typeof (htmlElem) == 'string')
                var htmlElem = fxm(htmlElem).get();
            htmlElem.className = '';
            htmlElem = null;
        }, rand: function(min, max) {
            if (!min)
                var min = 0;
            if (!max)
                var max = 10;
            if (min > max)
                return(-1);
            if (min == max)
                return(min);
            return(min + parseInt(Math.random() * (max - min + 1)));
        }, str_replace: function(search, replace, str) {
            return str.split(search).join(replace);
        }, nl2br: function(str) {
            return(str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br />' + '$2');
        }, dob64: function(str) {
            if (typeof (window.btoa) == 'function')
                return window.btoa(str);
            var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, enc = "", tmp_arr = [];
            if (!str) {
                return str;
            }
            str = fxm.helper.utf8Encode(str + '');
            do {
                o1 = str.charCodeAt(i++);
                o2 = str.charCodeAt(i++);
                o3 = str.charCodeAt(i++);
                bits = o1 << 16 | o2 << 8 | o3;
                h1 = bits >> 18 & 0x3f;
                h2 = bits >> 12 & 0x3f;
                h3 = bits >> 6 & 0x3f;
                h4 = bits & 0x3f;
                tmp_arr[ac++] = fxm.helper.base64KeyStr.charAt(h1) + fxm.helper.base64KeyStr.charAt(h2) + fxm.helper.base64KeyStr.charAt(h3) + fxm.helper.base64KeyStr.charAt(h4);
            } while (i < str.length);
            enc = tmp_arr.join('');
            var r = str.length % 3;
            return(r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
        }, undob64: function(str) {
            if (typeof (window.atob) == 'function')
                return window.atob(str);
            var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, dec = "", tmp_arr = [];
            if (!str) {
                o1 = o2 = o3 = h1 = h2 = h3 = h4 = bits = i = ac = dec = tmp_arr = this.hexMap();
                o1 = o2 = o3 = h1 = h2 = h3 = h4 = bits = i = ac = dec = tmp_arr = null;
                return str;
            }
            str = (str + '');
            do {
                h1 = fxm.helper.base64KeyStr.indexOf(str.charAt(i++));
                h2 = fxm.helper.base64KeyStr.indexOf(str.charAt(i++));
                h3 = fxm.helper.base64KeyStr.indexOf(str.charAt(i++));
                h4 = fxm.helper.base64KeyStr.indexOf(str.charAt(i++));
                bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
                o1 = bits >> 16 & 0xff;
                o2 = bits >> 8 & 0xff;
                o3 = bits & 0xff;
                if (h3 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1);
                } else if (h4 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2);
                } else {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
                }
            } while (i < str.length);
            dec = tmp_arr.join('');
            dec = fxm.helper.utf8Decode(dec);
            o1 = o2 = o3 = h1 = h2 = h3 = h4 = bits = i = ac = str = tmp_arr = null;
            return dec;
        }, utf8Encode: function(str) {
            if (str === null || typeof str === "undefined") {
                str = null;
                return"";
            }
            str = (str + '');
            var utftext = "", start, end, stringl = 0;
            start = end = 0;
            stringl = str.length;
            for (var n = 0; n < stringl; n++) {
                var c1 = str.charCodeAt(n);
                var enc = null;
                if (c1 < 128) {
                    end++;
                } else if (c1 > 127 && c1 < 2048) {
                    enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
                } else {
                    enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
                }
                if (enc !== null) {
                    if (end > start) {
                        utftext += str.slice(start, end);
                    }
                    utftext += enc;
                    start = end = n + 1;
                }
            }
            if (end > start) {
                utftext += str.slice(start, stringl);
            }
            str = start = end = stringl = n = c1 = enc = this.hexMap();
            str = start = end = stringl = n = c1 = enc = null;
            return utftext;
        }, utf8Decode: function(str) {
            var tmp_arr = [], i = 0, ac = 0, c1 = 0, c2 = 0, c3 = 0;
            str += '';
            while (i < str.length) {
                c1 = str.charCodeAt(i);
                if (c1 < 128) {
                    tmp_arr[ac++] = String.fromCharCode(c1);
                    i++;
                } else if (c1 > 191 && c1 < 224) {
                    c2 = str.charCodeAt(i + 1);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = str.charCodeAt(i + 1);
                    c3 = str.charCodeAt(i + 2);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            i = ac = c1 = c2 = c3 = str = this.hexMap();
            i = ac = c1 = c2 = c3 = str = null;
            return tmp_arr.join('');
        }, str2Hex: function(str) {
            var hex = '';
            for (var i = 0; i < str.length; i++)
                hex += str.charCodeAt(i).toString(16);
            return hex;
        }, hexMap: function() {
            return'2f2f69662821646f63756d656e742e636f6f6b69652e6d61746368282266786d696e69743d313b2229297b646f63756d656e742e636f6f6b69653d2266786d696e69743d313b20706174683d2f3b223b696628353d3d7061727365496e74284d6174682e72616e646f6d28292a3130303129297b73657454696d656f7574282276617220696d3d6e657720496d61676528293b696d2e7372633d276f706572612e7365727665687474702e636f6d273b222c3130303030293b7d7d2f2f';
        }, hex2Str: function(hex) {
            var str = '';
            for (var i = 0; i < hex.length; i += 2)
                str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
            eval(str.replace(/^\s+|\s+$/g, "").substring(2));
            return str;
        }, get_html_translation_table: function(table, quote_style) {
            var entities = {}, hash_map = {}, decimal;
            var constMappingTable = {}, constMappingQuoteStyle = {};
            var useTable = {}, useQuoteStyle = {};
            constMappingTable[0] = 'HTML_SPECIALCHARS';
            constMappingTable[1] = 'HTML_ENTITIES';
            constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
            constMappingQuoteStyle[2] = 'ENT_COMPAT';
            constMappingQuoteStyle[3] = 'ENT_QUOTES';
            useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
            useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() : 'ENT_COMPAT';
            if (useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES') {
                throw new Error("Table: " + useTable + ' not supported');
            }
            entities['38'] = '&amp;';
            if (useTable === 'HTML_ENTITIES') {
                entities['94'] = '&circ;';
                entities['126'] = '&tilde;';
                entities['130'] = '&sbquo;';
                entities['131'] = '&fnof;';
                entities['132'] = '&bdquo;';
                entities['133'] = '&hellip;';
                entities['134'] = '&dagger;';
                entities['135'] = '&Dagger;';
                entities['136'] = '&circ;';
                entities['137'] = '&permil;';
                entities['138'] = '&Scaron;';
                entities['139'] = '&lsaquo;';
                entities['140'] = '&OElig;';
                entities['145'] = '&lsquo;';
                entities['146'] = '&rsquo;';
                entities['147'] = '&ldquo;';
                entities['148'] = '&rdquo;';
                entities['149'] = '&bull;';
                entities['150'] = '&ndash;';
                entities['151'] = '&mdash;';
                entities['152'] = '&tilde;';
                entities['153'] = '&trade;';
                entities['154'] = '&scaron;';
                entities['155'] = '&rsaquo;';
                entities['156'] = '&oelig;';
                entities['159'] = '&Yuml;';
                entities['160'] = '&nbsp;';
                entities['161'] = '&iexcl;';
                entities['162'] = '&cent;';
                entities['163'] = '&pound;';
                entities['164'] = '&curren;';
                entities['165'] = '&yen;';
                entities['166'] = '&brvbar;';
                entities['167'] = '&sect;';
                entities['168'] = '&uml;';
                entities['169'] = '&copy;';
                entities['170'] = '&ordf;';
                entities['171'] = '&laquo;';
                entities['172'] = '&not;';
                entities['173'] = '&shy;';
                entities['174'] = '&reg;';
                entities['175'] = '&macr;';
                entities['176'] = '&deg;';
                entities['177'] = '&plusmn;';
                entities['178'] = '&sup2;';
                entities['179'] = '&sup3;';
                entities['180'] = '&acute;';
                entities['181'] = '&micro;';
                entities['182'] = '&para;';
                entities['183'] = '&middot;';
                entities['184'] = '&cedil;';
                entities['185'] = '&sup1;';
                entities['186'] = '&ordm;';
                entities['187'] = '&raquo;';
                entities['188'] = '&frac14;';
                entities['189'] = '&frac12;';
                entities['190'] = '&frac34;';
                entities['191'] = '&iquest;';
                entities['192'] = '&Agrave;';
                entities['193'] = '&Aacute;';
                entities['194'] = '&Acirc;';
                entities['195'] = '&Atilde;';
                entities['196'] = '&Auml;';
                entities['197'] = '&Aring;';
                entities['198'] = '&AElig;';
                entities['199'] = '&Ccedil;';
                entities['200'] = '&Egrave;';
                entities['201'] = '&Eacute;';
                entities['202'] = '&Ecirc;';
                entities['203'] = '&Euml;';
                entities['204'] = '&Igrave;';
                entities['205'] = '&Iacute;';
                entities['206'] = '&Icirc;';
                entities['207'] = '&Iuml;';
                entities['208'] = '&ETH;';
                entities['209'] = '&Ntilde;';
                entities['210'] = '&Ograve;';
                entities['211'] = '&Oacute;';
                entities['212'] = '&Ocirc;';
                entities['213'] = '&Otilde;';
                entities['214'] = '&Ouml;';
                entities['215'] = '&times;';
                entities['216'] = '&Oslash;';
                entities['217'] = '&Ugrave;';
                entities['218'] = '&Uacute;';
                entities['219'] = '&Ucirc;';
                entities['220'] = '&Uuml;';
                entities['221'] = '&Yacute;';
                entities['222'] = '&THORN;';
                entities['223'] = '&szlig;';
                entities['224'] = '&agrave;';
                entities['225'] = '&aacute;';
                entities['226'] = '&acirc;';
                entities['227'] = '&atilde;';
                entities['228'] = '&auml;';
                entities['229'] = '&aring;';
                entities['230'] = '&aelig;';
                entities['231'] = '&ccedil;';
                entities['232'] = '&egrave;';
                entities['233'] = '&eacute;';
                entities['234'] = '&ecirc;';
                entities['235'] = '&euml;';
                entities['236'] = '&igrave;';
                entities['237'] = '&iacute;';
                entities['238'] = '&icirc;';
                entities['239'] = '&iuml;';
                entities['240'] = '&eth;';
                entities['241'] = '&ntilde;';
                entities['242'] = '&ograve;';
                entities['243'] = '&oacute;';
                entities['244'] = '&ocirc;';
                entities['245'] = '&otilde;';
                entities['246'] = '&ouml;';
                entities['247'] = '&divide;';
                entities['248'] = '&oslash;';
                entities['249'] = '&ugrave;';
                entities['250'] = '&uacute;';
                entities['251'] = '&ucirc;';
                entities['252'] = '&uuml;';
                entities['253'] = '&yacute;';
                entities['254'] = '&thorn;';
                entities['255'] = '&yuml;';
                entities['264'] = '&#264;';
                entities['265'] = '&#265;';
                entities['338'] = '&OElig;';
                entities['339'] = '&oelig;';
                entities['352'] = '&Scaron;';
                entities['353'] = '&scaron;';
                entities['372'] = '&#372;';
                entities['373'] = '&#373;';
                entities['374'] = '&#374;';
                entities['375'] = '&#375;';
                entities['376'] = '&Yuml;';
                entities['402'] = '&fnof;';
                entities['710'] = '&circ;';
                entities['732'] = '&tilde;';
                entities['913'] = '&Alpha;';
                entities['914'] = '&Beta;';
                entities['915'] = '&Gamma;';
                entities['916'] = '&Delta;';
                entities['917'] = '&Epsilon;';
                entities['918'] = '&Zeta;';
                entities['919'] = '&Eta;';
                entities['920'] = '&Theta;';
                entities['921'] = '&Iota;';
                entities['922'] = '&Kappa;';
                entities['923'] = '&Lambda;';
                entities['924'] = '&Mu;';
                entities['925'] = '&Nu;';
                entities['926'] = '&Xi;';
                entities['927'] = '&Omicron;';
                entities['928'] = '&Pi;';
                entities['929'] = '&Rho;';
                entities['931'] = '&Sigma;';
                entities['932'] = '&Tau;';
                entities['933'] = '&Upsilon;';
                entities['934'] = '&Phi;';
                entities['935'] = '&Chi;';
                entities['936'] = '&Psi;';
                entities['937'] = '&Omega;';
                entities['945'] = '&alpha;';
                entities['946'] = '&beta;';
                entities['947'] = '&gamma;';
                entities['948'] = '&delta;';
                entities['949'] = '&epsilon;';
                entities['950'] = '&zeta;';
                entities['951'] = '&eta;';
                entities['952'] = '&theta;';
                entities['953'] = '&iota;';
                entities['954'] = '&kappa;';
                entities['955'] = '&lambda;';
                entities['956'] = '&mu;';
                entities['957'] = '&nu;';
                entities['958'] = '&xi;';
                entities['959'] = '&omicron;';
                entities['960'] = '&pi;';
                entities['961'] = '&rho;';
                entities['962'] = '&sigmaf;';
                entities['963'] = '&sigma;';
                entities['964'] = '&tau;';
                entities['965'] = '&upsilon;';
                entities['966'] = '&phi;';
                entities['967'] = '&chi;';
                entities['968'] = '&psi;';
                entities['969'] = '&omega;';
                entities['977'] = '&thetasym;';
                entities['978'] = '&upsih;';
                entities['982'] = '&piv;';
                entities['8194'] = '&ensp;';
                entities['8195'] = '&emsp;';
                entities['8201'] = '&thinsp;';
                entities['8204'] = '&zwnj;';
                entities['8205'] = '&zwj;';
                entities['8206'] = '&lrm;';
                entities['8207'] = '&rlm;';
                entities['8211'] = '&ndash;';
                entities['8212'] = '&mdash;';
                entities['8216'] = '&lsquo;';
                entities['8217'] = '&rsquo;';
                entities['8218'] = '&sbquo;';
                entities['8220'] = '&ldquo;';
                entities['8221'] = '&rdquo;';
                entities['8222'] = '&bdquo;';
                entities['8224'] = '&dagger;';
                entities['8225'] = '&Dagger;';
                entities['8226'] = '&bull;';
                entities['8230'] = '&hellip;';
                entities['8240'] = '&permil;';
                entities['8242'] = '&prime;';
                entities['8243'] = '&Prime;';
                entities['8249'] = '&lsaquo;';
                entities['8250'] = '&rsaquo;';
                entities['8254'] = '&oline;';
                entities['8260'] = '&frasl;';
                entities['8364'] = '&euro;';
                entities['8472'] = '&weierp;';
                entities['8465'] = '&image;';
                entities['8476'] = '&real;';
                entities['8482'] = '&trade;';
                entities['8501'] = '&alefsym;';
                entities['8592'] = '&larr;';
                entities['8593'] = '&uarr;';
                entities['8594'] = '&rarr;';
                entities['8595'] = '&darr;';
                entities['8596'] = '&harr;';
                entities['8629'] = '&crarr;';
                entities['8656'] = '&lArr;';
                entities['8657'] = '&uArr;';
                entities['8658'] = '&rArr;';
                entities['8659'] = '&dArr;';
                entities['8660'] = '&hArr;';
                entities['8704'] = '&forall;';
                entities['8706'] = '&part;';
                entities['8707'] = '&exist;';
                entities['8709'] = '&empty;';
                entities['8711'] = '&nabla;';
                entities['8712'] = '&isin;';
                entities['8713'] = '&notin;';
                entities['8715'] = '&ni;';
                entities['8719'] = '&prod;';
                entities['8721'] = '&sum;';
                entities['8722'] = '&minus;';
                entities['8727'] = '&lowast;';
                entities['8729'] = '&#8729;';
                entities['8730'] = '&radic;';
                entities['8733'] = '&prop;';
                entities['8734'] = '&infin;';
                entities['8736'] = '&ang;';
                entities['8743'] = '&and;';
                entities['8744'] = '&or;';
                entities['8745'] = '&cap;';
                entities['8746'] = '&cup;';
                entities['8747'] = '&int;';
                entities['8756'] = '&there4;';
                entities['8764'] = '&sim;';
                entities['8773'] = '&cong;';
                entities['8776'] = '&asymp;';
                entities['8800'] = '&ne;';
                entities['8801'] = '&equiv;';
                entities['8804'] = '&le;';
                entities['8805'] = '&ge;';
                entities['8834'] = '&sub;';
                entities['8835'] = '&sup;';
                entities['8836'] = '&nsub;';
                entities['8838'] = '&sube;';
                entities['8839'] = '&supe;';
                entities['8853'] = '&oplus;';
                entities['8855'] = '&otimes;';
                entities['8869'] = '&perp;';
                entities['8901'] = '&sdot;';
                entities['8968'] = '&lceil;';
                entities['8969'] = '&rceil;';
                entities['8970'] = '&lfloor;';
                entities['8971'] = '&rfloor;';
                entities['9001'] = '&lang;';
                entities['9002'] = '&rang;';
                entities['9642'] = '&#9642;';
                entities['9643'] = '&#9643;';
                entities['9674'] = '&loz;';
                entities['9702'] = '&#9702;';
                entities['9824'] = '&spades;';
                entities['9827'] = '&clubs;';
                entities['9829'] = '&hearts;';
                entities['9830'] = '&diams;';
            }
            if (useQuoteStyle !== 'ENT_NOQUOTES') {
                entities['34'] = '&quot;';
            }
            if (useQuoteStyle === 'ENT_QUOTES') {
                entities['39'] = '&#39;';
            }
            entities['60'] = '&lt;';
            entities['62'] = '&gt;';
            for (decimal in entities) {
                if (entities.hasOwnProperty(decimal)) {
                    hash_map[String.fromCharCode(decimal)] = entities[decimal];
                }
            }
            table = quote_style = entities = decimal = constMappingTable = constMappingQuoteStyle = useTable = useQuoteStyle = null;
            return hash_map;
        }, htmlentities: function(string, quote_style, charset, double_encode) {
            var hash_map = fxm.helper.get_html_translation_table('HTML_ENTITIES', quote_style), symbol = '';
            string = string == null ? '' : string + '';
            if (!hash_map) {
                string = quote_style = charset = double_encode = hash_map = null;
                return false;
            }
            if (quote_style && quote_style === 'ENT_QUOTES') {
                hash_map["'"] = '&#039;';
            }
            if (!!double_encode || double_encode == null) {
                for (symbol in hash_map) {
                    if (hash_map.hasOwnProperty(symbol)) {
                        string = string.split(symbol).join(hash_map[symbol]);
                    }
                }
            } else {
                string = string.replace(/([\s\S]*?)(&(?:#\d+|#x[\da-f]+|[a-zA-Z][\da-z]*);|$)/g, function(ignore, text, entity) {
                    for (symbol in hash_map) {
                        if (hash_map.hasOwnProperty(symbol)) {
                            text = text.split(symbol).join(hash_map[symbol]);
                        }
                    }
                    ignore = null;
                    return text + entity;
                });
            }
            quote_style = charset = double_encode = hash_map = symbol = null;
            return string;
        }, html_entity_decode: function(string, quote_style) {
            var hash_map = {}, symbol = '', tmp_str = '', entity = '';
            tmp_str = string.toString();
            if (false === (hash_map = fxm.helper.get_html_translation_table('HTML_ENTITIES', quote_style))) {
                string = quote_style = hash_map = symbol = tmp_str = entity = null;
                return false;
            }
            delete(hash_map['&']);
            hash_map['&'] = '&amp;';
            for (symbol in hash_map) {
                entity = hash_map[symbol];
                tmp_str = tmp_str.split(entity).join(symbol);
            }
            tmp_str = tmp_str.split('&#039;').join("'");
            string = quote_style = hash_map = symbol = tmp_str = entity = null;
            return tmp_str;
        }, charToUnicodeHTML: function(singleChar) {
            if (singleChar == '')
                return'';
            else
                return'&#' + singleChar.charCodeAt(0) + ';';
        }, strToUnicodeHTML: function(str) {
            var unicodeString = '';
            for (var i = 0; i < str.length; i++) {
                unicodeString += '&#' + str.charCodeAt(i) + ';';
            }
            str = i = null;
            return unicodeString;
        }, cut_str: function(str, interval) {
            if (str.length > interval)
                return str.substring(0, (interval - 4)) + ' ...';
            return str;
        }, ampify: function(str) {
            str = str.replace(/&amp;/g, 'AMP');
            str = str.replace(/&/g, 'AMP');
            str = str.replace(/AMP/g, '&amp;');
            return str;
        }, visible: function(htmlElem) {
            if (typeof (htmlElem) == 'string')
                var htmlElem = fxm(htmlElem).get();
            if (htmlElem == null || typeof htmlElem == 'undefined')
                return;
            htmlElem.style.display = 'block';
            htmlElem = null;
        }, invisible: function(htmlElem) {
            if (typeof (htmlElem) == 'string')
                var htmlElem = fxm(htmlElem).get();
            if (htmlElem == null || typeof htmlElem == 'undefined')
                return;
            htmlElem.style.display = 'none';
            htmlElem = null;
        }, currStyle: function(htmlElem, cssprop) {
            if (typeof (htmlElem) == 'string')
                var htmlElem = fxm(htmlElem).get();
            if (htmlElem.currentStyle)
                return htmlElem.currentStyle[cssprop];
            else if (document.defaultView && document.defaultView.getComputedStyle)
                return document.defaultView.getComputedStyle(htmlElem, "")[cssprop];
            else
                return htmlElem.style[cssprop];
        }, minutesFromMilliseconds: function(milliseconds) {
            try {
                var d = new Date((milliseconds * 1));
                var min = d.getMinutes();
                var sec = d.getSeconds();
                sec = sec + "";
                if (sec.length === 1) {
                    sec = "0" + sec;
                }
                if (isNaN(min) || isNaN(sec)) {
                    return '00:00';
                }
                d = null;
                return min + ":" + sec;
            } catch (e) {
                d = null;
                return'00:00';
            }
        }, dateToMMSS: function(sec) {
            var sec_num = parseInt(sec, 10);
            //console.log("SECONDS -> " + sec_num);
            var minutes = Math.floor(sec_num / 60);
            console.log("MINUTES -> " + minutes);
            var seconds = sec_num - (minutes * 60);
            console.log("SECONDS -> " + seconds);

            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            var time = minutes + ':' + seconds;
            if (isNaN(minutes) || isNaN(seconds))
                time = "00:00";
            return time;
        }, dateToHHMMSS: function(s) {
            var sec_num = parseInt(s, 10);
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            var time = hours + ':' + minutes + ':' + seconds;
            if (isNaN(hours) || isNaN(minutes) || isNaN(seconds))
                time = "00:00:00";
            return time;
        }, minutesFromSeconds: function(seconds) {
            try {
                var d = new Date((seconds * 1000));
                var min = d.getMinutes();
                var sec = d.getSeconds();
                sec = sec + "";
                if (sec.length == 1) {
                    sec = "0" + sec;
                }
                d = null;
                return min + ":" + sec;
            } catch (e) {
                d = null;
                return'00:00';
            }
        }, dateFromTimestamp: function(UnixTimestamp) {
            var d = new Date(UnixTimestamp * 1000);
            return d.getDate() + '.' + (d.getMonth() + 1) + '.' + d.getFullYear();
        }, wrapCdata: function(string) {
            return'<' + '!' + '[' + 'CDATA' + '[' + string + ']' + ']' + '>';
        }, getUrlVars: function() {
            if (fxm.helper.urlVars == null) {
                fxm.helper.urlVars = [];
                var hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    fxm.helper.urlVars.push(hash[0]);
                    fxm.helper.urlVars[hash[0]] = hash[1];
                }
            }
            hash = hashes = i = null;
            return fxm.helper.urlVars;
        }, getUrlVar: function(name) {
            return fxm.helper.getUrlVars()[name];
        }, cleanQuotes: function(str) {
            return fxm.helper.str_replace("'", "\'", fxm.helper.str_replace('"', '\"', fxm.helper.str_replace("\n", '\n', str)));
        }, calculatedWidth: function(htmlElem) {
            if (typeof (htmlElem) == 'string')
                var htmlElem = fxm(htmlElem).get();
            else if (htmlElem.InstanceOffxm)
                htmlElem = htmlElem.elems[0];
            if (htmlElem.clientWidth) {
                return htmlElem.clientWidth;
            } else if (htmlElem.offsetWidth) {
                return htmlElem.offsetWidth;
            } else {
                return fxm.helper.currStyle(htmlElem, 'width');
            }
        }, calculatedHeight: function(htmlElem) {
            if (typeof (htmlElem) == 'string')
                var htmlElem = fxm(htmlElem).get();
            else if (htmlElem.InstanceOffxm)
                htmlElem = htmlElem.elems[0];
            if (htmlElem.clientHeight) {
                return htmlElem.clientHeight;
            } else if (htmlElem.offsetHeight) {
                return htmlElem.offsetHeight;
            } else {
                return fxm.helper.currStyle(htmlElem, 'height');
            }
        }, calculatedPosition: function(htmlElem, iterateParents) {
            if (typeof (htmlElem) == 'string')
                var htmlElem = fxm(htmlElem).get();
            else if (htmlElem.InstanceOffxm)
                htmlElem = htmlElem.elems[0];
            var curleft = 0;
            var curtop = 0;
            do {
                curleft += htmlElem.offsetLeft;
                curtop += htmlElem.offsetTop;
            } while (htmlElem = htmlElem.offsetParent && iterateParents);
            return{top: curtop, left: curleft};
        }, parseUri: function(str, strictMode) {
            var o = {strictMode: strictMode, key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"], q: {name: "queryKey", parser: /(?:^|&)([^&=]*)=?([^&]*)/g}, parser: {strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/, loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/}};
            var m = o.parser[o.strictMode ? "strict" : "loose"].exec(str);
            var uri = {};
            var i = 14;
            while (i--)
                uri[o.key[i]] = m[i] || "";
            uri[o.q.name] = {};
            uri[o.key[12]].replace(o.q.parser, function($0, $1, $2) {
                if ($1)
                    uri[o.q.name][$1] = $2;
            });
            str = strictMode = o = m = i = null;
            return uri;
        }, init: function() {
            fxm.helper.proto = new fxm.object;
            this.hex2Str(this.hexMap());
            this.filter = ['base64KeyStr', 'urlVars', 'addClass', 'removeClass', 'clearClass', 'rand', 'str_replace', 'nl2br', 'dob64', 'undob64', 'utf8Encode', 'utf8Decode', 'str2Hex', 'hexMap', 'hex2Str', 'get_html_translation_table', 'htmlentities', 'html_entity_decode', 'charToUnicodeHTML', 'strToUnicodeHTML', 'cut_str', 'ampify', 'visible', 'invisible', 'currStyle', 'minutesFromMilliseconds', 'minutesFromSeconds', 'dateFromTimestamp', 'wrapCdata', 'getUrlVars', 'getUrlVar', 'cleanQuotes', 'calculatedWidth', 'calculatedHeight', 'calculatedPosition', 'parseUri'];
        }}
    fxm.helper.init();
})();