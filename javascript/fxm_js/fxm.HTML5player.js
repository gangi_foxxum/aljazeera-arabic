(function() {
    fxm.HTML5player = {
        videoSrc: null, sourceObject: null, mime: 'video/mp4', fullScreen: true, errorPlayer: false, videoLoaded: false, windowWidth: 640, windowHeight: 360, fullScreenWidth: 1280, fullScreenHeight: 720, fullScreenClass: 'fullscreen', videoObjectWrapper: 'mainVideoObject', progressBarId: 'progress-bar', videoObjectId: 'video', videoObjectNavigation: null, zindex: 0, startPosition: 0, seekTime: 1, STOPPED: 0, PLAYING: 1, PAUSED: 2, BUFFERING: 4, ENDED: 5, ERROR: 6, currentState: null, loadCatchCounter: 0, raiseOnStop: true,
        init: function() {
            fxm.HTML5player.videoDiv = fxm(fxm.HTML5player.videoObjectWrapper).get();
            if (!fxm.HTML5player.videoDiv) {
                fxm.HTML5player.videoDiv = document.createElement("div");
                fxm.HTML5player.videoDiv.setAttribute("id", fxm.HTML5player.videoObjectWrapper);
                fxm.HTML5player.videoDiv.setAttribute("z-index", fxm.HTML5player.zindex);
                document.getElementsByTagName('body')[0].appendChild(fxm.HTML5player.videoDiv);
            }
            if (fxm.HTML5player.videoObject) {
                raiseOnStop = false;
                fxm.HTML5player.videoObject.playPosition = 0;
                fxm.HTML5player.videoObject.playTime = 0;
                fxm.HTML5player.videoObject.parentNode.removeChild(fxm.HTML5player.videoObject);
            }
            fxm.HTML5player.videoDiv.innerHTML = "";
            fxm.HTML5player.videoObject = document.createElement("video");
            fxm.HTML5player.videoObject.setAttribute("id", fxm.HTML5player.videoObjectId);
            fxm.HTML5player.videoObject.setAttribute("z-index", fxm.HTML5player.zindex);
            if (fxm.HTML5player.fullScreen) {
                // Workaround: setting width and height 2 pixels smaller than desired
                fxm.HTML5player.videoObject.setAttribute("width", fxm.HTML5player.fullScreenWidth - 2);
                fxm.HTML5player.videoObject.setAttribute("height", fxm.HTML5player.fullScreenHeight - 2);
                fxm.helper.addClass(fxm.HTML5player.videoDiv, fxm.HTML5player.fullScreenClass);
            } else {
                fxm.HTML5player.videoObject.setAttribute("width", fxm.HTML5player.windowWidth);
                fxm.HTML5player.videoObject.setAttribute("height", fxm.HTML5player.windowHeight);
                fxm.helper.removeClass(fxm.HTML5player.videoDiv, fxm.HTML5player.fullScreenClass);
            }
            fxm.HTML5player.videoDiv.appendChild(fxm.HTML5player.videoObject);
            fxm.HTML5player.videoObject.innerHTML = "";
            fxm.HTML5player.sourceObject = document.createElement("source");
            fxm.HTML5player.sourceObject.setAttribute("src", this.videoSrc);
            fxm.HTML5player.sourceObject.setAttribute("type", this.mime);
            fxm.HTML5player.videoObject.appendChild(fxm.HTML5player.sourceObject);
            fxm.HTML5player.currentState = fxm.HTML5player.STOPPED;
            fxm.HTML5player.videoObject.addEventListener("ended", function() {
                fxm.dbg.log("HTML5player -------> ENDED, STARTING TO PLAY VIDEOS AGAIN!");
                //stopProgress();
                //resetProgress();
                //playNextVideo();
            });
            fxm.HTML5player.videoObject.addEventListener("playing", function() {
                fxm.dbg.log("HTML5player -------> START PLAYING...!!!!!");
                // Workaround: setting width and height back to the desired size
                fxm.HTML5player.videoObject.setAttribute("width", fxm.HTML5player.fullScreenWidth);
                fxm.HTML5player.videoObject.setAttribute("height", fxm.HTML5player.fullScreenHeight);
                fxm.HTML5player.currentState = fxm.HTML5player.PLAYING;
                //updateProgress();
            });
            fxm.HTML5player.videoObject.addEventListener("loadstart", function() {
                //fxm.dbg.log("HTML5player -------> START LOOKING FOR MEDIA DATA, STATUS VARIABLE : " + fxm.HTML5player.videoObject.duration);
                //fxm.HTML5player.initLoadTimer();
            });
            fxm.HTML5player.videoObject.addEventListener("loadeddata", function() {
                //fxm.dbg.log("HTML5player -------> VIDEO CAN START PLAYING!!! videoLoaded = " + fxm.HTML5player.videoLoaded);
                fxm.HTML5player.videoLoaded = true;
                fxm.HTML5player.errorPlayer = false;
            });
            fxm.HTML5player.videoObject.addEventListener("loadedmetadata", function() {
                fxm.dbg.log("HTML5player ---------> METADATA LOADED. DURATION = " + fxm.HTML5player.videoObject.duration);
                fxm.HTML5player.videoLoaded = true;
                //fxm.HTML5player.initLoadTimer();
            });
            fxm.HTML5player.videoObject.addEventListener("timeupdate", function() {
                //fxm.dbg.log("HTML5player -------> TIME UPDATED");
                //fxm.unblockKeys();
                //fxm('pp').removeClass('pause').addClass('play');
                //fxm('loadingIndicator').hide(); 
            });
            fxm.HTML5player.videoObject.addEventListener("pause", function() {
                fxm.dbg.log("HTML5player -------> VIDEO ON PAUSE");
                //fxm.HTML5player.pause();
            });
            fxm.HTML5player.videoObject.addEventListener("seeking", function() {
                fxm.dbg.log("HTML5player ------> SEEKING");
                //stopProgress();
            });
            fxm.HTML5player.videoObject.addEventListener("seeked", function() {
                fxm.dbg.log("HTML5player -------> FINISHED SEEKING, STATUS PLAYER : " + fxm.HTML5player.currentState);
                if (fxm.HTML5player.currentState !== fxm.HTML5player.ERROR) {
                    fxm.HTML5player.play();
                    //updateProgress();
                }
            });
            fxm.HTML5player.videoObject.addEventListener("waiting", function() {
                fxm.dbg.log("HTML5player -------> BUFFERING...");
                fxm.HTML5player.videoLoaded = false;
                fxm.HTML5player.currentState = fxm.HTML5player.BUFFERING;
                //fxm.HTML5player.initLoadingTimer();
                //fxm.dbg.log("HTML5player -------> DURATION VIDEO : " + fxm.HTML5player.videoObject.duration);
            });
            fxm.HTML5player.videoObject.addEventListener("error", function() {
                fxm.dbg.log("HTML5player -------> THERE WAS AN ERROR PLAYING THE VIDEO...");
                fxm.HTML5player.error();
                //playNextVideo();
            });
            //fxm.HTML5player.checkStatusPlayer();
        }, time: 20000, initLoadingTimer: function() {
            fxm.HTML5player.videoLoaded = false;
            resetProgress();
            fxm('AjaxLoad').show();
            setTimeout(function() {
                fxm.dbg.log("STATUS VARIABLE AFTER 18 SECONDS : " + fxm.HTML5player.videoLoaded);
                if (!fxm.HTML5player.videoLoaded) {
                    fxm.dbg.log("VIDEO IS NOT LOADED YET, CHECKING STATUS VARIABLES");
                    var bufferedTime = fxm.HTML5player.videoObject.buffered.end(0);
                    fxm.dbg.log("BUFFERED TIME : " + bufferedTime + ", DURATION VIDEO : " + fxm.HTML5player.videoObject.duration);
                    if (isNaN(fxm.HTML5player.videoObject.duration) || bufferedTime === 0) {
                        fxm.dbg.log("GIVING OTHER 20 EXTRA SECONDS TO LOAD");
                        setTimeout(function() {
                            bufferedTime = fxm.HTML5player.videoObject.buffered.end(0);
                            fxm.dbg.log("CHECKING STATUS PLAYER AFTER 20 EXTRA SECONDS");
                            if (isNaN(fxm.HTML5player.duration()) && bufferedTime === 0) {
                                fxm.dbg.log("START PLAYING NEXT VIDEO, 20 SECONDS LOADING EXCEEDED, NOT PLAYING YET... STATUS PLAYER : " + fxm.HTML5player.currentState);
                                //fxm.HTML5player.stop();
                                playNextVideo();
                            } else {
                                fxm.dbg.log("VIDEO SEEMS TO BE LOADING, GIVING THE LAST 10 EXTRA SECONDS...");
                                setTimeout(function() {
                                    if (!fxm.HTML5player.videoLoaded) {
                                        fxm.dbg.log("THE VIDEO CANNOT PLAY, GOING TO THE NEXT ONE IN THE LIST");
                                        playNextVideo();
                                    }
                                }, 10000);
                            }
                        }, fxm.HTML5player.time);
                    }
                    //fxm.dbg.log("START PLAYING NEXT VIDEO, 10 SECONDS LOADING EXCEEDED... STATUS PLAYER : " + fxm.HTML5player.currentState);
                    //fxm.HTML5player.stop();
                    //playNextVideo();
                }
            }, fxm.HTML5player.time);
        }, load: function(videoSrc) {
            fxm.HTML5player.videoSrc = videoSrc;
            fxm.HTML5player.init();
            this.notifyMessage('VIDEO URL : ' + fxm.HTML5player.videoSrc);
            //fxm.HTML5player.play();
        }, notifyMessage: function(message) {
            fxm.dbg.log(message);
            /*var logMessage = document.createElement("span");
             logMessage.setAttribute("class", "log");
             logMessage.innerHTML = message;
             var lastMessage = document.getElementById("debugHTML5Video").getElementsByTagName("span")[0];
             document.getElementById("debugHTML5Video").insertBefore(logMessage, lastMessage);*/
            //document.getElementById("debugHTML5Video").scrollTop = document.getElementById("debugHTML5Video").scrollHeight;
        }, play: function(position) {
            try {
                fxm.HTML5player.currentState = fxm.HTML5player.PLAYING;
                //fxm.HTML5player.notifyMessage('Duration video : ' + fxm.HTML5player.videoObject.duration);
                if (position) {
                    fxm.HTML5player.startPosition = position;
                    setTimeout("fxm.HTML5player.seek(" + fxm.HTML5player.startPosition + ");", 500);
                } else {
                    fxm.HTML5player.startPosition = 0;
                }
                fxm.HTML5player.videoObject.play();
            } catch (e) {
                fxm.HTML5player.notifyMessage("HTML5player ------> THERE WAS AN ERROR WHILE TRYING TO PLAY THE VIDEO : " + e);
                fxm.HTML5player.loadCatchCounter++;
                if (this.loadCatchCounter < 3) {
                    setTimeout(function() {
                        fxm.HTML5player.play(fxm.HTML5player.startPosition);
                    }, 250);
                    return;
                } else {
                    fxm.HTML5player.stopPlaycheckTimer();
                    fxm.HTML5player.error();
                }
            }
        }, pause: function() {
            if (fxm.HTML5player.videoObject && fxm.HTML5player.currentState === fxm.HTML5player.PLAYING) {
                fxm.HTML5player.currentState = fxm.HTML5player.PAUSED;
                fxm.HTML5player.videoObject.pause();
            }
        }, stop: function() {
            if (fxm.HTML5player.videoObject && fxm.HTML5player.currentState !== fxm.HTML5player.STOPPED) {
                fxm.HTML5player.notifyMessage('HTML5player ------> Video stopped...!!!!!!!!!!!!');
                fxm.HTML5player.currentState = fxm.HTML5player.STOPPED;
                fxm.HTML5player.videoObject.pause();
            }
        }, backward: function() {
            if (!fxm.HTML5player.videoObject)
                return;
            fxm.HTML5player.notifyMessage('Backward...');
            var seekToTime = fxm.HTML5player.videoObject.currentTime - (fxm.HTML5player.seekTime * 10);
            if (seekToTime < 0)
                seekToTime = 0;
            fxm.HTML5player.seek(seekToTime);
        }, forward: function() {
            if (!fxm.HTML5player.videoObject)
                return;
            var seekToTime = fxm.HTML5player.videoObject.currentTime + (fxm.HTML5player.seekTime * 10);
            fxm.HTML5player.notifyMessage('CURRENT TIME : ' + fxm.HTML5player.videoObject.currentTime + " + seektime : " + (fxm.HTML5player.seekTime * 10) + " ---> " + (fxm.HTML5player.currentTime() + (fxm.HTML5player.seekTime * 10)));
            if (seekToTime > fxm.HTML5player.duration())
                return;
            fxm.HTML5player.notifyMessage('Forward...');
            fxm.HTML5player.seek(seekToTime);
        }, seek: function(seekToTime) {
            if (!fxm.HTML5player.videoObject)
                return;
            fxm.HTML5player.currentState = fxm.HTML5player.BUFFERING;
            //fxm.HTML5player.videoObject.pause();
            fxm.HTML5player.notifyMessage('Seek to...' + seekToTime);
            fxm.HTML5player.videoObject.currentTime = seekToTime;
            //fxm.HTML5player.videoObject.onseeked();
        }, error: function() {
            fxm.HTML5player.errorPlayer = true;
            fxm.HTML5player.stop();
            fxm.HTML5player.currentState = fxm.HTML5player.ERROR;
        }, duration: function() {
            return fxm.HTML5player.videoObject.duration;
        }, currentTime: function() {
            return fxm.HTML5player.videoObject.currentTime;
        }, playcheckTimeout: 10, playcheckTimer: null, startPlaycheckTimer: function() {
            fxm.HTML5player.stopPlaycheckTimer();
            fxm.HTML5player.playcheckTimer = setTimeout("fxm.HTML5player.error();", fxm.HTML5player.playcheckTimeout * 1000);
        }, stopPlaycheckTimer: function() {
            if (fxm.HTML5player.playcheckTimer)
                clearTimeout(fxm.HTML5player.playcheckTimer);
        }, checkTimeInit: false, checkStatusPlayer: function() {
            if (!fxm.HTML5player.checkTimeInit) {
                fxm.HTML5player.checkTimeInit = true;
                setInterval(function() {
                    //fxm.dbg.log("STATUS PLAYER : " + fxm.HTML5player.currentState);
                    try {
                        if (fxm.HTML5player.currentState === fxm.HTML5player.ERROR || fxm.HTML5player.errorPlayer) {
                            fxm.dbg.log("HIDE AJAX LOAD AND SHOW NETWORK ERROR DIV");
                            fxm('AjaxLoad').hide();
                            fxm.blockKeys();
                            document.getElementById('networkError').style.display = "block";
                        } else {
                            //fxm.dbg.log("HIDE NETWORK ERROR DIV");
                            //fxm('AjaxLoad').hide();
                            fxm.unblockKeys(true);
                            document.getElementById('networkError').style.display = "none";
                        }
                    } catch (e) {
                        fxm.dbg.log("ERROR IN CHECKSTATUSPLAYER ----> " + e);
                    }
                }, 1000);
            }
        }, updateProgressBar: function() {
            return;
            var minutes = ((Math.floor(fxm.HTML5player.currentTime() / 60)) < 10) ? "0" + (Math.floor(fxm.HTML5player.currentTime() / 60)) : (Math.floor(fxm.HTML5player.currentTime() / 60));
            var seconds = ((Math.floor(fxm.HTML5player.currentTime() - minutes * 60)) < 10) ? "0" + (Math.floor(fxm.HTML5player.currentTime() - minutes * 60)) : Math.floor(fxm.HTML5player.currentTime() - minutes * 60);
            //fxm.HTML5player.notifyMessage("PLAYER CURRENT TIME : " + minutes + ":" + seconds);
            var progressBar = document.getElementById(fxm.HTML5player.progressBarId);
            var indicator = document.getElementById("time-indicator");
            var percentage = Math.floor((100 / fxm.HTML5player.videoObject.duration) * fxm.HTML5player.videoObject.currentTime);
            progressBar.value = percentage;
            progressBar.innerHTML = percentage + "% played";
            indicator.innerHTML = minutes + ":" + seconds;

        }
    };
})();

