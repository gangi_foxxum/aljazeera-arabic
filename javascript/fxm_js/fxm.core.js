(function() {
    var fxm = (function() {
        var fxmTmp = function(selector, getBy, parent, type) {
            return new fxmTmp.fn.init(selector, getBy, parent, type);
        };
        fxmTmp.fn = fxmTmp.prototype = {
            constructor: fxmTmp,
            InstanceOffxm: true,
            elems: [],
            byId: function(selector) {
                var tmpElems = [];
                if (typeof(selector) === 'string') selector = new Array(selector);
                for (var i = 0; i < selector.length; i++) {
                    if (typeof selector[i] === 'string') {
                        tmpElems.push(document.getElementById(selector[i]));
                    }
                }
                this.elems = tmpElems;
                tmpElems = null;
                i = null;
                selector = null;
                return this;
            },
            byName: function(selector, parent) {
                var tmpElems = [];
                if (typeof(selector) === 'string') selector = new Array(selector);
                if (parent && parent.InstanceOffxm) parent = parent.elems[0];
                else if (typeof(parent) === 'string') parent = document.getElementById(parent);
                for (var i = 0; i < selector.length; i++) {
                    if (typeof selector[i] === 'string') {
                        var e = (parent || document).getElementsByName(selector[i]);
                        for (var j = 0; j < e.length; j++) {
                            tmpElems.push(e[j]);
                        }
                    }
                }
                this.elems = tmpElems;
                tmpElems = null;
                i = null;
                e = null;
                selector = null;
                parent = null;
                return this;
            },
            byTag: function(selector, parent) {
                var tmpElems = [];
                if (typeof(selector) === 'string') selector = new Array(selector);
                if (parent && parent.InstanceOffxm) parent = parent.elems[0];
                else if (typeof(parent) === 'string') parent = document.getElementById(parent);
                for (var i = 0; i < selector.length; i++) {
                    if (typeof selector[i] === 'string') {
                        var e = (parent || document).getElementsByTagName(selector[i]);
                        for (var j = 0; j < e.length; j++) {
                            tmpElems.push(e[j]);
                        }
                    }
                }
                this.elems = tmpElems;
                tmpElems = null;
                i = null;
                e = null;
                selector = null;
                parent = null;
                return this;
            },
            byClass: function(selector, parent, type) {
                var tmpElems = [];
                if (parent && parent.InstanceOffxm) parent = parent.elems[0];
                else if (typeof(parent) === 'string') parent = document.getElementById(parent);
                var pattern = new RegExp("(^| )" + selector + "( |$)");
                var e = (parent || document).getElementsByTagName(type || '*')
                for (var i = 0; i < e.length; i++) {
                    if (pattern.test(e[i].className)) {
                        tmpElems.push(e[i]);
                    }
                }
                this.elems = tmpElems;
                tmpElems = null;
                pattern = null;
                i = null;
                e = null;
                selector = null;
                parent = null;
                type = null;
                return this;
            },
            byInstance: function(elem) {
                if (elem.InstanceOffxm) {
                    elem = elem.elems[0];
                }
                var elems = [];
                elems.push(elem);
                this.elems = elems;
                elem = null;
                elems = null;
                return this;
            },
            get: function() {
                return (this.elems.length == 1) ? this.elems[0] : this.elems;
            },
            currStyle: function(cssprop, style) {
                if (style) {
                    for (var i = 0; i < this.elems.length; i++) {
                        this.elems[i].style[cssprop] = style;
                    }
                    cssprop = null;
                    style = null;
                    i = null;
                    return this;
                } else {
                    return fxm.helper.currStyle(this.elems[0], cssprop);
                }
            },
            calculatedHeight: function() {
                return fxm.helper.calculatedHeight(this.elems[0]);
            },
            calculatedWidth: function() {
                return fxm.helper.calculatedWidth(this.elems[0]);
            },
            calculatedLeft: function() {},
            calculatedTop: function() {},
            html: function(inner) {
                if (inner) {
                    for (var i = 0; i < this.elems.length; i++) {
                        this.elems[i].innerHTML = inner;
                    }
                    inner = null;
                    i = null;
                    return this;
                } else {
                    return this.elems[0].innerHTML;
                }
            },
            attrib: function(attribute, value) {
                if (value) {
                    for (var i = 0; i < this.elems.length; i++) {
                        this.elems[i].setAttribute(attribute, value);
                    }
                    attribute = null;
                    value = null;
                    i = null;
                    return this;
                } else {
                    return this.elems[0].getAttribute(attribute);
                }
            },
            addEventListener: function(listener, event) {
                for (var i = 0; i < this.elems.length; i++) {
                    this.elems[i].addEventListener(listener, event);
                }
                listener = null;
                event = null;
                i = null;
                return this;
            },
            focus: function() {
                this.elems[0].focus();
                return this;
            },
            blur: function() {
                this.elems[0].blur();
                return this;
            },
            hide: function() {
                for (var i = 0; i < this.elems.length; i++) {
                    this.elems[i].style.display = 'none';
                }
                i = null;
            },
            show: function() {
                for (var i = 0; i < this.elems.length; i++) {
                    this.elems[i].style.display = 'block';
                }
                i = null;
            },
            toggle: function() {
                for (var i = 0; i < this.elems.length; i++) {
                    if (fxm.helper.currStyle(this.elems[i], 'display') == 'none') this.elems[i].style.display = 'block';
                    else this.elems[i].style.display = 'none';
                }
                i = null;
            },
            checked: function(bol) {
                for (var i = 0; i < this.elems.length; i++) {
                    if (this.elems[i].nodeName.toLowerCase() === 'input' && (this.elems[i].getAttribute('type') == 'checkbox' || this.elems[i].getAttribute('type') == 'radio')) {
                        this.elems[i].checked = bol;
                    }
                }
                bol = null;
                i = null;
                return this;
            },
            even: function() {
                var tmpElems = [];
                for (var i = 2; i < this.elems.length; i += 2) {
                    tmpElems.push(this.elems[i]);
                }
                this.elems = tmpElems;
                tmpElems = null;
                i = null;
                return this;
            },
            odd: function() {
                var tmpElems = [];
                for (var i = 1; i < this.elems.length; i += 2) {
                    tmpElems.push(this.elems[i]);
                }
                this.elems = tmpElems;
                tmpElems = null;
                i = null;
                return this;
            },
            un: function(action, callback) {
                if (this.elems[0].removeEventListener) {
                    for (var i = 0; i < this.elems.length; i++) {
                        this.elems[i].removeEventListener(action, callback, false);
                    }
                } else {
                    for (var i = 0; i < this.elems.length; i++) {
                        this.elems[i].detachEvent('on' + action, callback);
                    }
                }
                action = null;
                callback = null;
                i = null;
                return this;
            },
            on: function(action, callback) {
                if (this.elems[0].addEventListener) {
                    for (var i = 0; i < this.elems.length; i++) {
                        this.elems[i].addEventListener(action, callback, false);
                    }
                } else if (this.elems[0].attachEvent) {
                    for (var i = 0; i < this.elems.length; i++) {
                        this.elems[i].attachEvent('on' + action, callback);
                    }
                }
                action = null;
                callback = null;
                i = null;
                return this;
            },
            init: function(selector, getBy, parent, type) {
                if (selector) {
                    getBy = (typeof(selector) === 'object') ? 'object' : getBy;
                    switch (getBy) {
                        case 'class':
                            return this.byClass(selector, parent, type);
                            break;
                        case 'tag':
                            return this.byTag(selector, parent);
                            break;
                        case 'object':
                            return this.byInstance(selector);
                            break;
                        case 'id':
                        default:
                            return this.byId(selector);
                            break;
                    }
                } else {
                    return this;
                }
            },
            addClass: function(cssClass) {
                for (var i = 0; i < this.elems.length; i++) {
                    fxm.helper.addClass(this.elems[i], cssClass);
                }
                cssClass = null;
                i = null;
                return this;
            },
            removeClass: function(cssClass) {
                for (var i = 0; i < this.elems.length; i++) {
                    fxm.helper.removeClass(this.elems[i], cssClass);
                }
                cssClass = null;
                i = null;
                return this;
            },
            clearClass: function() {
                for (var i = 0; i < this.elems.length; i++) {
                    fxm.helper.clearClass(this.elems[i]);
                }
                i = null;
                return this;
            }
        };
        fxmTmp.fn.init.prototype = fxmTmp.fn;
        return fxmTmp;
    })();
    fxm.blocked = new Array(true);
    fxm.blockKeys = function() {
        this.blocked.push(true);
        fxm("MouseBlocker").hide();
        return;
    };
    fxm.unblockKeys = function(force) {
        if (force) {
            while (this.blocked.length > 0) this.blocked.pop();
            fxm("MouseBlocker").hide();
        } else {
            if (this.blocked.length > 0) this.blocked.pop();
            if (this.blocked.length == 0) fxm("MouseBlocker").hide();
        }
        return;
    };
    fxm.isBlocked = function() {
        return (this.blocked.length > 0) ? true : false;
    };
    fxm.loadingDivId = 'loadingIndicator';
    fxm.showLoading = function() {
        this.blockKeys();
        fxm(fxm.loadingDivId).show();
        return;
    };
    fxm.hideLoading = function(force) {
        this.unblockKeys(force);
        if (!this.isBlocked()) fxm(fxm.loadingDivId).hide();
        return;
    };
    fxm.VERSION = '1.1.2';
    fxm.version = function() {
        return this.VERSION;
    };
    fxm.BODY = document.getElementsByTagName('body')[0];
    fxm.HEAD = document.getElementsByTagName('head')[0];
    window.fxm = fxm;
})();
