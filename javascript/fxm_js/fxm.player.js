
(function() {
    fxm.player = {videoSrc: null, mime: 'video/mp4', fullScreen: true, windowWidth: 640, windowHeight: 360, fullScreenWidth: 1280, fullScreenHeight: 720, fullScreenClass: 'fullscreen', videoObjectWrapper: 'mainVideoObject', videoObjectId: 'video', videoObjectNavigation: null, zindex: 0, startPosition: 0, seekTime: 10, STOPPED: 0, PLAYING: 1, PAUSED: 2, BUFFERING: 4, ENDED: 5, ERROR: 6, currentState: null, loadCatchCounter: 0, raiseOnStop: true, init: function() {
            fxm.player.videoDiv = fxm(fxm.player.videoObjectWrapper).get();
            if (!fxm.player.videoDiv) {
                fxm.player.videoDiv = document.createElement("div");
                fxm.player.videoDiv.setAttribute("id", fxm.player.videoObjectWrapper);
                fxm.player.videoDiv.setAttribute("z-index", fxm.player.zindex);
                document.getElementsByTagName('body')[0].appendChild(fxm.player.videoDiv);
            }
            if (fxm.player.videoObject) {
                fxm.player.raiseOnStop = false;
                //fxm.player.videoObject.playPosition = 0;
                //fxm.player.videoObject.playTime = 0;
                document.getElementById(fxm.player.videoObjectWrapper).removeChild(fxm.player.videoObject);
                //fxm.player.videoObject.parentNode.removeChild(fxm.player.videoObject);
            }
            fxm.player.videoDiv.innerHTML = '';
            fxm.player.videoObject = document.createElement("object");
            fxm.player.videoObject.setAttribute("id", fxm.player.videoObjectId);
            fxm.player.videoObject.setAttribute("type", this.mime);
            fxm.player.videoObject.setAttribute("enabled", "false");
            fxm.player.videoObject.setAttribute("uiMode", "mini");
            fxm.player.videoObject.setAttribute("z-index", fxm.player.zindex);
            if (fxm.player.fullScreen) {
                fxm.player.videoObject.setAttribute("width", fxm.player.fullScreenWidth);
                fxm.player.videoObject.setAttribute("height", fxm.player.fullScreenHeight);
                fxm.helper.addClass(fxm.player.videoDiv, fxm.player.fullScreenClass);
            } else {
                fxm.player.videoObject.setAttribute("width", fxm.player.windowWidth);
                fxm.player.videoObject.setAttribute("height", fxm.player.windowHeight);
                fxm.helper.removeClass(fxm.player.videoDiv, fxm.player.fullScreenClass);
            }
            fxm.player.videoDiv.appendChild(fxm.player.videoObject);
            fxm.player.currentState = fxm.player.STOPPED;
            fxm.player.videoObject.onPlayStateChange = fxm.player.checkPlayState;
            fxm.player.videoObject.data = this.videoSrc;
        }, load: function(videoSrc) {
            fxm.player.videoSrc = videoSrc;
            fxm.player.init();
        }, checkPlayState: function() {
            if (!fxm.player.videoObject) {
                return;
            }
            var playState = fxm.player.videoObject.playState;
            switch (playState) {
                case 5:
                    fxm.player.onend();
                    break;
                case 6:
                    fxm.player.onerror();
                    break;
                case 1:
                    fxm.player.onplay();
                    break;
                case 0:
                    fxm.player.onstop();
                    break;
                case 2:
                    fxm.player.onpause();
                    break;
                case 3:
                    fxm.player.onbuffer();
                    break;
                case 4:
                    fxm.player.onbuffer();
                    break;
                default:
                    break;
                }
        }, play: function(position) {
            if (fxm.player.currentState != fxm.player.PLAYING || (fxm.player.helper.isNumber(position) && position >= 0)) {
                fxm.player.currentState = fxm.player.BUFFERING;
            }
            try {
                fxm.player.startPlaycheckTimer();
                fxm.player.videoObject.play(1);
                if (position) {
                    fxm.player.startPosition = position;
                    setTimeout("fxm.player.seek(" + fxm.player.startPosition + ");", 500);
                } else {
                    fxm.player.startPosition = 0;
                }
            } catch (e) {
                fxm.player.loadCatchCounter++;
                if (this.loadCatchCounter < 3) {
                    setTimeout(function() {
                        fxm.player.play(fxm.player.startPosition);
                    }, 250);
                    return;
                } else {
                    fxm.player.stopPlaycheckTimer();
                    fxm.player.onerror();
                }
            }
        }, pause: function() {
            if (fxm.player.videoObject && fxm.player.currentState == fxm.player.PLAYING) {
                fxm.player.currentState = fxm.player.PAUSED;
                fxm.player.videoObject.play(0);
            }
        }, resume: function() {
            if (fxm.player.videoObject && fxm.player.currentState == fxm.player.PAUSED) {
                fxm.player.play();
            }
        }, stop: function() {
            if (fxm.player.videoObject && fxm.player.currentState != fxm.player.STOPPED) {
                fxm.player.currentState = fxm.player.STOPPED;
                fxm.player.videoObject.stop();
            }
        }, backward: function() {
            if (!fxm.player.videoObject)
                return;
            var seekToTime = fxm.player.videoObject.playPosition - (fxm.player.seekTime * 1000);
            if (seekToTime < 0)
                seekToTime = 0;
            fxm.player.videoObject.seek(seekToTime);
        }, forward: function() {
            if (!fxm.player.videoObject)
                return;
            var seekToTime = fxm.player.videoObject.playPosition + (fxm.player.seekTime * 1000);
            if (seekToTime > fxm.player.videoObject.playTime)
                return;
            fxm.player.videoObject.seek(seekToTime);
        }, seek: function(seekToTime) {
            if (!fxm.player.videoObject)
                return;
            fxm.player.videoObject.seek(seekToTime);
        }, duration: function() {
            return fxm.player.videoObject.playTime;
        }, currentTime: function() {
            return fxm.player.videoObject.playPosition;
        }, playcheckTimeout: 10, playcheckTimer: null, startPlaycheckTimer: function() {
            fxm.player.stopPlaycheckTimer();
            fxm.player.playcheckTimer = setTimeout("fxm.player.onerror();", fxm.player.playcheckTimeout * 1000);
        }, stopPlaycheckTimer: function() {
            if (fxm.player.playcheckTimer)
                clearTimeout(fxm.player.playcheckTimer);
        }, onplay: function(e) {
            if (fxm.player.currentState == fxm.player.BUFFERING || fxm.player.currentState == fxm.player.PAUSED || fxm.player.currentState == fxm.player.STOPED) {
                fxm.player.bandwidthcheckTimer = setTimeout("fxm.player.initBandwidthcheck();", 5000);
            }
            fxm.player.stopPlaycheckTimer();
            fxm.player.currentState = fxm.player.PLAYING;
            if (typeof(fxm.player.onplayHook) == 'function')
                fxm.player.onplayHook();
        }, onpause: function(e) {
            fxm.player.stopPlaycheckTimer();
            fxm.player.stopBandwidthcheckTimer();
            fxm.player.currentState = fxm.player.PAUSED;
            if (typeof(fxm.player.onplayHook) == 'function')
                fxm.player.onpauseHook();
        }, onbuffer: function(e) {
            fxm.player.currentState = fxm.player.BUFFERING;
            if (typeof(fxm.player.onplayHook) == 'function')
                fxm.player.onbufferHook();
        }, onerror: function(e) {
            fxm.player.stopPlaycheckTimer();
            fxm.player.stopBandwidthcheckTimer();
            fxm.player.currentState = fxm.player.ERROR;
            if (typeof(fxm.player.onerrorHook) == 'function')
                fxm.player.onerrorHook();
        }, onstop: function(e) {
            fxm.player.stopPlaycheckTimer();
            fxm.player.stopBandwidthcheckTimer();
            fxm.player.currentState = fxm.player.STOPPED;
            if (typeof(fxm.player.onstopHook) == 'function' && fxm.player.raiseOnStop)
                fxm.player.onstopHook();
            else
                fxm.player.raiseOnStop = true;
        }, onend: function(e) {
            fxm.player.stopPlaycheckTimer();
            fxm.player.stopBandwidthcheckTimer();
            fxm.player.currentState = fxm.player.ENDED;
            fxm.player.startPosition = 0;
            if (typeof(fxm.player.onendHook) == 'function')
                fxm.player.onendHook();
        }, onlowbandwidth: function() {
            if (typeof(fxm.player.onlowbandwidthHook) == 'function')
                fxm.player.onlowbandwidthHook();
        }, initBandwidthcheck: function() {
            if ((fxm.player.duration() - fxm.player.currentTime()) / 1000 >= 20) {
                try {
                    fxm.player.bandwidthcheckCounter = 0;
                    fxm.player.bandwidthcheckStartTime = new Date().getTime();
                    fxm.player.bandwidthcheckVideoStartTime = fxm.player.currentTime();
                    fxm.player.bandwidthcheckTimer = setTimeout("fxm.player.startBandwidthcheckTimer();", 5000);
                } catch (e) {
                }
            } else {
            }
        }, startBandwidthcheckTimer: function() {
            fxm.player.stopBandwidthcheckTimer();
            if (((new Date().getTime() - fxm.player.bandwidthcheckStartTime) - (fxm.player.currentTime() - fxm.player.bandwidthcheckVideoStartTime)) / 1000 > 2.5)
                fxm.player.bandwidthcheckCounter++;
            if (fxm.player.bandwidthcheckCounter < 3) {
                fxm.player.bandwidthcheckStartTime = new Date().getTime();
                fxm.player.bandwidthcheckVideoStartTime = fxm.player.currentTime();
                fxm.player.bandwidthcheckTimer = setTimeout("fxm.player.startBandwidthcheckTimer();", 5000);
            } else {
                fxm.player.onlowbandwidth();
            }
        }, stopBandwidthcheckTimer: function() {
            if (fxm.player.bandwidthcheckTimer)
                clearTimeout(fxm.player.bandwidthcheckTimer);
        }, bandwidthcheckStartTime: null, bandwidthcheckVideoStartTime: null, bandwidthcheckTimer: null, onplayHook: function() {
        }, onpauseHook: function() {
        }, onbufferHook: function() {
        }, onerrorHook: function() {
        }, onstopHook: function() {
        }, onendHook: function() {
        }, onlowbandwidthHook: function() {
        }, helper: {isNumber: function(n) {
                !isNaN(parseFloat(n)) && isFinite(n);
            }}};
})();