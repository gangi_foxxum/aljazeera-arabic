
(function() {
    fxm.debug = {_dump: function(obj, instanceOfFxmObject, filter) {
            if (obj.instanceOf && obj.instanceOf('fxm.object'))
                var instanceOfFxmObject = true;
            var returnarray = {};
            if (filter && typeof (filter) == 'object') {
                switch (filter.shift()) {
                    case'include':
                        var doFilter = 'include';
                        break;
                    case'exclude':
                        var doFilter = 'exclude';
                        break;
                    default:
                        var doFilter = 'all';
                        break;
                }
            } else {
                var doFilter = 'all';
            }
            for (var key in obj) {
                if (doFilter == 'all' || (doFilter == 'include' && filter.in_array(key)) || (doFilter == 'exclude' && !filter.in_array(key))) {
                    if (typeof (obj[key]) == 'string' || typeof (obj[key]) == 'number' || typeof (obj[key]) == 'undefined') {
                        returnarray[key] = obj[key];
                    } else if (obj[key] == null) {
                        returnarray[key] = 'NULL';
                    } else if (typeof (obj[key]) == 'boolean') {
                        if (obj[key])
                            returnarray[key] = 'TRUE';
                        else
                            returnarray[key] = 'FALSE';
                    } else if (typeof (obj[key]) == 'object') {
                        returnarray[key] = (obj[key].instanceOf) ? obj[key]._dump() : fxm.debug._dump(obj[key]);
                    } else if (typeof (obj[key]) == 'function') {
                        if (!instanceOfFxmObject)
                            returnarray[key] = obj[key];
                        else
                            returnarray[key] = 'function';
                    }
                }
            }
            return returnarray;
        }, dumpText: function(obj, level) {
            var dumped_text = "";
            if (!level)
                level = 0;
            var level_padding = "";
            for (var j = 0; j < level + 1; j++)
                level_padding += "  ";
            if (typeof (obj) == 'object') {
                for (var item in obj) {
                    var value = obj[item];
                    if (typeof (value) == 'object') {
                        dumped_text += level_padding + "'" + item + "' ...\n";
                        dumped_text += fxm.debug.dumpText(value, level + 1);
                    } else {
                        dumped_text += level_padding + "'" + item + "' = \"" + value + "\"\n";
                    }
                }
            } else {
                dumped_text = "===" + obj + "===(" + typeof (obj) + ")";
            }
            return dumped_text;
        }, dump: function(obj, doAlert, prefix) {
            if (obj.instanceOf && obj.instanceOf('fxm.object')) {
                var text = fxm.debug.dumpText(obj._dump());
            } else if (typeof (obj) == 'object' || typeof (obj) == 'function') {
                var text = fxm.debug.dumpText(fxm.debug._dump(obj));
            } else if (typeof (obj) == 'boolean') {
                if (obj)
                    var text = 'TRUE';
                else
                    var text = 'FALSE';
            } else if (obj == null) {
                var text = 'NULL';
            } else {
                var text = obj;
            }
            if (prefix)
                text = prefix + ' - ' + text;
            if (doAlert)
                alert(text);
            return text;
        }, dumpFlat: function(obj, doAlert) {
            var returnarray = {};
            for (var key in obj) {
                if (typeof (obj[key]) == 'string' || typeof (obj[key]) == 'number' || typeof (obj[key]) == 'undefined') {
                    returnarray[key] = obj[key];
                } else if (obj[key] == null) {
                    returnarray[key] = 'NULL';
                } else if (typeof (obj[key]) == 'boolean') {
                    if (obj[key])
                        returnarray[key] = 'TRUE';
                    else
                        returnarray[key] = 'FALSE';
                } else if (typeof (obj[key]) == 'object') {
                    returnarray[key] = 'object';
                } else if (typeof (obj[key]) == 'function') {
                    returnarray[key] = 'function';
                }
            }
            return fxm.debug.dump(returnarray, doAlert);
        }};
    fxm.dbg = {xhr: null,fps: false,fpsBottom: false, fpsLeft: false, visible: false, doAlert: false, loggingUrl: null, errorUrl: null, consoleLog: false, lines: 10, position: 'bottom', dbgDivId: 'fxmdbg', currLines: [], dbgDiv: null,fpsContainer: null,avgFpsContainer: null,minFpsContainer: null, maxFpsContainer: null, initialized: false, init: function(confObj) {
            this._construct(confObj);
            if (this.fps){
              var minFps =0;
              var maxFps =0;
              var avgFps=0;
              this.fpsContainer= document.createElement('p');
              this.minFpsContainer= document.createElement('p');
              this.maxFpsContainer=document.createElement('p');
              this.avgFpsContainer=document.createElement('p');
              this.fpsContainer.setAttribute('class', 'fpsCounter');
              this.maxFpsContainer.setAttribute('class', 'fpsCounter');
              this.minFpsContainer.setAttribute('class', 'fpsCounter');
              this.avgFpsContainer.setAttribute('class', 'fpsCounter');

              this.fpsContainer.setAttribute('id', 'fpsContainer');
              this.maxFpsContainer.setAttribute('id', 'minfpsContainer');
              this.minFpsContainer.setAttribute('id', 'maxfpsContainer');
              this.avgFpsContainer.setAttribute('id', 'avgfpsContainer');
              this.minFpsContainer.style.top= 20+'px';
              this.maxFpsContainer.style.top= 40+'px';
              this.avgFpsContainer.style.top= 60+'px';

              if (this.fpsBottom){
                this.fpsContainer.style.top= 680+'px';
                this.minFpsContainer.style.top= 640+'px';
                this.maxFpsContainer.style.top= 660+'px';
                this.avgFpsContainer.style.top= 620+'px';


              }
              if (this.fpsLeft){
                  this.fpsContainer.style.left= 10+'px';
                  this.minFpsContainer.style.left= 10+'px';
                  this.maxFpsContainer.style.left= 10+'px';
                  this.avgFpsContainer.style.left= 10+'px';

              }
              document.getElementsByTagName('body')[0].appendChild(this.fpsContainer);
              document.getElementsByTagName('body')[0].appendChild(this.maxFpsContainer);
              document.getElementsByTagName('body')[0].appendChild(this.minFpsContainer);
              document.getElementsByTagName('body')[0].appendChild(this.avgFpsContainer);
              var fps = {
                  startTime: 0,
                  frameNumber: 0,
                  getFPS: function() {
                      this.frameNumber++;
                      var d = new Date().getTime(),
                          currentTime = (d - this.startTime) / 1000,
                          result = Math.floor((this.frameNumber / currentTime));
                      if (currentTime > 1) {
                          this.startTime = new Date().getTime();
                          this.frameNumber = 0;
                      }
                      return result;
                  }
              };
              var f = document.querySelector("#fpsContainer");
              var minf = document.querySelector("#minfpsContainer");
              var maxf = document.querySelector("#maxfpsContainer");
              var avgf= document.querySelector("#avgfpsContainer");
              var filled= false;


                var counter=0;

              function gameLoop() {
                  setTimeout(gameLoop, 1000 / 60);
                  var currentFps=fps.getFPS();

                  if(counter===99){
                    minFps=currentFps;
                  }
                  if (counter >= 100){
                    if(maxFps<currentFps){
                      maxFps=currentFps;
                    }
                    if(currentFps<minFps){
                      minFps=currentFps;
                    }
                    avgFps=(minFps+maxFps+currentFps)/3;
                    avgf.innerHTML = "Avg FPS: "+parseInt(avgFps);
                    minf.innerHTML = "Min FPS: "+minFps;
                    maxf.innerHTML = "Max FPS: "+maxFps;
                    if (currentFps>=avgFps){
                      $(this.fpsContainer).css('color', '	#00FF00');

                    }
                    if (currentFps<avgFps){
                      $(this.fpsContainer).css('color', 'rgb(240, 37, 25)');

                    }

                  }
                  else{
                  counter++;
                  }
                  f.innerHTML = "FPS: "+currentFps;


              }
              gameLoop();

            }
            if (this.visible) {
                try {
                    var height = (20 * this.lines) + 20;
                    this.dbgDiv = document.createElement('div');
                    this.dbgDiv.setAttribute('id', this.dbgDivId);
                    document.getElementsByTagName('body')[0].appendChild(this.dbgDiv);
                    this.dbgDiv.style.top = ((this.position == 'bottom') ? ((720 - height) + 'px') : '0px');
                    this.dbgDiv.style.height = (height - 20) + 'px';
                    fxm(this.dbgDivId).addClass('fxm_dbgDiv fxm_transparent_curton');
                    fxm(this.dbgDivId).show();
                    this.initialized = true;

                } catch (e) {
                    alert(e);
                }
            }
        }, log: function(msg, from, type) {
            if (!this.initialized)
                return;
            try {
                if (this.consoleLog && window.console && console.log)
                    console.log('log:', msg);
            } catch (e) {
                alert('no console.log available');
            }
            var color = 'green';
            if (this.visible) {
                if (type !== undefined && type !== null) {
                    if (type === 'error') {
                        color = 'red';
                    } else if (type === 'alert') {
                        color = 'yellow';
                    }
                    if (from !== undefined && from !== null) {
                        this.write('<b class="log" style="font-weight: bold; color: ' + color + '">file:</b> ' + from + ' ---> <b class="log" style="font-weight: bold; color: ' + color + ';">log:</b> ' + fxm.debug.dump(msg));
                    } else {
                        this.write('<b class="log" style="font-weight: bold; color: ' + color + ';">log:</b> ' + fxm.debug.dump(msg));
                    }
                } else if (from !== undefined && from !== null) {
                    this.write('<b class="log" style="font-weight: bold; color: ' + color + '">file:</b> ' + from + ' ---> <b class="log" style="font-weight: bold; color: ' + color + ';">log:</b> ' + fxm.debug.dump(msg));
                } else {
                    this.write('<b class="log" style="font-weight: bold; color: ' + color + ';">log:</b> ' + fxm.debug.dump(msg));
                }
            }
            if (this.loggingUrl)
                this.writeFile(msg, 'log');
            if (this.doAlert)
                alert('log: ' + fxm.debug.dump(msg))
        }, error: function(msg) {
            if (!this.initialized)
                return;
            try {
                if (this.consoleLog && window.console && console.log)
                    console.log('error:', msg);
            } catch (e) {
                alert('no console.log available');
            }
            if (this.visible)
                this.write('error: ' + fxm.debug.dump(msg));
            if (this.errorUrl)
                this.writeFile(msg, 'error');
            if (this.doAlert)
                alert('error: ' + fxm.debug.dump(msg))
        }, write: function(msg) {
            if (this.currLines.length >= this.lines)
                this.currLines.shift();
            this.currLines.push((msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br />' + '$2'));
            var DebugText = '';
            for (i = this.currLines.length - 1; i >= 0; --i) {
                DebugText += this.currLines[i] + "\n<br/>";
            }
            fxm(this.dbgDivId).html(DebugText);
        }, writeFile: function(msg, type) {
            if (type == 'log' && this.loggingUrl) {
                this.doRequest('log=' + fxm.helper.dob64(fxm.debug.dump(msg)), this.loggingUrl);
            } else if (type == 'error' && this.errorUrl) {
                this.doRequest('error=' + fxm.helper.dob64(fxm.debug.dump(msg)), this.errorUrl);
            }
        }, doRequest: function(params, url) {
            this.xhr = new XMLHttpRequest();
            params = params + '&rand=' + fxm.helper.rand(0, 9999999);
            this.xhr.open("POST", url, true);
            this.xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            this.xhr.setRequestHeader("Content-length", params.length);
            this.xhr.setRequestHeader("Connection", "close");
            this.xhr.onreadystatechange = function() {
                if (fxm.dbg.xhr.readyState == 4 && fxm.dbg.xhr.status == 200) {
                }
            };
            this.xhr.send(params);
        }, _construct: function(confObj) {
            for (var key in confObj) {
                this[key] = confObj[key];
            }
        }};
    fxm.dbg.constructor.prototype = new fxm.object;
})();
