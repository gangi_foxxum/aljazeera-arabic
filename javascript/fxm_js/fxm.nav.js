
(function() {
    fxm.navMap = {selfClass: 'fxm.navMap', navObjects: [], navObjectsItemsMap: [], pushNav: function(nav, replace) {
            if (typeof (replace) != 'undefined') {
                var id = parseInt(replace);
                this.navObjects[id] = nav;
                nav.navMapId = id;
                var initializeItems = true;
            } else {
                if (!nav.navMapId) {
                    var id = this.navObjects.length;
                    this.navObjects.push(nav);
                    nav.navMapId = id;
                    var initializeItems = true;
                } else {
                    var initializeItems = false;
                }
            }
            if (initializeItems) {
                this.navObjectsItemsMap[id] = [];
                for (navItem in nav.navItems) {
                    if (nav.navItems[navItem].instanceOf && nav.navItems[navItem].instanceOf('fxm.navItem'))
                        this.navObjectsItemsMap[id].push(nav.navItems[navItem].elemId);
                }
            }
            nav = replace = initializeItems = null;
            return this.navObjects[id];
        }, activate: function(navItemId) {
            var navObject = this.getNavObject(navItemId);
            if (navObject) {
                if (fxm.currNav.deactivate)
                    fxm.currNav.deactivate();
                fxm.currNav = navObject;
                if (fxm.currNav.activateHook) {
                    fxm.currNav.activateHook(navItemId);
                } else {
                    fxm.currNav.activate(navItemId);
                }
            }
            navItemId = navObject = null;
        }, deactivate: function(navItemId) {
            var navObject = this.getNavObject(navItemId);
            if (navObject)
                navObject.deactivate();
            navItemId = navObject = null;
        }, moveTo: function(navItemId) {
            var navObject = this.getNavObject(navItemId);
            if (navObject) {
                if (fxm.currNav && fxm.currNav.deactivate) {
                    fxm.currNav.deactivate();
                } else {
                    for (id in this.navObjects) {
                        if (this.navObjects[id].instanceOf && this.navObjects[id].instanceOf('fxm.nav'))
                            this.navObjects[id].deactivate();
                    }
                }
                fxm.currNav = navObject;
                fxm.currNav.activateSafe(navItemId);
            }
            navItemId = navObject = null;
        }, getNavObject: function(id) {
            if (typeof (id) == 'number' && this.navObjects[id]) {
                return this.navObjects[id];
            } else if (typeof (id) == 'string') {
                for (itemsMapId in this.navObjectsItemsMap) {
                    if (typeof (this.navObjectsItemsMap[itemsMapId]) != 'function' && this.navObjectsItemsMap[itemsMapId].in_array(id))
                        return this.navObjects[itemsMapId];
                }
                id = id.replace(/(\d+)$/, '');
                for (objId in this.navObjects) {
                    if (this.navObjects[objId].instanceOf && this.navObjects[objId].instanceOf('fxm.nav') && this.navObjects[objId].itemPrefix == id)
                        return this.navObjects[objId];
                }
            }
            return null;
        }, _dump: function() {
            var returnarray = {};
            returnarray['navObjects'] = {};
            for (obj in this.navObjects) {
                returnarray['navObjects'][obj] = this.navObjects[obj].defaultItem;
            }
            returnarray['navObjectsItemsMap'] = fxm.debug._dump(this.navObjectsItemsMap, true);
            returnarray['selfClass'] = 'fxm.navMap';
            return returnarray;
        }, dump: function(doAlert, prefix) {
            return fxm.debug.dump(this, doAlert, prefix);
        }}
    fxm.navMap.constructor.prototype = new fxm.object;
    fxm.currNav = null;
    fxm.navItem = function(elemId, moveLeft, moveRight, moveUp, moveDown, isControllElem) {
        this.selfClass = 'fxm.navItem';
        this.elemId = elemId;
        this.moveLeft = moveLeft;
        this.moveRight = moveRight;
        this.moveUp = moveUp;
        this.moveDown = moveDown;
        this.isControllElem = (isControllElem) ? true : false;
        this.directionValue = function(direction) {
            switch (direction) {
                case'left':
                    return this.moveLeft;
                case'right':
                    return this.moveRight;
                case'up':
                    return this.moveUp;
                case'down':
                    return this.moveDown;
                default:
                    return null;
                }
        };
        this.focused = false;
        this.preFocus = null;
        this.postFocus = null;
        this.preBlur = null;
        this.postBlur = null;
        this.disabled = false;
        this.self = function() {
            return'fxm.navItem.' + this.elemId;
        };
        this._dump = function() {
            var filter = ['include', 'selfClass', 'elemId', 'isControllElem', 'focused', 'disabled'];
            if (typeof (this.moveLeft) != 'object' && typeof (this.moveLeft) != 'function')
                filter.push('moveLeft');
            if (typeof (this.moveRight) != 'object' && typeof (this.moveRight) != 'function')
                filter.push('moveRight');
            if (typeof (this.moveUp) != 'object' && typeof (this.moveUp) != 'function')
                filter.push('moveUp');
            if (typeof (this.moveDown) != 'object' && typeof (this.moveDown) != 'function')
                filter.push('moveDown');
            return fxm.debug._dump(this, true, filter);
        };
        this.dump = function(doAlert, prefix) {
            return fxm.debug.dump(this, doAlert, prefix);
        };
    }
    fxm.navItem.prototype = new fxm.object;
    fxm.nav = function(navItems, replace) {
        this.selfClass = 'fxm.nav';
        this.navMapId = null;
        this.parentObj = null;
        this.navItems = navItems;
        this.defaultItem = this.navItems['default'];
        this.currItem = this.navItems['default'];
        this.itemPrefix = (this.navItems['prefix']) ? this.navItems['prefix'] : null;
        this.activeItem = null;
        this.currElem = null;
        this.moveLeft = function() {
            this.move('left');
        };
        this.moveRight = function() {
            this.move('right');
        };
        this.moveUp = function() {
            this.move('up');
        };
        this.moveDown = function() {
            this.move('down');
        };
        this.click = function() {
            if (this.currElem) {
                if (!this.navItems[this.currItem].isControllElem) {
                    this.setActiveItem(this.currItem);
                }
                if (this.currElem.onclick) {
                    this.currElem.onclick();
                }
            }
        };
        this.move = function(direction, navItem) {
            var currItem = this.currItem;
            if (navItem) {
                currItem = navItem;
            }
            if (!this.navItems[currItem] || !this.navItems[currItem].directionValue(direction)) {
                return;
            }
            var nextId = this.navItems[currItem].directionValue(direction);
            if (typeof (nextId) == 'function') {
                nextId(this.navItems[currItem], direction);
                return;
            } else if (typeof (nextId) == 'object') {
                switch (direction) {
                    case'left':
                        nextId.left(this.navItems[currItem], direction);
                        return;
                        break;
                    case'right':
                        nextId.right(this.navItems[currItem], direction);
                        return;
                        break;
                    case'up':
                        nextId.up(this.navItems[currItem], direction);
                        return;
                        break;
                    case'down':
                        nextId.down(this.navItems[currItem], direction);
                        return;
                        break;
                    }
            }
            if (!this.navItems[nextId]) {
                fxm.navMap.moveTo(nextId);
                return;
            }
            nextItem = this.navItems[nextId];
            if (nextItem) {
                htmlElem = fxm(nextId).get();
                if (htmlElem == null || typeof (htmlElem) == 'undefined') {
                    return;
                }
                if (fxm.helper.currStyle(htmlElem, 'display') != 'none' && !nextItem.disabled) {
                    this.activate(nextId);
                } else {
                    this.move(direction, nextId);
                }
                return;
            }
        };
        this.activate = function(nextItem) {
            if (!nextItem)
                var nextItem = this.currItem;
            this.deactivate();
            var focusItem = this.navItems[nextItem];
            if (focusItem) {
                if (focusItem.preFocus) {
                    focusItem.preFocus(focusItem.elemId);
                }
                var focusHtmlElem = fxm(nextItem).get();
                if (!focusHtmlElem || typeof (focusHtmlElem) == 'undefined' || fxm.helper.currStyle(focusHtmlElem, 'display') == 'none') {
                    return false;
                }
                fxm.helper.addClass(focusHtmlElem, 'focus');
                focusItem.focused = true;
                if (focusItem.postFocus) {
                    focusItem.postFocus(focusItem.elemId);
                }
                this.currItem = nextItem;
                this.currElem = focusHtmlElem;
                fxm.currNav = this;
                return true;
            }
            return false;
        };
        this.activateSafe = function(navItemId) {
            if (!this.activateHook || !this.activateHook(navItemId)) {
                if (!this.activate(navItemId)) {
                    if (!this.currItem || !this.activate(this.currItem)) {
                        if (!this.activeItem || !this.activate(this.activeItem)) {
                            if (!this.defaultItem || !this.activate(this.defaultItem)) {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        };
        this.activateHook = null;
        this.deactivate = function(itemId) {
            if (itemId != null && itemId != '') {
                var unfocusItem = this.navItems[itemId];
            } else {
                var unfocusItem = this.navItems[this.currItem];
            }
            if (unfocusItem) {
                if (unfocusItem.preBlur) {
                    unfocusItem.preBlur(unfocusItem.elemId);
                }
                if (unfocusItem.deactivate && !unfocusItem.deactivate(nextItem)) {
                    return false;
                }
                unfocusItem.focused = false;
                if (unfocusItem.postBlur) {
                    unfocusItem.postBlur(unfocusItem.elemId);
                }
                var unfocusHtmlElem = fxm(this.currItem).get();
                if (!unfocusHtmlElem || typeof (unfocusHtmlElem) == 'undefined' || fxm.helper.currStyle(unfocusHtmlElem, 'display') == 'none') {
                    return false;
                }
                fxm.helper.removeClass(unfocusHtmlElem, 'focus');
                return true;
            }
            return false;
        };
        this.disable = function(itemId) {
            var disableItem = this.navItems[itemId];
            if (disableItem) {
                if (disableItem.disable && !disableItem.disable()) {
                    return;
                }
                disableItem.disabled = true;
            }
        };
        this.enable = function(itemId) {
            var enableItem = this.navItems[itemId];
            if (enableItem) {
                if (enableItem.enable && !enableItem.enable()) {
                    return;
                }
                enableItem.disabled = false;
            }
        };
        this.add = function(elemId, moveLeft, moveRight, moveUp, moveDown) {
            this.navItems[elemId] = new fxm.navItem(elemId, moveLeft, moveRight, moveUp, moveDown);
            var htmlElem = fxm(elemId).get();
            htmlElem.addEventListener("mouseover", fxm.mouseOver, false);
            htmlElem.addEventListener("mouseout", fxm.mouseOut, false);
            htmlElem.addEventListener("focus", fxm.mouseOver, false);
            htmlElem.addEventListener("blur", fxm.mouseOut, false);
            fxm.navMap.navObjectsItemsMap[this.navMapId].push(elemId);
        };
        this.get = function(elemId) {
            if (!elemId || elemId == '') {
                return this.navItems[this.currItem];
            }
            if (!this.navItems[elemId])
                return;
            return this.navItems[elemId];
        };
        this.setActiveItem = function(itemId) {
            this.removeActiveItem();
            this.activeItem = itemId;
            fxm.helper.addClass(fxm(this.activeItem).get(), 'active');
            if (this.setActiveItemHook)
                this.setActiveItemHook(itemId);
        };
        this.setActiveItemHook = null;
        this.removeActiveItem = function() {
            if (this.activeItem) {
                fxm.helper.removeClass(fxm(this.activeItem).get(), 'active');
                this.activeItem = null;
            }
        };
        this.self = function() {
            return'fxm.nav.' + this.defaultItem;
        };
        for (navItem in this.navItems) {
            if (this.navItems[navItem].instanceOf && this.navItems[navItem].instanceOf('fxm.navItem')) {
                var htmlElem = fxm(navItem).get();
                htmlElem.addEventListener("mouseover", fxm.mouseOver, false);
                htmlElem.addEventListener("mouseout", fxm.mouseOut, false);
                htmlElem.addEventListener("focus", fxm.mouseOver, false);
                htmlElem.addEventListener("blur", fxm.mouseOut, false);
            }
        }
        this._dump = function() {
            return fxm.debug._dump(this, true, ['include', 'selfClass', 'navMapId', 'navItems', 'defaultItem', 'currItem', 'itemPrefix', 'activeItem', 'currElem', 'moveLeft', 'moveRight', 'moveUp', 'moveDown', 'click', 'move', 'activate', 'activateSafe', 'activateHook', 'deactivate', 'disable', 'enable', 'add', 'get', 'setActiveItem', 'removeActiveItem', 'self']);
        };
        this.dump = function(doAlert, prefix) {
            return fxm.debug.dump(this, doAlert, prefix);
        };
        return fxm.navMap.pushNav(this, replace);
    }
    fxm.nav.prototype = new fxm.object;
    fxm.mouseOver = function(tagId) {
        var myId;
        if (typeof (tagId) == 'string') {
            myId = tagId;
        } else {
            myId = this.id;
        }
        fxm.navMap.activate(myId);
    }
    fxm.mouseOut = function(tagId) {
        var myId;
        if (typeof (tagId) == 'string') {
            myId = tagId;
        } else {
            myId = this.id;
        }
        fxm.navMap.deactivate(myId);
    }
})();