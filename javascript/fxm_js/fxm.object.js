(function() {
    if (typeof(Object.getPrototypeOf) !== "function") {
        if (typeof("test".__proto__) === "object") {
            Object.getPrototypeOf = function(object) {
                return object.__proto__;
            };
        } else {
            Object.getPrototypeOf = function(object) {
                return object.constructor.prototype;
            };
        }
    }
    fxm.object = function(confObj) {
        this.selfClass = 'fxm.object';
        this.instanceOf = function(constructorObj) {
            if (constructorObj) {
                var obj = this;
                while (obj && obj.selfClass) {
                    if (constructorObj == obj.selfClass) return true;
                    obj = Object.getPrototypeOf(obj);
                }
                return false;
            } else {
                var hirarchy = [];
                var obj = this;
                while (obj && obj.selfClass) {
                    hirarchy.push(obj.selfClass);
                    obj = Object.getPrototypeOf(obj);
                }
                return hirarchy;
            }
        };
        this._dump = function() {
            return fxm.debug._dump(this, true);
        };
        this.dump = function(doAlert, prefix) {
            return fxm.debug.dump(this, doAlert, prefix);
        };
        this._construct = function(confObj) {
            for (var key in confObj) {
                this[key] = confObj[key];
            }
        };
        this._construct(confObj);
    }
})();
