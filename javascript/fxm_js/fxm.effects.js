(function() {
    fxm.effect = {};
    fxm.effect.move = function(confObj) {
        this.selfClass = 'fxm.effect.move';
        this.identifier = null;
        this.containerId = null;
        this.blockNavigation = true;
        this.sequence = 50;
        this.speed = 1000;
        this.callback = null;
        this.initTime = null;
        this.updateTimer = null;
        this.stepSize = null;
        this.positionTop = null;
        this.positionLeft = null;
        this.finalTop = null;
        this.finalLeft = null;
        this.elem = null;
        this.stop = function() {
            if (this.updateTimer) {
                clearTimeout(this.updateTimer);
            }
        };
        this.left = function(pixels, callbackMethod) {
            this.callback = callbackMethod;
            this.stepSize = pixels / (this.speed / this.sequence);
            this.finalTop = this.positionTop;
            this.finalLeft = this.positionLeft - pixels;
            this.initTimestamp = new Date().getTime();
            if (this.blockNavigation) fxm.blockKeys();
            this.move('left');
        };
        this.right = function(pixels, callbackMethod) {
            this.callback = callbackMethod;
            this.stepSize = pixels / (this.speed / this.sequence);
            this.finalTop = this.positionTop;
            this.finalLeft = this.positionLeft + pixels;
            this.initTimestamp = new Date().getTime();
            if (this.blockNavigation) fxm.blockKeys();
            this.move('right');
        };
        this.up = function(pixels, callbackMethod) {
            this.callback = callbackMethod;
            this.stepSize = pixels / (this.speed / this.sequence);
            this.finalTop = this.positionTop - pixels;
            this.finalLeft = this.positionLeft;
            this.initTimestamp = new Date().getTime();
            if (this.blockNavigation) fxm.blockKeys();
            this.move('up');
        };
        this.down = function(pixels, callbackMethod) {
            this.callback = callbackMethod;
            this.stepSize = pixels / (this.speed / this.sequence);
            this.finalTop = this.positionTop + pixels;
            this.finalLeft = this.positionLeft;
            this.initTimestamp = new Date().getTime();
            if (this.blockNavigation) fxm.blockKeys();
            this.move('down');
        };
        this.move = function(direction) {
            if ((new Date().getTime() - this.initTimestamp) < this.speed) {
                try {
                    var tmpPosition = null;
                    switch (direction) {
                        case 'up':
                            tmpPosition = (this.positionTop - this.stepSize);
                            if (tmpPosition >= this.finalTop) this.positionTop = tmpPosition;
                            break;
                        case 'right':
                            tmpPosition = (this.positionLeft + this.stepSize);
                            if (tmpPosition <= this.finalLeft) this.positionLeft = tmpPosition;
                            break;
                        case 'down':
                            tmpPosition = (this.positionTop + this.stepSize);
                            if (tmpPosition <= this.finalTop) this.positionTop = tmpPosition;
                            break;
                        case 'left':
                            tmpPosition = (this.positionLeft - this.stepSize);
                            if (tmpPosition >= this.finalLeft) this.positionLeft = tmpPosition;
                            break;
                    }
                    this.elem.style.top = this.positionTop + 'px';
                    this.elem.style.left = this.positionLeft + 'px';
                    this.updateTimer = setTimeout('window["' + this.identifier + '"].move("' + direction + '")', this.sequence);
                } catch (e) {
                    this.updateTimer = setTimeout('window["' + this.identifier + '"].move("' + direction + '")', this.sequence);
                }
            } else {
                this.positionTop = this.finalTop;
                this.positionLeft = this.finalLeft;
                this.elem.style.top = this.positionTop + 'px';
                this.elem.style.left = this.positionLeft + 'px';
                if (this.blockNavigation) fxm.unblockKeys();
                if (typeof(this.callback) == 'function') this.callback();
            }
        };
        this.self = function() {
            return this.selfClass + '.' + this.containerId;
        };
        this._dump = function() {
            return fxm.debug._dump(this, true, ['include', 'selfClass', 'identifier', 'containerId']);
        };
        this.dump = function(doAlert, prefix) {
            return fxm.debug.dump(this, doAlert, prefix);
        };
        this._construct = function(confObj) {
            for (var key in confObj) {
                this[key] = confObj[key];
            }
            this.elem = fxm(this.containerId).get();
            var position = fxm.helper.calculatedPosition(this.elem);
            this.positionTop = position.top;
            this.positionLeft = position.left;
            this.identifier = this.containerId + fxm.helper.rand(0, new Date().getTime());
            window[this.identifier] = this;
        };
        this._construct(confObj);
    };
    fxm.effect.move.prototype = new fxm.object;
})();
