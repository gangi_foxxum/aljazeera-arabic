(function() {
    fxm.kbd = {};
    fxm.keyboard = {
        deactivated: true,
        nofocus: false,
        multitap: false,
        inputFields: [],
        position: "bottom",
        language: null,
        inputType: null,
        kbdDivId: "fxmkeyboard",
        kbdDiv: null,
        kbdDivBGImg: null,
        kbdHtml: null,
        kbdInputId: null,
        kbdInputNavObject: null,
        Nav: null,
        Keys: null,
        Cursor: '<span class="fxm_kbdCursor">|</span>',
        preCursor: [],
        postCursor: [],
        cancelCallback: null,
        callback: null,
        count: 0,
        currChars: "kbdControl_bigChars",
        init: function(confObj) {
            this._construct(confObj);
            if (typeof confObj.deactivated != "undefined")
                this.deactivated = confObj.deactivated;
            else
                this.deactivated = fxmconfig.kbd.deactivated;
            if (typeof confObj.nofocus != "undefined")
                this.nofocus = confObj.nofocus;
            else
                this.nofocus = fxmconfig.kbd.nofocus;
            if (typeof confObj.multitap != "undefined") this.multitap = confObj.multitap;
            else
                this.multitap = fxmconfig.kbd.multitap;
            var tmpLang = null;
            if (typeof confObj.language != "undefined")
                tmpLang = confObj.language;
            else
                tmpLang = fxmconfig.kbd.language;
            try {
                this.language = eval("fxm.kbd." + tmpLang)
            } catch (e) {
                this.language = null
            }
            if (this.language == null || this.language.chars.length != 144) {
                fxm.dbg.log("You need to set the Keyboard Language!")
            } else {
                var languageKeys = [];
                var text = tmpLang + "-";
                for (var i = 0; i < this.language.chars.length; i++) {
                    var letter = this.language.chars[i];
                    languageKeys.push({
                        letter: letter
                    });
                    text += letter + "-"
                }
            }
            this.kbdHtml = '<div id="kbdWrapper" class="fxm_keyboard_wrapper">';
            this.kbdHtml += '<div id="kbdInputWrapper" class="fxm_kbdInputWrapper text">';
            this.kbdHtml += '<div id="kbdInput" class="fxm_kbdInput">' + this.Cursor + "</div>";
            this.kbdHtml += "</div>";
            this.kbdHtml += '<ul id="kbdControls_left" class="fxm_kbdControls_left">';
            this.kbdHtml += '<li id="kbdControl_smallChars" class="fxm_kbdControl_smallChars" onclick="fxm.keyboard.controlPage(0);"></li>';
            this.kbdHtml += '<li id="kbdControl_bigChars" class="fxm_kbdControl_bigChars" onclick="fxm.keyboard.controlPage(1);"></li>';
            this.kbdHtml += '<li id="kbdControl_specialChars" class="fxm_kbdControl_2" onclick="fxm.keyboard.controlPage(2);">' + this.language.controls.specialChars + "</li>";
            this.kbdHtml += "</ul>";
            this.kbdHtml += '<ul id="kbdKeys" class="fxm_kbdKeys"></ul>';
            this.kbdHtml += '<ul id="kbdControls" class="fxm_kbdControls">';
            this.kbdHtml += '<li id="kbdControl_http" class="fxm_kbdControl_3" onclick="fxm.keyboard.controlPressed(\'http://\');">http://</li>';
            this.kbdHtml += '<li id="kbdControl_www" class="fxm_kbdControl_2" onclick="fxm.keyboard.controlPressed(\'www.\');">www.</li>';
            this.kbdHtml += '<li id="kbdControl_at" class="fxm_kbdControl_1" onclick="fxm.keyboard.controlPressed(\'@\');">@</li>';
            this.kbdHtml += '<li id="kbdControl_space" class="fxm_kbdControl_3" onclick="fxm.keyboard.controlPressed(\' \');"> </li>';
            this.kbdHtml += '<li id="kbdControl_com" class="fxm_kbdControl_2" onclick="fxm.keyboard.controlPressed(\'.com\');">.com</li>';
            this.kbdHtml += '<li id="kbdControl_cancel" class="fxm_kbdControl_3" onclick="fxm.keyboard.cancel();">' + this.language.controls.cancelText + "</li>";
            this.kbdHtml += "</ul>";
            this.kbdHtml += '<ul id="kbdControls_right" class="fxm_kbdControls_right">';
            this.kbdHtml += '<li id="kbdControl_del" class="fxm_kbdControl_2" onclick="fxm.keyboard.controlDel();">' + this.language.controls.delText + "</li>";
            this.kbdHtml += '<li id="kbdControl_backspace" class="fxm_kbdControl_backspace" onclick="fxm.keyboard.controlBackspace();"></li>';
            this.kbdHtml += '<li id="kbdControl_backward" class="fxm_kbdControl_backward" onclick="fxm.keyboard.controlBack();"></li>';
            this.kbdHtml += '<li id="kbdControl_forward" class="fxm_kbdControl_forward" onclick="fxm.keyboard.controlForward();">&#187;</li>';
            this.kbdHtml += '<li id="kbdControl_submit" class="fxm_kbdControl_2" onclick="fxm.keyboard.write();">' + this.language.controls.submitText + "</li>";
            this.kbdHtml += "</ul>";
            this.kbdHtml += "</div>";
            this.kbdDiv = document.createElement("div");
            this.kbdDiv.setAttribute("id", this.kbdDivId);
            document.getElementsByTagName("body")[0].appendChild(this.kbdDiv);
            fxm(this.kbdDivId).addClass("fxm_keyboard fxm_transparent_curton");
            fxm(this.kbdDivId).html(this.kbdHtml);
            this.Control = new fxm.nav({
                "default": "kbdControl_bigChars",
                kbdControl_smallChars: new fxm.navItem("kbdControl_smallChars", "kbdControl_backspace", "kbdKey_24", "kbdKey_0", "kbdControl_specialChars"),
                kbdControl_at: new fxm.navItem("kbdControl_at", "kbdControl_www", "kbdControl_space", "kbdKey_39", "kbdKey_3"),
                kbdControl_space: new fxm.navItem("kbdControl_space", "kbdControl_at", "kbdControl_com", "kbdKey_41", "kbdKey_5"),
                kbdControl_bigChars: new fxm.navItem("kbdControl_bigChars", "kbdControl_backspace", "kbdKey_24", "kbdKey_0", "kbdControl_specialChars"),
                kbdControl_http: new fxm.navItem("kbdControl_http", "kbdControl_submit", "kbdControl_www", "kbdControl_specialChars", "kbdKey_0"),
                kbdControl_www: new fxm.navItem("kbdControl_www", "kbdControl_http", "kbdControl_at", "kbdKey_37", "kbdKey_1"),
                kbdControl_com: new fxm.navItem("kbdControl_com", "kbdControl_space", "kbdControl_cancel", "kbdKey_43", "kbdKey_7"),
                kbdControl_specialChars: new fxm.navItem("kbdControl_specialChars", "kbdControl_forward", "kbdKey_36", this.kbdControl_currChars, "kbdControl_http"),
                kbdControl_backward: new fxm.navItem("kbdControl_backward", "kbdKey_47", "kbdControl_forward", "kbdControl_backspace", "kbdControl_submit"),
                kbdControl_forward: new fxm.navItem("kbdControl_forward", "kbdControl_backward", "kbdControl_specialChars", "kbdControl_backspace", "kbdControl_submit"),
                kbdControl_del: new fxm.navItem("kbdControl_del", "kbdKey_23", "kbdKey_12", "kbdKey_11", "kbdControl_backspace"),
                kbdControl_backspace: new fxm.navItem("kbdControl_backspace", "kbdKey_35", this.kbdControl_currChars, "kbdControl_del", "kbdControl_backward"),
                kbdControl_cancel: new fxm.navItem("kbdControl_cancel", "kbdControl_com", "kbdControl_submit", "kbdKey_46", "kbdKey_10"),
                kbdControl_submit: new fxm.navItem("kbdControl_submit", "kbdControl_cancel", "kbdControl_http", "kbdControl_backward", "kbdControl_del")
            });
            this.Keys = new fxm.slider.twoD.paging({
                containerId: "kbdKeys",
                itemIdPrefix: "kbdKey_",
                horizontalItems: 12,
                verticalItems: 4,
                chessboardPattern: true,
                disableAutomaticBrowsing: true,
                horizontal: false,
                itemHtmlTemplate: '<li id="{{itemIdPrefix}}{{itemId}}" onclick="fxm.keyboard.keyPressed({{itemId}});"></li>',
                setItem: function(e, t) {
                    fxm(this.itemIdPrefix + t).html(fxm.helper.charToUnicodeHTML(this.itemsArray[e]["letter"]))
                },
                leaveAsc: function() {
                    switch (this.Nav.currItem) {
                        case "kbdKey_36":
                            fxm.navMap.moveTo("kbdControl_http");
                            break;
                        case "kbdKey_37":
                            fxm.navMap.moveTo("kbdControl_www");
                            break;
                        case "kbdKey_38":
                            fxm.navMap.moveTo("kbdControl_www");
                            break;
                        case "kbdKey_39":
                            fxm.navMap.moveTo("kbdControl_at");
                            break;
                        case "kbdKey_40":
                        case "kbdKey_41":
                        case "kbdKey_42":
                            fxm.navMap.moveTo("kbdControl_space");
                            break;
                        case "kbdKey_43":
                        case "kbdKey_44":
                            fxm.navMap.moveTo("kbdControl_com");
                            break;
                        case "kbdKey_45":
                        case "kbdKey_46":
                        case "kbdKey_47":
                            fxm.navMap.moveTo("kbdControl_cancel");
                            break
                    }
                    return
                },
                leaveDesc: function() {
                    switch (this.Nav.currItem) {
                        case "kbdKey_0":
                            fxm.navMap.moveTo("kbdControl_http");
                            break;
                        case "kbdKey_1":
                            fxm.navMap.moveTo("kbdControl_www");
                            break;
                        case "kbdKey_2":
                            fxm.navMap.moveTo("kbdControl_www");
                            break;
                        case "kbdKey_3":
                            fxm.navMap.moveTo("kbdControl_at");
                            break;
                        case "kbdKey_4":
                        case "kbdKey_5":
                        case "kbdKey_6":
                            fxm.navMap.moveTo("kbdControl_space");
                            break;
                        case "kbdKey_7":
                        case "kbdKey_8":
                            fxm.navMap.moveTo("kbdControl_com");
                            break;
                        case "kbdKey_9":
                        case "kbdKey_10":
                        case "kbdKey_11":
                            fxm.navMap.moveTo("kbdControl_cancel");
                            break
                    }
                    return
                },
                leaveUpLeft: function() {
                    switch (this.Nav.currItem) {
                        case "kbdKey_0":
                            fxm.navMap.moveTo("kbdKey_11");
                            break;
                        case "kbdKey_12":
                            fxm.navMap.moveTo("kbdControl_del");
                            break;
                        case "kbdKey_24":
                            fxm.navMap.moveTo(fxm.keyboard.currChars);
                            break;
                        case "kbdKey_36":
                            fxm.navMap.moveTo("kbdControl_specialChars");
                            break
                    }
                    return
                },
                leaveDownRight: function() {
                    switch (this.Nav.currItem) {
                        case "kbdKey_11":
                            fxm.navMap.moveTo("kbdKey_0");
                            break;
                        case "kbdKey_23":
                            fxm.navMap.moveTo("kbdControl_del");
                            break;
                        case "kbdKey_35":
                            fxm.navMap.moveTo("kbdControl_backspace");
                            break;
                        case "kbdKey_47":
                            fxm.navMap.moveTo("kbdControl_backward");
                            break
                    }
                    return
                },
                itemsArray: languageKeys
            })
        },
        _construct: function(e) {
            for (var t in e) {
                this[t] = e[t]
            }
        },
        kbdControl_currChars: function() {
            fxm.currNav.activate(fxm.keyboard.currChars)
        },
        controlPage: function(e) {
            if (e == 0 && fxm.keyboard.count == 0) {
                fxm("kbdControl_bigChars").removeClass("focus");
                fxm("kbdControl_smallChars").removeClass("focus")
            }
            this.Keys.moveToPage(e);
            if (e == 0) {
                fxm("kbdControl_bigChars").show();
                fxm("kbdControl_smallChars").hide();
                if (fxm.keyboard.count > 0)
                    fxm.navMap.moveTo("kbdControl_bigChars");
                fxm.keyboard.currChars = "kbdControl_bigChars";
                fxm.keyboard.count = 1
            } else if (e == 1) {
                fxm("kbdControl_smallChars").show();
                fxm("kbdControl_bigChars").hide();
                if (fxm.keyboard.count > 0)
                    fxm.navMap.moveTo("kbdControl_smallChars");
                fxm.keyboard.currChars = "kbdControl_smallChars"
            }
        },
        focus: function(e) {
            if (this.nofocus)
                return;
            if (!this.multitap) {
                this.inputFields[e] = true;
                fxm(e).focus()
            }
        },
        blur: function(e) {
            if (this.nofocus)
                return;
            if (!this.multitap) {
                this.inputFields[e] = false;
                fxm(e).blur()
            } else if (this.inputFields[e]) {
                fxm(e).blur()
            }
        },
        open: function(e, t, n, r) {
            if (this.multitap && (typeof this.inputFields[e] == "undefined" || this.inputFields[e] == false)) {
                for (var i in this.inputFields) {
                    this.inputFields[i] = false
                }
                this.inputFields[e] = true;
                fxm(e).focus();
                fxm(e).attrib("onblur", "fxm.keyboard.inputFields['" + e + "'] = false;");
                return
            }
            if (this.deactivated)
                return;
            if (r)
                this.callback = r;
            if (!this.nofocus)
                fxm(e).blur();
            this.kbdInputNavObject = fxm.currNav;
            fxm.currNav.deactivate();
            this.kbdInputId = e;
            this.kbdDiv.style.display = "block";
            fxm("kbdInputWrapper").removeClass("password");
            fxm("kbdInputWrapper").removeClass("text");
            fxm("kbdInputWrapper").removeClass("textarea");
            switch (t) {
                case "password":
                    this.inputType = "password";
                    fxm("kbdInputWrapper").addClass("password");
                    break;
                case "textarea":
                    this.inputType = "textarea";
                    fxm("kbdInputWrapper").addClass("textarea");
                    break;
                case "text":
                default:
                    this.inputType = "text";
                    fxm("kbdInputWrapper").addClass("text");
                    break
            }
            this.preCursor = [];
            this.postCursor = [];
            this.preCursor = fxm(this.kbdInputId).get().value.split("");
            this.updateText();
            this.kbdInputNavObject.deactivate();
            this.controlPage(0);
            fxm.currNav.deactivate();
            fxm.currNav = this.Keys.Nav;
            fxm.navMap.moveTo("kbdKey_0")
        },
        updateText: function() {
            var e = this.preCursor.join("");
            var t = this.postCursor.join("");
            var n = "";
            if (this.inputType == "password") {
                for (var r = 0; r < e.length; r++) {
                    n += "*"
                }
                n += this.Cursor;
                for (var r = 0; r < t.length; r++) {
                    n += "*"
                }
            } else {
                n = fxm.helper.strToUnicodeHTML(e) + this.Cursor + fxm.helper.strToUnicodeHTML(t)
            }
            fxm("kbdInput").html(n);
            e = t = n = r = null
        },
        write: function() {
            if (this.multitap)
                this.inputFields[this.kbdInputId] = false;
            if (this.inputType == "textarea")
                fxm(this.kbdInputId).html(this.preCursor.join("") + this.postCursor.join(""));
            else
                fxm(this.kbdInputId).get().value = this.preCursor.join("") + this.postCursor.join("");
            fxm("kbdInput").html(this.Cursor);
            fxm.currNav.deactivate();
            fxm.currNav = this.kbdInputNavObject;
            this.kbdDiv.style.display = "none";
            fxm.currNav.activate();
            fxm.keyboard.count = 0;
            fxm.keyboard.currChars = "kbdControl_bigChars";
            if (typeof this.callback == "function")
                this.callback()
        },
        cancel: function() {
            if (this.multitap)
                this.inputFields[this.kbdInputId] = false;
            fxm.currNav.deactivate();
            fxm.currNav = this.kbdInputNavObject;
            fxm("kbdInput").html(this.Cursor);
            this.kbdDiv.style.display = "none";
            fxm.currNav.activate();
            fxm.keyboard.count = 0;
            fxm.keyboard.currChars = "kbdControl_bigChars";
            if (typeof this.cancelCallback === "function") {
                this.cancelCallback()
            } /*else if (typeof this.callback === 'function') {
                this.callback();
            }*/
        },
        keyPressed: function(e) {
            this.preCursor.push(this.Keys.getValue(e, "letter"));
            this.updateText()
        },
        controlPressed: function(e) {
            this.preCursor.push(e);
            this.updateText()
        },
        controlBack: function() {
            if (this.preCursor.length > 0)
                this.postCursor.unshift(this.preCursor.pop());
            this.updateText()
        },
        controlForward: function() {
            if (this.postCursor.length > 0)
                this.preCursor.push(this.postCursor.shift());
            this.updateText()
        },
        controlDel: function() {
            if (this.postCursor.length > 0)
                this.postCursor.shift();
            this.updateText()
        },
        controlBackspace: function() {
            if (this.preCursor.length > 0)
                this.preCursor.pop();
            this.updateText()
        }
    };
    fxm.keyboard.constructor.prototype = new fxm.object
})()
