blockRemoteControl = false;
/* lets the back space key be ignored when in full screen mode and
 and cursor keys by default (keys will will be handled
 in handleRemoteButtons()
 */
function ignoreKeyPress(e) {
    //fxm.dbg.log('keyPress: '+e.keyCode);

    // Prevent Vavigation on Loading Div


    switch (e.keyCode) {
        // prevent cursor keys from being interpreted by platform
        case VK_LEFT:
        case VK_RIGHT:
        case VK_DOWN:
        case VK_UP:
        case VK_ENTER:
            e.preventDefault();
            return false;
            break;

            // prevent color keys from being interpreted by platform
        case VK_RED:
        case VK_GREEN:
        case VK_YELLOW:
            e.preventDefault();
            return false;
            break;

        case VK_BLUE:
            e.preventDefault();
            return false;
            break;

        case VK_BACK:
        case VK_BACK_SPACE:
            return false;
            break;

        case VK_PLAY:
        case VK_PAUSE:
            //case VK_PLAY_PAUSE:
            //case VK_PAUSE_PLAY:
        case VK_STOP:
        case VK_FAST_FWD:
        case VK_REWIND:
            e.preventDefault();
            break;
    }
}

/* handles the remote buttons */
function handleRemoteButtons(e) {
    //fxm.dbg.log('handleRemoteButtons: '+e.keyCode);

    if (blockRemoteControl)
        return;

    switch (e.keyCode) {

        case VK_ENTER:
        case VK_LEFT:
        case VK_RIGHT:
        case VK_UP:
        case VK_DOWN:
        case VK_0:
        case VK_BACK:
        case VK_BACK_SPACE:
        case VK_PLAY:
        case VK_PAUSE:
        case VK_STOP:
        case VK_FAST_FWD:
        case VK_REWIND:
            e.preventDefault();
            handleNavigationButtons(e);
            break;

            // prevent color keys from being interpreted by platform
        case VK_RED:
            e.preventDefault();
            redButtonPress();
            return false;
            break;

            // prevent color keys from being interpreted by platform
        case VK_GREEN:
            e.preventDefault();
            greenButtonPress();
            return false;
            break;

            // prevent color keys from being interpreted by platform
        case VK_YELLOW:
            e.preventDefault();
            yellowButtonPress();
            return false;
            break;

        case VK_BLUE:
            e.preventDefault();
            blueButtonPress();
            return false;
            break;

        default:
            e.preventDefault();
            return false;
            break;
    }

    return false;

}

/* handles the buttons on the remote control */
function handleNavigationButtons(e) {
    switch (e.keyCode) {
        case VK_ENTER:
            fxm.currNav.click();
            break;

        case VK_LEFT:
            fxm.currNav.moveLeft();
            break;

        case VK_RIGHT:
            fxm.currNav.moveRight();
            break;

        case VK_UP:
            fxm.currNav.moveUp();
            break;

        case VK_DOWN:
            fxm.currNav.moveDown();
            break;
    }
}

function redButtonPress() {

}

function greenButtonPress() {

}

function yellowButtonPress() {

}

function blueButtonPress() {

}

// handling remote control buttons
document.onkeydown = handleRemoteButtons;
// ignoring regular keypress event selective
document.addEventListener("keypress", ignoreKeyPress, true);