App.addView((function() {
    // Private variables and functions
    var _categoriesId =null,
        _lastCategorie;
    var _setCategories = function () {
        _categoriesId = Focus.register({
                watchLive: [_hideSidebar, _hideSidebar, "cat6", "search"],
                search:[_hideSidebar, _hideSidebar, _checkWatchLive, "cat0"],
                cat0:[_hideSidebar, _hideSidebar, "search", "cat1"],
                cat1:[_hideSidebar, _hideSidebar, "cat0", "cat3"],
                cat3:[_hideSidebar, _hideSidebar, "cat1", "cat4"],
                cat4:[_hideSidebar, _hideSidebar, "cat3", "cat5"],
                cat5:[_hideSidebar, _hideSidebar, "cat4", "cat6"],
             cat6:[_hideSidebar, _hideSidebar, "cat5", _checkWatchLive]
        });
    };
    var _checkWatchLive = function(){
		if ($("#watchLive").is(":visible")){
			Focus.moveTo("watchLive");
		}
		else{
			Focus.moveTo("search");
		}

	}

    var _listeners = function(){

        $("#watchLive").click(function(){
             if (SmartLibrary.$_GET('hls') !== "false"){
                App.Main.setSidebarFlag(false);
                 $("#moreTab").hide();
                App.Main.playLiveStream();
                $("#videoPlayer").css('width', "1280px");
                $("#videoPlayer").css('height',"720px");
                $("#mainVideoObject").css('width', "1280px");
                $("#mainVideoObject").css('height',"720px");
                $("#playerView").css('width', "1280px");
                $("#playerView").css('height',"720px");
                $("#liveWrapper").show();
                $("#liveVideoWrapper").css("width", "1280px");
                $("#liveVideoWrapper").css('height',"720px");
                $("#liveVideoWrapper").css('top',"-147px");
                $("#liveVideoWrapper").css('left',"-593px");
                $("#liveVideoWrapper").css('z-index',"1500");
                _hideSidebar();
            }
          });

        $("#search").click(function(){
            App.Main.setCurrentLoc("search");
            $("#moreTab").hide();
            window.localStorage.setItem('prevCategorie', window.localStorage.getItem('currentCategorie'));
            window.localStorage.setItem('currentCategorie', _language.search);
            ajkeen.AJNavigate(window.localStorage.getItem('prevCategorie'), window.localStorage.getItem('currentCategorie'));
            $("#searchView").show();
            $("#liveWrapper").hide();
            $("#liveInfo").hide();
            $("#carouselWrapper").hide();
            $("#collectionWrapper").hide();
            $("#episodesInfo").hide();
            _hideSidebar();
            videoPlayer.pause();
        });

        $("#cat0").click(function(){
            App.Main.setCurrentLoc("episodes");
            $("#moreTab").hide();
            $("#episodesInfo").show();
            $("#splashIcon").show();
            showItemCategorie(_language.latestNews);
            _backFromSearch();
            videoPlayer.pause();
        });
        $("#cat1").click(function () {
            $("#moreTab").hide();
            App.Main.setCurrentLoc("episodes");
            $("#episodesInfo").show();
            $("#splashIcon").show();
            showItemCategorie(_language.latestProgrammes);
            _backFromSearch();
            videoPlayer.pause();
        });
//
//        $("#cat2").click(function () {
//          App.Main.setCurrentLoc("episodes");
//            $("#episodesInfo").show();
//            showItemCategorie(_language.editorsPick);
//            _backFromSearch();
//            videoPlayer.pause();
//        });

        $("#cat3").click(function () {
            $("#moreTab").hide();
            App.Main.setCurrentLoc("collections");
            App.Main.collectionFilter(_language.discussions);
            $("#splashIcon").show();
            $("#sidebarWrapper").hide();
            $("#collectionWrapper").show();
            $("#episodesInfo").hide();
            $("#mainView").css("opacity", "1");
            _backFromSearch();
            videoPlayer.pause();
        });
        $("#cat4").click(function () {
            $("#moreTab").hide();
            App.Main.setCurrentLoc("collections");
            App.Main.collectionFilter(_language.newsMagazine);
            $("#sidebarWrapper").hide();
            $("#splashIcon").show();
            $("#collectionWrapper").show();
            $("#episodesInfo").hide();
            $("#mainView").css("opacity", "1");
            _backFromSearch();
            videoPlayer.pause();
        });
        $("#cat5").click(function () {
            $("#moreTab").hide();
            App.Main.setCurrentLoc("collections");
            App.Main.collectionFilter(_language.documentaries);
            $("#splashIcon").show();
            $("#sidebarWrapper").hide();
            $("#collectionWrapper").show();
            $("#episodesInfo").hide();
            $("#mainView").css("opacity", "1");
            _backFromSearch();
            videoPlayer.pause();
        });
        $("#cat6").click(function(){
            App.Main.setCurrentLoc("privacy");
            $("#sidebarWrapper").hide("slide");
            $("#moreTab").show();
            _backFromSearch();
            $("#mainView").css("opacity", "1");
            Focus.register({privacyElem: ["cookieElem", _showSidebar, "blueInfo", ""],
                            cookieElem: ["", "privacyElem", "blueInfo", ""]});
            Focus.moveTo("privacyElem");
            videoPlayer.pause();
        });
    };

    var _backFromSearch = function (){
        if ($("#searchView").is(":visible")) {
            $("#searchView").hide();
            $("#episodesInfo").css("top","");
            $("#episodesInfo").css("width","");
            $("#episodesInfo").css("left","");
        }
    };
    var _showSidebar = function () {
        $("#mainView").css("opacity", "0.4");
        $("#sidebarWrapper").show();
        App.Sidebar.checkWatchLive();
    };
    var _hideSidebar = function() {
        $("#sidebarWrapper").hide();
        $("#mainView").css("opacity", "1");
        if (App.Main.getCurrentLoc() !== "privacy") {
            if ($("#liveVideoWrapper").is(":visible") && $("#liveVideoWrapper").css("width") !== "1280px") {
                Focus.moveTo("liveVideoWrapper");
                $("#carouselWrapper").show();
                $("#carouselWrapper").css("top", "")
                $("#collectionWrapper").css("top", "")
                $("#liveWrapper").show();
                $("#liveInfo").show();
            }

            if ($("#liveVideoWrapper").is(":visible") && $("#liveVideoWrapper").css("width") === "1280px") {
                Focus.moveTo("liveVideoWrapper");
                $("#liveVideoWrapper.focus").css("border-width", "0px");
                $("#carouselWrapper").show();
                $("#carouselWrapper").css("top", "")
                $("#collectionWrapper").css("top", "")
                $("#liveWrapper").show();
                $("#liveInfo").show();
            }

            if (($("#carouselWrapper").is(":visible")) && ($("#liveVideoWrapper").is(":visible")) === false) {
                App.Main.findCarousel("carousel_images_")
            }
            if (($("#carouselWrapper").is(":visible") === false) && ($("#liveVideoWrapper").is(":visible")) === false && ($("#collectionWrapper").is(":visible"))) {
                App.Main.findCarousel("collection_images_");
            }
            if ($("#searchWrapper").is(":visible")) {
                Focus.moveTo("key_ض");
            }
        }else{
            Focus.moveTo("privacyElem");
        }

    };

    var showItemCategorie = function (categorie) {
        App.Main.itemFilter(categorie);
        window.localStorage.setItem('prevCategorie', window.localStorage.getItem('currentCategorie'));
        window.localStorage.setItem('currentCategorie', categorie);
        ajkeen.AJNavigate(window.localStorage.getItem('prevCategorie'), window.localStorage.getItem('currentCategorie'));
        $("#sidebarWrapper").hide();
        $("#mainView").css("opacity", "1");
        $("#liveWrapper").hide();
        $("#liveInfo").hide();
        $("#carouselWrapper").show();
        $("#carouselWrapper").css("top", "-150px");
        $("#collectionWrapper").hide();
    };

      // Public variables and functions
    var view = {};

    view.id = 'Sidebar';
    view.containerID = 'sidebarView';

    view.onHideOptions = {left: '-100%'};
    view.onShowOptions = {left: 0};

    view.onLoad = function() {};
    view.onUnload = function() {};

    view.init = function(view) {
       _setCategories();
       _listeners();

    };
    view.deinit = function() {};

    view.onHideStart = function() {};
    view.onHideComplete = function() {};

    view.onShowStart = function() {};
    view.onShowComplete = function() {};

    view.onHistoryBack = function() {};

    view.getLastCategorie = function(){
        return _lastCategorie;
    }
    view.showItemCategorie = showItemCategorie;
    view.checkWatchLive = _checkWatchLive;
    return view;
})());
