<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>{{TITLE}}</title>
        <meta charset="utf-8" />
        {{CSS}}
    </head>
    <body>

        <div id="fxm_focus"></div>
        <div id ="updateAgreement" class="agreement">
            <div id="updateLogo" class="logoAgreement"></div>
            <div id="updateTitle" class="titleAgreement"><p>تحديثات لسياسة الخصوصية وسياسة ملفات تعريف الارتباط الخاصة بنا</p></div>
            <div id="updateTextContainer" class="textAgreement">
                <p>لقد قمنا بتحديث سياسة الخصوصية وسياسة ملفات تعريف الارتباط الخاصة بنا لمنحك المزيد من الشفافية في البيانات التي نجمعها وكيفية استخدامها. يجب عليك الموافقة على الشروط الواردة في سياسة الخصوصية وسياسة ملفات تعريف الارتباط الخاصة بنا للمتابعة. للعثور على المزيد ، يرجى زيارة موقعنا على الإنترنت لقراءة سياساتنا.</p>
            </div>
            <div id="updateUrlContainer" class="urlAgreement">
                <p>network.aljazeera.com/ar/privacy</p>
            </div>
            <div id="updateButtonAgree" class ="buttonAgreement" onclick="App.Main.AcceptAgreement()"><p>أوافق</p></div>
        </div>
        <div id ="privacyAgreement" class="agreement">
            <div id="privacyLogo" class="logoAgreement"></div>
            <div id="privacyTitle" class="titleAgreement"><p>سياسة الخصوصية وسياسة ملفات تعريف الارتباط</p></div>
            <div id="privacyTextContainer" class="textAgreement">
                <p>لتزويدك بأفضل تجربة مستخدم ممكنة ،نحن نستخدم ملفات تعريف الارتباط ونجمع بعض البيانات حول استخدام هذا التطبيق. يجب عليك الموافقة على الشروط الواردة في سياسة الخصوصية وسياسة ملفات تعريف الارتباط الخاصة بنا للمتابعة. للعثور على المزيد ، يرجى زيارة موقعنا على الإنترنت لقراءة سياساتنا.</p>
            </div>
            <div id="privacyUrlContainer" class="urlAgreement">
                <p>network.aljazeera.com/ar/privacy</p>
            </div>
            <div id="privacyButtonAgree" class ="buttonAgreement" onclick="App.Main.AcceptAgreement()"><p>أوافق</p></div>
        </div>
        <div id="mainAppWrapper">

            <div id="appWrapper">

                {{VIEWS}}

            </div>

        </div>

        {{JAVASCRIPT}}
        <script>
            var videoPlayer = document.getElementById('videoPlayer');
            var clientInfo = new ClientJS(); // Create A New Client Info Object
            var ajkeen = new AJKeen(); //This is for analytics
        </script>
    </body>
</html>
